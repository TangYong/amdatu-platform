/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.ace.client.model.Artifact;
import org.amdatu.ace.client.model.Model;
import org.amdatu.ace.client.model.Target;
import org.amdatu.ace.client.model.TargetState;
import org.amdatu.ace.client.test.SimpleHttpServer;
import org.amdatu.ace.client.test.ValidateWorkspaceCommit;
import org.amdatu.ace.client.test.ValidateWorkspaceCreate;
import org.amdatu.ace.client.test.ValidateWorkspaceRemove;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;
import org.mortbay.jetty.Handler;

/**
 * Tests for the AceClient using a mock Jetty backend that allows
 * assertions on interaction at the HTTP level.
 */
public class AceClientTest {

    public final static String SCHEME = "http";
    public final static String HOST = "localhost";
    public final static String CLIENT_PATH = "/client/work";
    public final static String WORKSPACE_NAME = "rest-1";

    private static int m_startport = 9200;

    private int getNextPort() {
        return ++m_startport;
    }

    /**
     * Simple test on the client and workspace life cycle.
     * 
     * @throws Exception
     */
    @Test
    public void testAceClientConnect() throws Exception {

        final int port = getNextPort();

        Mockery mockeryContext = new Mockery();
        final Handler aceClientServerHandler = mockeryContext.mock(Handler.class);

        mockeryContext.checking(new Expectations() {
            {
                oneOf(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCreate(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                one(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceCommit(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
                one(aceClientServerHandler).handle(with(aNonNull(String.class)),
                    with(aNonNull(HttpServletRequest.class)),
                    with(aNonNull(HttpServletResponse.class)), with(aNonNull(int.class)));
                will(new ValidateWorkspaceRemove(SCHEME, HOST, port, CLIENT_PATH, WORKSPACE_NAME));
            }
        });

        SimpleHttpServer server = new SimpleHttpServer(port, aceClientServerHandler);
        server.startServer();

        try {
            AceClient c = new AceClient(SCHEME + "://" + HOST + ":" + port + "" + CLIENT_PATH);
            AceClientWorkspace w = c.createNewWorkspace();
            assertNotNull(w);
            w.commit();
            w.remove();
        }
        finally {
            server.stopServer();
            mockeryContext.assertIsSatisfied();
        }
    }

    @Test(expected = AceClientException.class)
    public void testAceClientConnectFail() throws Exception {
        AceClient c = new AceClient("http://localhost:9999/client/work");
        c.createNewWorkspace();
    }

    @Test
    public void testModelSerialization() throws Exception {
        String json = getJsonResource("testmodel1.json");

        Model m = AceClientUtil.fromJson(json, Model.class);
        assertNotNull(m);
        assertEquals(27, m.getResources(Artifact.class).length);

        // round trip
        String json2 = AceClientUtil.toJson(m);
        Model m2 = AceClientUtil.fromJson(json2, Model.class);
        assertNotNull(m2);
        assertEquals(27, m2.getResources(Artifact.class).length);
    }

    @Test
    public void testTargetDeserialization() throws Exception {
        String json = getJsonResource("targetmodel1.json");

        Target t = AceClientUtil.fromJson(json, Target.class);
        assertNotNull(t);
        
        TargetState state = t.getState();
        assertNotNull(state);

        assertEquals("Registered", state.getRegistrationState());

        assertNotNull(state.getArtifactsFromShop());
        assertEquals(3, state.getArtifactsFromShop().length);
        
        assertTrue(state.isAutoApprove());
        
        assertFalse(state.isLastInstallSuccess());
    }

    /**
     * @param name
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private String getJsonResource(String name) throws IOException {
        URL testJsonUrl = AceClientTest.class.getClassLoader().getResource(name);

        Reader r = null;
        StringBuilder builder = new StringBuilder();

        try {
            r = new InputStreamReader(testJsonUrl.openStream(), "UTF-8");
            
            int read;
            final char[] buffer = new char[512];
            while ((read = r.read(buffer)) >= 0) {
                builder.append(buffer, 0, read);
            }
            
            r.close();
            r = null;
        }
        finally {
            if (r != null) {
                r.close();
            }
        }
        return builder.toString();
    }
}
