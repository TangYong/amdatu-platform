/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.ace.client.AceClientUtil;
import org.amdatu.ace.client.model.AbstractResource;
import org.amdatu.ace.client.model.Resource;
import org.hamcrest.Description;
import org.junit.Assert;
import org.mortbay.jetty.HttpHeaders;
import org.mortbay.jetty.HttpMethods;

public class ValidateResourceCreate extends ValidateRequestAction<Object> {

    private AbstractResource m_resource;

    public ValidateResourceCreate(String scheme, String host, int port, String clientpath, String workspaceid,
        AbstractResource expectedResource) {
        super(scheme, host, port, clientpath, workspaceid);
        m_resource = expectedResource;
    }

    public void describeTo(Description description) {
        description.appendText("Validates correctness of a HTTP request for an Ace Client resource create request");
    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, int dispatch)
        throws IOException, ServletException {

        String collection = m_resource.getClass().getAnnotation(Resource.class).path();

        Assert.assertEquals(HttpMethods.POST, request.getMethod());
        Assert.assertEquals(getWorkspacePath() + "/" + collection, request.getPathInfo());

        BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()));
        StringBuffer buffer = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            buffer.append(line);
            buffer.append('\n');
        }
        rd.close();
        String data = buffer.toString();
        AbstractResource newResource = AceClientUtil.fromJson(data, m_resource.getClass());
        Assert.assertEquals("Submitted resource must be equal to expected resource", m_resource, newResource);

        response.setStatus(HttpURLConnection.HTTP_MOVED_TEMP);
        response.setHeader(HttpHeaders.LOCATION, getWorkspaceEndpoint() + collection + "/artifact1");
    }
}