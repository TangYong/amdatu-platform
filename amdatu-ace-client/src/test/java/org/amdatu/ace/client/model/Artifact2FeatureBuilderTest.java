/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.junit.Assert;
import org.junit.Test;

public class Artifact2FeatureBuilderTest {

    @Test
    public void testArtifact2FeatureBuilder() throws Exception {

        Feature2DistributionBuilder b = new Feature2DistributionBuilder();
        Feature2Distribution f2d = b
            .setLeftEndpoint("left")
            .setLeftCardinality("10")
            .setRightEndpoint("right")
            .setRightCardinality("11")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertNotNull(f2d);
        Assert.assertEquals("left", f2d.getLeftEndpoint());
        Assert.assertEquals("10", f2d.getLeftCardinality());
        Assert.assertEquals("right", f2d.getRightEndpoint());
        Assert.assertEquals("11", f2d.getRightCardinality());
        Assert.assertEquals("attributeValue", f2d.getAttribute("anAttribute"));
        Assert.assertEquals("tagValue", f2d.getTag("aTag"));
    }
}
