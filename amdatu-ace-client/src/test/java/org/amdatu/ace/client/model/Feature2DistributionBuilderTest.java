/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.junit.Assert;
import org.junit.Test;

public class Feature2DistributionBuilderTest {

    @Test
    public void testArtifact2FeatureBuilder() throws Exception {

        Artifact2FeatureBuilder b = new Artifact2FeatureBuilder();
        Artifact2Feature a2f = b
            .setLeftEndpoint("left")
            .setLeftCardinality("10")
            .setRightEndpoint("right")
            .setRightCardinality("11")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertNotNull(a2f);
        Assert.assertEquals("left", a2f.getLeftEndpoint());
        Assert.assertEquals("10", a2f.getLeftCardinality());
        Assert.assertEquals("right", a2f.getRightEndpoint());
        Assert.assertEquals("11", a2f.getRightCardinality());
        Assert.assertEquals("attributeValue", a2f.getAttribute("anAttribute"));
        Assert.assertEquals("tagValue", a2f.getTag("aTag"));
    }
}
