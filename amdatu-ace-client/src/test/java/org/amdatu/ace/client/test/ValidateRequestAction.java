/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hamcrest.Description;
import org.jmock.api.Action;
import org.jmock.api.Invocation;
import org.mortbay.jetty.HttpConnection;
import org.mortbay.jetty.Request;
import org.mortbay.jetty.handler.AbstractHandler;

abstract class ValidateRequestAction<T> extends AbstractHandler implements Action {

    private final String m_scheme;
    private final String m_host;
    private final int m_port;
    private final String m_clientpath;
    private final String m_workspaceid;

    public ValidateRequestAction(String scheme, String host, int port, String clientpath, String workspaceid) {
        m_scheme = scheme;
        m_host = host;
        m_port = port;
        m_clientpath = clientpath;
        m_workspaceid = workspaceid;
    }

    public void describeTo(Description description) {
        description.appendText("Validates a HTTP request");
    }

    public Object invoke(Invocation invocation) throws Throwable {
        String target = (String) invocation.getParameter(0);
        HttpServletRequest request = (HttpServletRequest) invocation.getParameter(1);
        HttpServletResponse response = (HttpServletResponse) invocation.getParameter(2);
        int dispatch = (Integer) invocation.getParameter(3);

        handle(target, request, response, dispatch);

        Request base_request =
            (request instanceof Request) ? (Request) request : HttpConnection.getCurrentConnection()
                .getRequest();
        base_request.setHandled(true);
        return null;
    }

    public String getScheme() {
        return m_scheme;
    }

    public String getHost() {
        return m_host;
    }

    public int getPort() {
        return m_port;
    }

    public String getClientPath() {
        return m_clientpath;
    }

    public String getWorkspaceId() {
        return m_workspaceid;
    }

    public String getWorkspacePath() {
        return getClientPath() + "/" + getWorkspaceId();
    }

    public String getClientEndpoint() {
        return m_scheme + "://" + m_host + ":" + m_port + "" + m_clientpath;
    }

    public String getWorkspaceEndpoint() {
        return getClientEndpoint() + "/" + m_workspaceid;
    }
}
