/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.bio.SocketConnector;
import org.mortbay.jetty.handler.AbstractHandler;

/**
 * Simple Jetty based server that delegates everything to a specified handler.
 */
public class SimpleHttpServer extends AbstractHandler {

    private Server m_server;
    private int m_port;
    private Handler m_handler;

    public SimpleHttpServer(int port, Handler h) {
        super();
        m_port = port;
        m_handler = h;
        initServer();
    }

    private void initServer() {
        m_server = new Server();
        SocketConnector connector = new SocketConnector();
        connector.setPort(m_port);
        m_server.addConnector(connector);
        m_server.addHandler(this);
    }

    public void startServer() {
        try {
            m_server.start();
        }
        catch (Exception e) {
            stopServer();
            throw new RuntimeException(e);
        }
    }

    public void stopServer() {
        try {
            m_server.stop();
            m_server.join();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, int dispatch)
        throws IOException, ServletException {
        m_handler.handle(target, request, response, dispatch);
    }
}