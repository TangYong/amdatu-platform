/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.junit.Assert;
import org.junit.Test;

public class Distribution2TargetBuilderTest {

    @Test
    public void testArtifact2FeatureBuilder() throws Exception {

        Distribution2TargetBuilder b = new Distribution2TargetBuilder();
        Distribution2Target d2t = b
            .setLeftEndpoint("left")
            .setLeftCardinality("10")
            .setRightEndpoint("right")
            .setRightCardinality("11")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertNotNull(d2t);
        Assert.assertEquals("left", d2t.getLeftEndpoint());
        Assert.assertEquals("10", d2t.getLeftCardinality());
        Assert.assertEquals("right", d2t.getRightEndpoint());
        Assert.assertEquals("11", d2t.getRightCardinality());
        Assert.assertEquals("attributeValue", d2t.getAttribute("anAttribute"));
        Assert.assertEquals("tagValue", d2t.getTag("aTag"));
    }
}
