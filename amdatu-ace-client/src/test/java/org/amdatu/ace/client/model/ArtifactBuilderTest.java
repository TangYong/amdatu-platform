/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.junit.Assert;
import org.junit.Test;

public class ArtifactBuilderTest {

    @Test
    public void testArtifactBuilder() throws Exception {

        ArtifactBuilder b = new ArtifactBuilder();
        Artifact a = b
            .isBundle()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertNotNull(a);
        Assert.assertEquals(Artifact.MIMETYPE_BUNDLE, a.getMimetype());
        Assert.assertEquals("some://url", a.getUrl());
        Assert.assertEquals("myPid", a.getProcessorPid());
        Assert.assertEquals("name", a.getName());
        Assert.assertEquals("description", a.getDescription());
        Assert.assertEquals("bundleName", a.getBundleName());
        Assert.assertEquals("bundleSymbolicName", a.getBundleSymbolicName());
        Assert.assertEquals("bundleVersion", a.getBundleVersion());
        Assert.assertEquals("attributeValue", a.getAttribute("anAttribute"));
        Assert.assertEquals("tagValue", a.getTag("aTag"));
    }

    @Test
    public void testEquality() throws Exception {

        Artifact a = new ArtifactBuilder()
            .isBundle()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Artifact b = new ArtifactBuilder()
            .isBundle()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertEquals(a, b);
    }
    
    @Test
    public void testInequality() throws Exception {

        Artifact a = new ArtifactBuilder()
            .isBundle()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Artifact b = new ArtifactBuilder()
            .isConfig()
            .setUrl("some://url")
            .setProcessorPid("myPid")
            .setName("name")
            .setDescription("description")
            .setBundleName("bundleName")
            .setBundleSymbolicName("bundleSymbolicName")
            .setBundleVersion("bundleVersion")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertFalse(a.equals(b));
    }
}
