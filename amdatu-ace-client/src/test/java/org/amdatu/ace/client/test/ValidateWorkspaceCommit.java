/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.test;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hamcrest.Description;
import org.junit.Assert;
import org.mortbay.jetty.HttpMethods;

public class ValidateWorkspaceCommit extends ValidateRequestAction<Object> {

    public ValidateWorkspaceCommit(String scheme, String host, int port, String clientpath, String workspaceid) {
        super(scheme, host, port, clientpath, workspaceid);
    }

    public void describeTo(Description description) {
        description.appendText("Validates correctness of a HTTP request for committing an Ace Client workspace");
    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, int dispatch)
        throws IOException, ServletException {
        Assert.assertEquals(HttpMethods.POST, request.getMethod());
        Assert.assertEquals(getWorkspacePath(), request.getPathInfo());
        response.setStatus(HttpURLConnection.HTTP_OK);
    }
}