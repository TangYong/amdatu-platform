/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.junit.Assert;
import org.junit.Test;

public class FeatureBuilderTest {

    @Test
    public void testFeatureBuilder() throws Exception {

        FeatureBuilder b = new FeatureBuilder();
        Feature f = b
            .setName("name")
            .setDescription("description")
            .setAttribute("anAttribute", "attributeValue")
            .setTag("aTag", "tagValue")
            .build();

        Assert.assertNotNull(f);
        Assert.assertEquals("name", f.getName());
        Assert.assertEquals("description", f.getDescription());
        Assert.assertEquals("attributeValue", f.getAttribute("anAttribute"));
        Assert.assertEquals("tagValue", f.getTag("aTag"));
    }
}
