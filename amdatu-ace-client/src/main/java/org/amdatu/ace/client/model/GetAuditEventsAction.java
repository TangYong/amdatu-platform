/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

/**
 * Denotes an action to obtain the audit events for a {@link Target}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class GetAuditEventsAction extends AbstractAction {

    private final int m_start;
    private final int m_max;

    /**
     * Creates a new {@link GetAuditEventsAction} instance.
     */
    public GetAuditEventsAction() {
        this(-1, -1);
    }

    /**
     * Creates a new {@link GetAuditEventsAction} instance.
     * 
     * @param start the start index of the audit events to retrieve, >= 0;
     * @param max the maximum number of audit events to retrieve, >= 0.
     */
    public GetAuditEventsAction(int start, int max) {
        super("GET");

        m_start = start;
        m_max = max;
    }

    /**
     * @see org.amdatu.ace.client.model.AbstractAction#getPath()
     */
    @Override
    protected String getPath() {
        String result = "auditEvents";
        if (m_start >= 0) {
            result = result.concat("?start=" + m_start);
            if (m_max >= 0) {
                result = result.concat("&max=" + m_max);
            }
        }
        else if (m_max >= 0) {
            result = result.concat("?max=" + m_max);
        }

        return result;
    }
}
