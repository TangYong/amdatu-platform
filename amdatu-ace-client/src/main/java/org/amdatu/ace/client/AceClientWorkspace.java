/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

import org.amdatu.ace.client.model.AbstractAction;
import org.amdatu.ace.client.model.AbstractResource;
import org.amdatu.ace.client.model.AuditEvent;
import org.amdatu.ace.client.model.GetAuditEventsAction;
import org.amdatu.ace.client.model.Model;
import org.amdatu.ace.client.model.Resource;
import org.amdatu.ace.client.model.Target;

/**
 * Client implementation of a workspace that provides CRUD on resources and import/export methods.
 */
public class AceClientWorkspace {

    private final AceClient m_client;
    private final String m_endpoint;

    /**
     * Creates a new {@link AceClientWorkspace} instance.
     * 
     * @param client the {@link AceClient} instance to use;
     * @param endpoint the endpoint of this workspace to use.
     */
    public AceClientWorkspace(AceClient client, String endpoint) {
        m_client = client;
        m_endpoint = endpoint;
    }

    /**
     * Commits all pending changes to Ace.
     * 
     * @return <code>true</code> if the commit succeeded, <code>false</code> if the commit failed.
     * @throws AceClientException in case the commit failed.
     */
    public boolean commit() throws AceClientException {
        return invokePost(m_endpoint, null, new BaseHttpResponseHandler<Boolean>() {
            @Override
            public Boolean convertResponse(int responseCode, String responseMessage, String ignored)
                throws AceClientException {
                if (HttpURLConnection.HTTP_CONFLICT == responseCode) {
                    return false;
                }
                if (HttpURLConnection.HTTP_OK != responseCode) {
                    throw new AceClientRestException("Failed to commit workspace for endpoint: " + m_endpoint,
                        responseCode, responseMessage);
                }
                return true;
            }
        });
    }

    /**
     * Creates a single resource in Ace.
     * 
     * @param object the resource to create, cannot be <code>null</code>.
     * @return the identifier of the created resource, never <code>null</code>.
     * @throws IllegalArgumentException in case the given resource was <code>null</code>;
     * @throws AceClientException in case the creation failed.
     */
    public <T extends AbstractResource> String createResource(T object) throws AceClientException {
        if (object == null) {
            throw new IllegalArgumentException("Resource cannot be null!");
        }
        if (object.getDefinition() != null) {
            throw new IllegalArgumentException("Cannot create existing resource!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(object);

        String endpoint = createResourceEndpoint(object);
        String data = AceClientUtil.toJson(object);

        return invokePost(endpoint, data, new BaseHttpResponseHandler<String>() {
            @Override
            public String convertResponse(HttpURLConnection connection, String responseContent) throws IOException,
                AceClientException {
                int responseCode = connection.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_MOVED_TEMP) {
                    throw new AceClientRestException("Error creating resource!", responseCode, connection
                        .getResponseMessage());
                }

                String location = connection.getHeaderField("Location");
                return URLDecoder.decode(location.substring(location.lastIndexOf("/") + 1), "UTF-8");
            }
        });
    }

    /**
     * Updates a single resource in Ace.
     * 
     * @param object the resource to update, cannot be <code>null</code>.
     * @return the identifier of the updated resource, never <code>null</code>.
     * @throws IllegalArgumentException in case the given resource was <code>null</code>;
     * @throws AceClientException in case the update failed.
     */
    public <T extends AbstractResource> String updateResource(T object) throws AceClientException {
        if (object == null) {
            throw new IllegalArgumentException("Resource cannot be null!");
        }
        if (object.getDefinition() == null) {
            throw new IllegalArgumentException("Resource must already exist to be updated!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(object);

        String endpoint = createResourceEndpoint(object);
        String data = AceClientUtil.toJson(object);

        return invokePut(endpoint, data, new BaseHttpResponseHandler<String>() {
            @Override
            public String convertResponse(HttpURLConnection connection, String responseContent) throws IOException,
                AceClientException {
                int responseCode = connection.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_MOVED_TEMP) {
                    throw new AceClientRestException("Error creating resource!", responseCode, connection
                        .getResponseMessage());
                }

                String location = connection.getHeaderField("Location");
                return URLDecoder.decode(location.substring(location.lastIndexOf("/") + 1), "UTF-8");
            }
        });
    }

    /**
     * Updates a set of resources.
     * 
     * @param objects the resources to update, should be at least one resource.
     * @return the identifiers of the updated resources, never <code>null</code>. The identifiers returned are in the same order as the given resources.
     * @throws IllegalArgumentException in case the given resources was <code>null</code> or an empty array;
     * @throws AceClientException in case one of the updates failed.
     */
    public <T extends AbstractResource> String[] updateResources(T... objects) throws AceClientException {
        if (objects == null || objects.length < 1) {
            throw new IllegalArgumentException("At least one resource should be given!");
        }

        String[] resourceIds = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            resourceIds[i] = updateResource(objects[i]);
        }

        return resourceIds;
    }

    /**
     * Creates a set of resources.
     * 
     * @param objects the resources to create, should be at least one resource.
     * @return the identifiers of the created resources, never <code>null</code>. The identifiers returned are in the same order as the given resources.
     * @throws IllegalArgumentException in case the given resources was <code>null</code> or an empty array;
     * @throws AceClientException in case the creation failed.
     */
    public <T extends AbstractResource> String[] createResources(T... objects) throws AceClientException {
        if (objects == null || objects.length < 1) {
            throw new IllegalArgumentException("At least one resource should be given!");
        }

        String[] resourceIds = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            resourceIds[i] = createResource(objects[i]);
        }

        return resourceIds;
    }

    /**
     * Deletes a single resource from Ace.
     * 
     * @param object the resource to delete, cannot be <code>null</code>.
     * @throws IllegalArgumentException in case the given resource type or identifier was <code>null</code>;
     * @throws AceClientException in case the deletion failed.
     */
    public <T extends AbstractResource> void deleteResource(T object) throws AceClientException {
        if (object == null) {
            throw new IllegalArgumentException("Resource cannot be null!");
        }
        if (object.getDefinition() == null) {
            throw new IllegalArgumentException("Resource must exist to be deleted!");
        }

        deleteResource(object.getClass(), object.getDefinition());
    }

    /**
     * Deletes a single resource from Ace.
     * 
     * @param resourceClass the type of resource to delete, cannot be <code>null</code>;
     * @param resourceId the identifier of the resource to delete, cannot be <code>null</code> or empty.
     * @throws IllegalArgumentException in case the given resource type or identifier was <code>null</code>;
     * @throws AceClientException in case the deletion failed.
     */
    public <T extends AbstractResource> void deleteResource(Class<T> resourceClass, String resourceId)
        throws AceClientException {
        if (resourceClass == null) {
            throw new IllegalArgumentException("Resource class cannot be null!");
        }
        if (resourceId == null || "".equals(resourceId.trim())) {
            throw new IllegalArgumentException("Resource ID cannot be null or empty!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(resourceClass);

        String endpoint = createResourceEndpoint(resourceClass, resourceId);

        invokeDelete(endpoint, new BaseHttpResponseHandler<Void>() {
            @Override
            public Void convertResponse(int responseCode, String responseMessage, String ignored)
                throws AceClientException {
                if (HttpURLConnection.HTTP_OK != responseCode) {
                    throw new AceClientRestException("Failed to delete resource!", responseCode, responseMessage);
                }
                return null;
            }
        });
    }

    /**
     * Convenience method to delete all existing resources of a given type from Ace.
     * 
     * @param resourceClass the type of resource to delete, cannot be <code>null</code>.
     * @throws IllegalArgumentException in case the given resource type was <code>null</code>;
     * @throws AceClientException in case the deletion failed.
     */
    public <T extends AbstractResource> void deleteResources(Class<T> resourceClass) throws AceClientException {
        String[] ids = getResourceIds(resourceClass);
        for (String id : ids) {
            deleteResource(resourceClass, id);
        }
    }

    /**
     * Exports the complete model from Ace.
     * 
     * @return the exported model, never <code>null</code>.
     * @throws AceClientException in case the export failed.
     */
    public Model export() throws AceClientException {
        Model model = new Model();
        for (Class<? extends AbstractResource> c : AceClient.RESOURCES_CLASSES) {
            model.addResources(getResources(c));
        }
        return model;
    }

    /**
     * Returns the {@link AceClient} instance.
     * 
     * @return the {@link AceClient} instance, never <code>null</code>.
     */
    public AceClient getClient() {
        return m_client;
    }

    /**
     * Returns the Ace endpoint of this particular workspace.
     * 
     * @return a workspace endpoint (URL), never <code>null</code>.
     */
    public String getEndpoint() {
        return m_endpoint;
    }

    /**
     * Returns a list of up to 100 audit events for the given target, sorted in descending order.
     * 
     * @param target the target to retrieve the audit events for, cannot be <code>null</code>.
     * @return a list of audit events, never <code>null</code>, but can be empty.
     * @throws IllegalArgumentException in case the given target was <code>null</code>;
     * @throws AceClientException in case the retrieval of audit events failed.
     */
    public AuditEvent[] getAuditEvents(Target target) throws AceClientException {
        return getAuditEvents(target, -1, -1);
    }

    /**
     * Returns a list of up to 100 audit events for the given target, sorted in descending order.
     * 
     * @param target the target to retrieve the audit events for, cannot be <code>null</code>;
     * @param start the start number of the audit event to return;
     * @param max the maximum number of audit events to return.
     * @return a list of audit events, never <code>null</code>, but can be empty.
     * @throws IllegalArgumentException in case the given target was <code>null</code>;
     * @throws AceClientException in case the retrieval of audit events failed.
     */
    public AuditEvent[] getAuditEvents(Target target, int start, int max) throws AceClientException {
        String json = invokeResourceAction(target, new GetAuditEventsAction(start, max));
        return AceClientUtil.fromJson(json, AuditEvent[].class);
    }

    /**
     * Fetches a single resource from Ace.
     * 
     * @param resourceClass the type of resource to retrieve, cannot be <code>null</code>;
     * @param resourceId the identifier of the resource to retrieve, cannot be <code>null</code> or empty.
     * @return the resource, never <code>null</code>.
     * @throws IllegalArgumentException in case the given resource type or identifier was <code>null</code>;
     * @throws AceClientException in case the retrieval of the resource failed.
     */
    public <T extends AbstractResource> T getResource(Class<T> resourceClass, String resourceId)
        throws AceClientException {
        if (resourceClass == null) {
            throw new IllegalArgumentException("Resource class cannot be null!");
        }
        if (resourceId == null || resourceId.trim().equals("")) {
            throw new IllegalArgumentException("Resource ID cannot be null or empty!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(resourceClass);

        String endpoint = createResourceEndpoint(resourceClass, resourceId);
        String data = getRepresentation(endpoint);

        return AceClientUtil.fromJson(data, resourceClass);
    }

    /**
     * Fetches all existing resource identifiers of a given type from Ace.
     * 
     * @param resourceClass the type of resource to retrieve all identifiers for, cannot be <code>null</code>.
     * @return an array with resource identifiers, never <code>null</code>, but can be empty.
     * @throws IllegalArgumentException in case the given resource type was <code>null</code>;
     * @throws AceClientException in case the retrieval of the resource failed.
     */
    public <T extends AbstractResource> String[] getResourceIds(Class<T> resourceClass) throws AceClientException {
        if (resourceClass == null) {
            throw new IllegalArgumentException("Resource class cannot be null!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(resourceClass);

        String endpoint = createResourceEndpoint(resourceClass, null);
        String data = getRepresentation(endpoint);
        String[] resourceIds = AceClientUtil.fromJson(data, String[].class);
        for (int i = 0; i < resourceIds.length; i++) {
            resourceIds[i] = AceClientUtil.decodeId(resourceIds[i]);
        }

        return resourceIds;
    }

    /**
     * Fetches all resources of a given type from Ace.
     * 
     * @param resourceClass the type of resource to retrieve, cannot be <code>null</code>.
     * @return an array with resources, never <code>null</code>, but can be empty.
     * @throws IllegalArgumentException in case the given resource type was <code>null</code>;
     * @throws AceClientException in case the retrieval of the resource failed.
     */
    public <T extends AbstractResource> T[] getResources(Class<T> resourceClass) throws AceClientException {
        if (resourceClass == null) {
            throw new IllegalArgumentException("Resource class cannot be null!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(resourceClass);

        String[] resourceIds = getResourceIds(resourceClass);

        T[] resources = (T[]) Array.newInstance(resourceClass, resourceIds.length);
        for (int i = 0; i < resourceIds.length; i++) {
            String endpoint = createResourceEndpoint(resourceClass, resourceIds[i]);
            String data = getRepresentation(endpoint);
            resources[i] = AceClientUtil.fromJson(data, resourceClass);
        }

        return resources;
    }

    /**
     * Imports a complete model into Ace.
     * 
     * @param model the model to import, cannot be <code>null</code>.
     * @throws IllegalArgumentException in case the given model was <code>null</code>;
     * @throws AceClientException in case the import failed.
     */
    public void importModel(Model model) throws AceClientException {
        if (model == null) {
            throw new IllegalArgumentException("Model to import cannot be null!");
        }
        for (Class<? extends AbstractResource> c : AceClient.RESOURCES_CLASSES) {
            createResources(model.getResources(c));
        }
    }

    /**
     * Invokes an action on a given resource.
     * <p>
     * The action is invoked by POSTing a empty request to an endpoint like:
     * <tt>/path/to/resource-name/resource-ID/action-name</tt>.
     * </p>
     * 
     * @param resource the resource to invoke the action on;
     * @param action the name of the action to invoke.
     * @return the raw (JSON) result of the action.
     * @throws AceClientException in case the invocation failed.
     */
    public <T extends AbstractResource, A extends AbstractAction> String invokeResourceAction(final T resource,
        final A action) throws AceClientException {
        if (resource == null) {
            throw new IllegalArgumentException("Cannot invoke action on null resource!");
        }
        if (resource.getDefinition() == null) {
            throw new IllegalArgumentException("Cannot invoke action on non-existing resource!");
        }
        if (action == null) {
            throw new IllegalArgumentException("Cannot invoke null action on resource!");
        }
        // Verify whether the given resource is actually known...
        verifyResource(resource);

        final String actionPath = action.path();

        String endPoint = createResourceActionEndpoint(resource, actionPath);

        return invokeHttpEndpoint(endPoint, action.getMethod(), action.getPayload(),
            new BaseHttpResponseHandler<String>() {
                @Override
                public String convertResponse(int responseCode, String responseMessage, String result)
                    throws AceClientException {
                    if (HttpURLConnection.HTTP_OK != responseCode) {
                        throw new AceClientRestException("Failed to invoke action (" + actionPath + ") on: "
                            + resource.getDefinition(), responseCode, responseMessage);
                    }
                    return result;
                }
            });
    }

    /**
     * Removes the current workspace ignoring any pending/uncommitted changes.
     * 
     * @throws AceClientException in case the remove failed.
     */
    public void remove() throws AceClientException {
        invokeDelete(m_endpoint, new BaseHttpResponseHandler<Void>() {
            @Override
            public Void convertResponse(int responseCode, String responseMessage, String ignored)
                throws AceClientException {
                if (HttpURLConnection.HTTP_OK != responseCode) {
                    throw new AceClientRestException("Failed to delete workspace for endpoint: " + m_endpoint,
                        responseCode, responseMessage);
                }
                return null;
            }
        });
    }

    /**
     * @param endpoint
     * @return The representation of the endpoint
     * @throws AceClientException
     */
    protected String getRepresentation(String endpoint) throws AceClientException {
        return invokeGet(endpoint, new BaseHttpResponseHandler<String>() {
            @Override
            public String convertResponse(int responseCode, String responseMsg, String json) throws AceClientException {
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    throw new AceClientRestException("Error getting resource!", responseCode, responseMsg);
                }
                return json;
            }
        });
    }

    protected <T> T invokeGet(String endpoint, HttpResponseHandler<T> callback) throws AceClientException {
        return invokeHttpEndpoint(endpoint, "GET", null, callback);
    }

    protected <T> T invokeDelete(String endpoint, HttpResponseHandler<T> callback) throws AceClientException {
        return invokeHttpEndpoint(endpoint, "DELETE", null, callback);
    }

    protected <T> T invokePost(String endpoint, String jsonContent, HttpResponseHandler<T> callback)
        throws AceClientException {
        return invokeHttpEndpoint(endpoint, "POST", jsonContent, callback);
    }

    protected <T> T invokePut(String endpoint, String jsonContent, HttpResponseHandler<T> callback)
        throws AceClientException {
        return invokeHttpEndpoint(endpoint, "PUT", jsonContent, callback);
    }

    /**
     * Invokes an HTTP endpoint.
     * 
     * @param endpoint the HTTP endpoint (= URL) to invoke;
     * @param method the HTTP method to use (GET/POST).
     * @return the result body of the HTTP invocation.
     * @throws AceClientException in case of errors invoking the HTTP endpoint.
     */
    private <T> T
        invokeHttpEndpoint(String endpoint, String method, String jsonContent, HttpResponseHandler<T> callback)
            throws AceClientException {
        HttpURLConnection connection = null;
        InputStream is = null;
        OutputStream os = null;

        try {
            URL endpointUrl = new URL(endpoint);
            connection = (HttpURLConnection) endpointUrl.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(jsonContent != null);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(method);
            connection.setRequestProperty("Accept-Encoding", "application/json");

            if (jsonContent != null) {
                byte[] data = jsonContent.getBytes();

                connection.setRequestProperty("Content-Length", Integer.toString(data.length));

                os = connection.getOutputStream();
                os.write(data);
                os.flush();
                os.close();
                os = null;
            }

            is = connection.getInputStream();

            StringBuilder response = new StringBuilder();
            int c = -1;
            while ((c = is.read()) >= 0) {
                response.append((char) c);
            }

            is.close();
            is = null;

            return callback.convertResponse(connection, response.toString());
        }
        catch (MalformedURLException e) {
            throw new AceClientException("Failed to get data: " + e.getMessage(), e);
        }
        catch (IOException e) {
            throw new AceClientException("Failed to get data: " + e.getMessage(), e);
        }
        finally {
            closeSilently(os);
            closeSilently(is);

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Creates an REST endpoint for accessing a given resource.
     * 
     * @param resource the resource to access, cannot be <code>null</code>.
     * @return a REST endpoint, never <code>null</code>.
     */
    private <T extends AbstractResource> String createResourceEndpoint(T resource) {
        return createResourceEndpoint(resource.getClass(), resource.getDefinition());
    }

    /**
     * Creates an REST endpoint for accessing a given resource.
     * 
     * @param resourceClass the type of resource to access, cannot be <code>null</code>;
     * @param definition the definition of the resource, cannot be <code>null</code>.
     * @return a REST endpoint, never <code>null</code>.
     */
    private <T extends AbstractResource> String createResourceEndpoint(Class<T> resourceClass, String definition) {
        String path = resourceClass.getAnnotation(Resource.class).path();
        String result = m_endpoint + "/" + path;

        if (definition != null) {
            result = result.concat("/").concat(AceClientUtil.encodeId(definition));
        }

        return result;
    }

    /**
     * Creates an REST endpoint for invoking a certain action on a given resource.
     * 
     * @param resource the resource to invoke the action on, cannot be <code>null</code>;
     * @param action the name of the action to invoke.
     * @return a REST endpoint, never <code>null</code>.
     */
    private <T extends AbstractResource> String createResourceActionEndpoint(T resource, String action) {
        String base = createResourceEndpoint(resource);
        return base + "/" + action;
    }

    /**
     * Silently closes a given resource.
     * 
     * @param resource the resource to close, can be <code>null</code>.
     */
    private void closeSilently(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            }
            catch (IOException ignored) {
                // Ignored; hopefully we don't have a leak...
            }
        }
    }

    /**
     * Helper method to determine whether the given object is a known resource.
     * 
     * @param object the object to test, cannot be <code>null</code>.
     * @throws AceClientException in case the given object is an unknown resource.
     */
    private <T extends AbstractResource> void verifyResource(T object) throws AceClientException {
        Class<? extends AbstractResource> resourceClass = object.getClass();
        verifyResource(resourceClass);
    }

    /**
     * Helper method to determine whether the given object-class is a known resource.
     * 
     * @param resourceClass the object-class to test, cannot be <code>null</code>.
     * @throws AceClientException in case the given object-class is an unknown resource.
     */
    private void verifyResource(Class<? extends AbstractResource> resourceClass) throws AceClientException {
        if (!AceClient.RESOURCES_CLASSES.contains(resourceClass)) {
            throw new AceClientException("Unknown resource class: " + resourceClass.getName());
        }
    }

    /**
     * Provides a base implementation of {@link HttpResponseHandler}.
     * 
     * @param <T> the type to return as (converted) response.
     */
    private class BaseHttpResponseHandler<T> implements HttpResponseHandler<T> {

        public T convertResponse(HttpURLConnection connection, String responseContent) throws IOException,
            AceClientException {
            return convertResponse(connection.getResponseCode(), connection.getResponseMessage(), responseContent);
        }

        public T convertResponse(int responseCode, String responseMessage, String responseContent) throws IOException,
            AceClientException {
            return null;
        }
    }

    /**
     * Defines a handler for converting HTTP responses to a concrete object representation.
     * 
     * @param <T> the type to return as (converted) response.
     */
    private interface HttpResponseHandler<T> {

        /**
         * @param connection the connection to handle;
         * @param responseContent the content of the response to handle.
         * @return the converted response, can be <code>null</code>;
         * @throws AceClientException in case the response code was unexpected.
         */
        T convertResponse(HttpURLConnection connection, String responseContent) throws IOException, AceClientException;
    }
}
