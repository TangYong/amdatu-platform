/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

/**
 * Denotes the state of {@link Target} at the moment it was retrieved from
 * the server.
 * <p>
 * A target is always composed of some additional state information not
 * present in the other resources. This information is part of the target
 * and always retrieved with a target (hence it is not a resource on its
 * own).
 * </p>
 * 
 * @see Target
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TargetState {

    // @checkstyle: off
    private String registrationState;
    private String provisioningState;
    private String storeState;
    private String currentVersion;
    private boolean isRegistered;
    private boolean needsApproval;
    private boolean autoApprove;
    private String[] artifactsFromShop;
    private String[] artifactsFromDeployment;
    private String lastInstallVersion;
    private boolean lastInstallSuccess;

    // @checkstyle : on

    /**
     * @return the artifactsFromDeployment
     */
    public String[] getArtifactsFromDeployment() {
        return artifactsFromDeployment;
    }

    /**
     * @return the artifactsFromShop
     */
    public String[] getArtifactsFromShop() {
        return artifactsFromShop;
    }

    /**
     * @return the currentVersion
     */
    public String getCurrentVersion() {
        return currentVersion;
    }

    /**
     * @return the lastInstallVersion
     */
    public String getLastInstallVersion() {
        return lastInstallVersion;
    }

    /**
     * @return the provisioningState
     */
    public String getProvisioningState() {
        return provisioningState;
    }

    /**
     * @return the registrationState
     */
    public String getRegistrationState() {
        return registrationState;
    }

    /**
     * @return the storeState
     */
    public String getStoreState() {
        return storeState;
    }

    /**
     * @return the autoApprove
     */
    public boolean isAutoApprove() {
        return autoApprove;
    }

    /**
     * @return the lastInstallSuccess
     */
    public boolean isLastInstallSuccess() {
        return lastInstallSuccess;
    }

    /**
     * @return the needsApproval
     */
    public boolean isNeedsApproval() {
        return needsApproval;
    }

    /**
     * @return the isRegistered
     */
    public boolean isRegistered() {
        return isRegistered;
    }
}