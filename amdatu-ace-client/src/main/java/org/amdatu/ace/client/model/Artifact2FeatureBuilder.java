/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import static org.amdatu.ace.client.model.AbstractAssociation.ATTR_LEFTCARDINALITY;
import static org.amdatu.ace.client.model.AbstractAssociation.ATTR_LEFTENDPOINT;
import static org.amdatu.ace.client.model.AbstractAssociation.ATTR_RIGHTCARDINALITY;
import static org.amdatu.ace.client.model.AbstractAssociation.ATTR_RIGHTENDPOINT;

import org.amdatu.ace.client.AceClientException;

public class Artifact2FeatureBuilder extends AbstractBuilder<Artifact2FeatureBuilder, Artifact2Feature> {

    public Artifact2FeatureBuilder() {
        super(new Artifact2Feature());
    }

    @Override
    public Artifact2Feature build() throws AceClientException {
        // TODO check required and put defaults
        return m_instance;
    }

    @Override
    protected Artifact2FeatureBuilder getThis() {
        return this;
    }

    public Artifact2FeatureBuilder setLeftEndpoint(String leftEndpoint) {
        return setAttribute(ATTR_LEFTENDPOINT, leftEndpoint);
    }

    public Artifact2FeatureBuilder setLeftCardinality(String leftCardinality) {
        return setAttribute(ATTR_LEFTCARDINALITY, leftCardinality);
    }

    public Artifact2FeatureBuilder setRightEndpoint(String rightEndpoint) {
        return setAttribute(ATTR_RIGHTENDPOINT, rightEndpoint);
    }

    public Artifact2FeatureBuilder setRightCardinality(String rightCardinality) {
        return setAttribute(ATTR_RIGHTCARDINALITY, rightCardinality);
    }
}
