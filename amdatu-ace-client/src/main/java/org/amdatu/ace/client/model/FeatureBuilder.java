/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import static org.amdatu.ace.client.model.Feature.ATTR_DESCRIPTION;
import static org.amdatu.ace.client.model.Feature.ATTR_NAME;

import org.amdatu.ace.client.AceClientException;

public class FeatureBuilder extends AbstractBuilder<FeatureBuilder, Feature> {

    public FeatureBuilder() {
        super(new Feature());

    }

    @Override
    public Feature build() throws AceClientException {
        // TODO check required and put defaults
        return m_instance;
    }

    @Override
    protected FeatureBuilder getThis() {
        return this;
    }

    public FeatureBuilder setName(String name) {
        return setAttribute(ATTR_NAME, name);
    }

    public FeatureBuilder setDescription(String description) {
        return setAttribute(ATTR_DESCRIPTION, description);
    }
}
