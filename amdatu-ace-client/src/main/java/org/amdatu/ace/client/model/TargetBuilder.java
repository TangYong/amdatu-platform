/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import static org.amdatu.ace.client.model.Target.ATTR_AUTOAPPROVE;
import static org.amdatu.ace.client.model.Target.ATTR_ID;

import org.amdatu.ace.client.AceClientException;

/**
 * Provides a builder for targets.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TargetBuilder extends AbstractBuilder<TargetBuilder, Target> {

    /**
     * Creates a new {@link TargetBuilder} instance.
     */
    public TargetBuilder() {
        super(new Target());

    }

    /**
     * @see org.amdatu.ace.client.model.AbstractBuilder#build()
     */
    @Override
    public Target build() throws AceClientException {
        assertMandatoryId();
        return m_instance;
    }

    /**
     * Sets the identifier of the target.
     * 
     * @param id the new identifier to set, cannot be <code>null</code> or empty.
     * @return this builder.
     */
    public TargetBuilder setId(String id) {
        return setAttribute(ATTR_ID, id);
    }

    /**
     * Sets whether or not the target should auto-approve all updates.
     * 
     * @param autoApprove <code>true</code> if the target should auto approve all new versions, <code>false</code>
     *        if the target should manually approve each new version.
     * @return this builder.
     */
    public TargetBuilder setAutoApprove(boolean autoApprove) {
        return setAttribute(ATTR_AUTOAPPROVE, autoApprove ? "true" : "false");
    }

    /**
     * @see org.amdatu.ace.client.model.AbstractBuilder#getThis()
     */
    @Override
    protected TargetBuilder getThis() {
        return this;
    }

    private void assertMandatoryId() throws AceClientException {
        String id = m_instance.getId();
        if (id == null || "".equals(id.trim())) {
            throw new AceClientException("Target ID should be set!");
        }
    }
}
