/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.amdatu.ace.client.AceClientException;

/**
 * Denotes an action that can be invoked on a resource.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public abstract class AbstractAction {

    private final String m_method;
    private String m_payload;

    /**
     * Creates a new {@link AbstractAction} instance for POSTing an empty payload.
     */
    protected AbstractAction() {
        this("POST");
    }

    /**
     * Creates a new {@link AbstractAction} instance for a given method with an empty payload.
     */
    protected AbstractAction(String method) {
        this(method, null);
    }

    /**
     * Creates a new {@link AbstractAction} instance for a given method and payload.
     * 
     * @param method the HTTP-method to use;
     * @param payload the (optional) payload to use.
     */
    protected AbstractAction(String method, String payload) {
        if (method == null || "".equals(method.trim())) {
            throw new IllegalArgumentException("Method cannot be null or empty!");
        }

        m_method = method;
        m_payload = payload;
    }

    /**
     * Returns the method of this action.
     * 
     * @return the method, never <code>null</code>.
     */
    public final String getMethod() {
        return m_method;
    }

    /**
     * Returns the optional payload of this action.
     * 
     * @return the payload, can be <code>null</code>.
     */
    public final String getPayload() {
        return m_payload;
    }

    /**
     * Returns the (relative) path where to invoke this action.
     * 
     * @return the path of this action, like "approve", without leading or trailing slashes. Cannot be <code>null</code>.
     * @throws AceClientException in case no proper path is defined for this action.
     */
    public final String path() throws AceClientException {
        String result = getPath();

        if (result == null || "".equals(result.trim())) {
            throw new AceClientException("Action " + getClass().getName() + " does not define a proper path!");
        }

        return result;
    }

    /**
     * Returns the (relative) path where to invoke this action.
     * 
     * @return the path of this action, like "approve", without leading or trailing slashes. Cannot be <code>null</code>.
     */
    protected String getPath() {
        String result = null;

        Action annotation = getClass().getAnnotation(Action.class);
        if (annotation != null) {
            result = annotation.path();
        }

        return result;
    }
}
