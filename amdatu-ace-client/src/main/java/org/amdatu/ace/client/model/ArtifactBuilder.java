/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import static org.amdatu.ace.client.model.Artifact.ATTR_BUNDLE_NAME;
import static org.amdatu.ace.client.model.Artifact.ATTR_BUNDLE_SYMBOLICNAME;
import static org.amdatu.ace.client.model.Artifact.ATTR_BUNDLE_VERSION;
import static org.amdatu.ace.client.model.Artifact.ATTR_CONFIG_FILENAME;
import static org.amdatu.ace.client.model.Artifact.ATTR_DESCRIPTION;
import static org.amdatu.ace.client.model.Artifact.ATTR_MIMETYPE;
import static org.amdatu.ace.client.model.Artifact.ATTR_NAME;
import static org.amdatu.ace.client.model.Artifact.ATTR_PROCESSORPID;
import static org.amdatu.ace.client.model.Artifact.ATTR_URL;
import static org.amdatu.ace.client.model.Artifact.MIMETYPE_BUNDLE;
import static org.amdatu.ace.client.model.Artifact.MIMETYPE_CONFIG;

import org.amdatu.ace.client.AceClientException;

public class ArtifactBuilder extends AbstractBuilder<ArtifactBuilder, Artifact> {

    public ArtifactBuilder() {
        super(new Artifact());
    }

    @Override
    public Artifact build() throws AceClientException {
        // TODO check required and put defaults
        return m_instance;
    }

    @Override
    protected ArtifactBuilder getThis() {
        return this;
    }

    public ArtifactBuilder setName(String artifactName) {
        return setAttribute(ATTR_NAME, artifactName);
    }

    public ArtifactBuilder setDescription(String artifactDescription) {
        return setAttribute(ATTR_DESCRIPTION, artifactDescription);
    }

    public ArtifactBuilder setUrl(String url) {
        return setAttribute(ATTR_URL, url);
    }

    public ArtifactBuilder setMimeType(String mimetype) {
        return setAttribute(ATTR_MIMETYPE, mimetype);
    }

    public ArtifactBuilder setProcessorPid(String processorPid) {
        return setAttribute(ATTR_PROCESSORPID, processorPid);
    }

    public ArtifactBuilder setBundleName(String bundleName) {
        return setAttribute(ATTR_BUNDLE_NAME, bundleName);
    }

    public ArtifactBuilder setBundleSymbolicName(String bundleSymbolicName) {
        return setAttribute(ATTR_BUNDLE_SYMBOLICNAME, bundleSymbolicName);
    }

    public ArtifactBuilder setBundleVersion(String bundleVersion) {
        return setAttribute(ATTR_BUNDLE_VERSION, bundleVersion);
    }

    public ArtifactBuilder setConfigFileName(String configFileName) {
        return setAttribute(ATTR_CONFIG_FILENAME, configFileName);
    }

    public ArtifactBuilder isBundle() {
        return setAttribute(ATTR_MIMETYPE, MIMETYPE_BUNDLE);
    }

    public ArtifactBuilder isConfig() {
        return setAttribute(ATTR_MIMETYPE, MIMETYPE_CONFIG);
    }
}
