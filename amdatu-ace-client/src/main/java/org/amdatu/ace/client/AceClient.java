/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.amdatu.ace.client.model.AbstractResource;
import org.amdatu.ace.client.model.Artifact;
import org.amdatu.ace.client.model.Artifact2Feature;
import org.amdatu.ace.client.model.Distribution;
import org.amdatu.ace.client.model.Distribution2Target;
import org.amdatu.ace.client.model.Feature;
import org.amdatu.ace.client.model.Feature2Distribution;
import org.amdatu.ace.client.model.Target;

/**
 * Main class of the Ace Client that creates workspaces and provide utility methods.
 */
public class AceClient {

    public static final List<Class<? extends AbstractResource>> RESOURCES_CLASSES =
        new LinkedList<Class<? extends AbstractResource>>();

    static {
        RESOURCES_CLASSES.add(Artifact.class);
        RESOURCES_CLASSES.add(Feature.class);
        RESOURCES_CLASSES.add(Distribution.class);
        RESOURCES_CLASSES.add(Target.class);
        RESOURCES_CLASSES.add(Artifact2Feature.class);
        RESOURCES_CLASSES.add(Feature2Distribution.class);
        RESOURCES_CLASSES.add(Distribution2Target.class);
    }

    private final String m_endpoint;

    public AceClient(String clientEndpoint) {
        m_endpoint = clientEndpoint;
    }

    public String getEndpoint() {
        return m_endpoint;
    }

    public AceClientWorkspace createNewWorkspace() throws AceClientException {
        try {
            URL url = new URL(m_endpoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            if (HttpURLConnection.HTTP_MOVED_TEMP != connection.getResponseCode()) {
                throw new AceClientException("Failed to create workspace for client: " + m_endpoint
                    + ". Response code is " + connection.getResponseCode() + ", response message is "
                    + connection.getResponseMessage());
            }
            String clientworkspaceEndpoint = connection.getHeaderField("Location");
            connection.disconnect();
            return new AceClientWorkspace(this, clientworkspaceEndpoint);
        }
        catch (MalformedURLException e) {
            throw new AceClientException("Failed to create workspace for client: " + m_endpoint
                + ". URL is malformed.", e);
        }
        catch (IOException e) {
            throw new AceClientException("Failed to create workspace for client: " + m_endpoint
                + ". Failed to open connection.", e);
        }
    }
}
