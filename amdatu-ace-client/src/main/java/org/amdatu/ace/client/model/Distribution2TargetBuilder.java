/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

import org.amdatu.ace.client.AceClientException;

public class Distribution2TargetBuilder extends
    AbstractAssociationBuilder<Distribution2TargetBuilder, Distribution2Target> {

    public Distribution2TargetBuilder() {
        super(new Distribution2Target());
    }

    @Override
    public Distribution2Target build() throws AceClientException {
        // TODO check required and put defaults
        return m_instance;
    }

    @Override
    protected Distribution2TargetBuilder getThis() {
        return this;
    }
}
