/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client;

/**
 * Defines a REST-specific exception.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AceClientRestException extends AceClientException {

    private static final long serialVersionUID = 5560345656176482268L;

    private final int m_responseCode;
    private final String m_responseMessage;

    /**
     * Creates a new {@link AceClientRestException} instance.
     * 
     * @param message the additional message of this exception;
     * @param responseCode the HTTP response code;
     * @param responseMessage the HTTP response message.
     */
    public AceClientRestException(String message, int responseCode, String responseMessage) {
        super(message);

        m_responseCode = responseCode;
        m_responseMessage = responseMessage;
    }

    /**
     * Returns the HTTP response code.
     * 
     * @return the HTTP response code, like 404 or 500.
     */
    public int getResponseCode() {
        return m_responseCode;
    }

    /**
     * Returns the HTTP response message.
     * 
     * @return the HTTP response message, can be <code>null</code>.
     */
    public String getResponseMessage() {
        return m_responseMessage;
    }
}
