/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.ace.client.model;

@Resource(path = "artifact")
public class Artifact extends AbstractResource {

    public static final String ATTR_NAME = "artifactName";
    public static final String ATTR_DESCRIPTION = "artifactDescription";
    public static final String ATTR_URL = "url";
    public static final String ATTR_MIMETYPE = "mimetype";
    public static final String ATTR_PROCESSORPID = "processorPid";

    public static final String ATTR_BUNDLE_NAME = "Bundle-Name";
    public static final String ATTR_BUNDLE_SYMBOLICNAME = "Bundle-SymbolicName";
    public static final String ATTR_BUNDLE_VERSION = "Bundle-Version";

    public static final String ATTR_CONFIG_FILENAME = "filename";

    public static final String MIMETYPE_BUNDLE = "application/vnd.osgi.bundle";
    public static final String MIMETYPE_CONFIG = "application/xml:osgi-autoconf";

    Artifact() {
        super();
    }

    public String getName() {
        return getAttribute(ATTR_NAME);
    }

    public String getDescription() {
        return getAttribute(ATTR_DESCRIPTION);
    }

    public String getUrl() {
        return getAttribute(ATTR_URL);
    }

    public String getMimetype() {
        return getAttribute(ATTR_MIMETYPE);
    }

    public String getProcessorPid() {
        return getAttribute(ATTR_PROCESSORPID);
    }

    public String getBundleName() {
        return getAttribute(ATTR_BUNDLE_NAME);
    }

    public String getBundleSymbolicName() {
        return getAttribute(ATTR_BUNDLE_SYMBOLICNAME);
    }

    public String getBundleVersion() {
        return getAttribute(ATTR_BUNDLE_VERSION);
    }

    public String getConfigFilename() {
        return getAttribute(ATTR_CONFIG_FILENAME);
    }

    public boolean isBundle() {
        return getAttribute(ATTR_MIMETYPE) != null && getAttribute(ATTR_MIMETYPE).equals(MIMETYPE_BUNDLE);
    }

    public boolean isConfig() {
        return getAttribute(ATTR_MIMETYPE) != null && getAttribute(ATTR_MIMETYPE).equals(MIMETYPE_CONFIG);
    }
}
