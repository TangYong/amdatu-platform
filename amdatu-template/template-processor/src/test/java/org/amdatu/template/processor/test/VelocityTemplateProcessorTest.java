/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor.test;

import static org.junit.Assert.assertArrayEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;

import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.amdatu.template.processor.velocity.VelocityTemplateContext;
import org.amdatu.template.processor.velocity.VelocityTemplateEngine;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class VelocityTemplateProcessorTest
{

    private static final String[] EXPECTED_OUTPUT = { "Bla bla lba beanValue", "", "${", "}", "${}", "A", "A",
        "${a.b}Bla bla", "blaa${a.b.c}Blabla", "${a.b.c}Ab ", "${c} ", " {a}", "zb", "velocity", "z" };

    private static final String LINE_DELIMITER = System.getProperty("line.separator");

    private static String INPUT_FILE_NAME;
    private static File INPUT_FILE;
    private static File BASE_DIR;
    private static URL INPUT_URL;
    private static Reader INPUT_READER;
    private static String INPUT_STRING;

    private Mockery m_mockery;
    private LogService m_logService;
    private BundleContext m_bundleContext;

    private TemplateContext m_context;
    private VelocityTemplateEngine m_eng;

    //

    @BeforeClass
    public static void setUpOnce() throws Exception {
        INPUT_FILE_NAME = "src/test/resources/template.txt";
        INPUT_FILE = new File(INPUT_FILE_NAME);
        BASE_DIR = File.createTempFile("abcd", "").getParentFile();
    }

    @Before
    public void init() throws Exception {

        INPUT_READER = new FileReader(INPUT_FILE);
        INPUT_URL = INPUT_FILE.toURI().toURL();
        INPUT_STRING = readInputFile();

        m_mockery = new Mockery();
        m_logService = m_mockery.mock(LogService.class);
        m_bundleContext = m_mockery.mock(BundleContext.class);
        m_mockery.checking(new Expectations() {
            {
                allowing(m_logService);
                allowing(m_bundleContext);
                will(returnValue(BASE_DIR));
            }
        });

        createContext();
    }

    @After
    public void reset() throws Exception {
        INPUT_READER.close();
    }

    @Test
    public void testGenerateString() throws TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc;
        String output;

        proc = eng.createProcessor(INPUT_FILE);
        output = proc.generateString(m_context);
        validate(output);

        proc = eng.createProcessor(INPUT_STRING);
        output = proc.generateString(m_context);
        validate(output);

        proc = eng.createProcessor(INPUT_READER);
        output = proc.generateString(m_context);
        validate(output);

        proc = eng.createProcessor(INPUT_URL);
        output = proc.generateString(m_context);
        validate(output);
    }

    @Test
    public void testGenerateFile() throws IOException, TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc;
        File outputFile = File.createTempFile("tet", ".txt");

        proc = eng.createProcessor(INPUT_FILE);
        proc.generateFile(m_context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_STRING);
        proc.generateFile(m_context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_READER);
        proc.generateFile(m_context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_URL);
        proc.generateFile(m_context, outputFile);
        validate(outputFile);
    }

    @Test
    public void testGenerateURL() throws IOException, TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc;
        URL outputURL;

        proc = eng.createProcessor(INPUT_FILE);
        outputURL = proc.generateURL(m_context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_STRING);
        outputURL = proc.generateURL(m_context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_READER);
        outputURL = proc.generateURL(m_context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_URL);
        outputURL = proc.generateURL(m_context);
        validate(outputURL);
    }

    @Test
    public void testGenerateStream() throws IOException, TemplateException {
        TemplateEngine eng = createEngine();
        TemplateProcessor proc;
        File outputFile = File.createTempFile("tet", ".txt");
        Writer writer;

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_FILE);
        proc.generateStream(m_context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_STRING);
        proc.generateStream(m_context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_READER);
        proc.generateStream(m_context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_URL);
        proc.generateStream(m_context, writer);
        validate(outputFile);
    }

    //

    private TemplateEngine createEngine() {
        m_eng = new VelocityTemplateEngine(m_logService, m_bundleContext);
        m_eng.init();
        return m_eng;
    }

    private void validate(String output) {
        String[] outputArray = output.split(LINE_DELIMITER);
        assertArrayEquals(EXPECTED_OUTPUT, outputArray);
    }

    private void validate(File outputFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(outputFile));
        validate(read(br));
    }

    private void validate(URL outputURL) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream) outputURL.getContent()));
        validate(read(br));
    }

    private String readInputFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        return read(br);
    }

    private String read(BufferedReader br) throws IOException {
        String line = br.readLine();
        String result = line;
        while ((line = br.readLine()) != null) {
            result += LINE_DELIMITER;
            result += line;
        }
        br.close();
        return result;
    }

    private void createContext() {
        m_context = new VelocityTemplateContext();
        m_context.put("a", "A");
        m_context.put("a.b", "AB");
        m_context.put("a.b.c", "ABC");
        m_context.put("newline", "\n");
        m_context.put("bean", new VelocityBean());
    }

}
