/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.template.processor;

import java.io.File;
import java.io.Writer;
import java.net.URL;

/**
 * The TemplateProcessor facilitates automatic replacement of configuration entries in static plain
 * text files (such as text, XML or RDF files). The manager will replace occurrences of
 * ${[configid]} whenever there is a configuration entry in the map available matching [configid].
 * If an entry is not found in the dictionary (context) specified it remains untouched. Several
 * generation methods are available.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TemplateProcessor {
    /**
     * Using the context specified the (in the engine) set template will be processed and a result
     * will be generated. This result will have all the configuration entries replaced as explained
     * in this interface's documentation. The result will be returned as a String.
     * 
     * @param context The context to use
     * @return The result as a String
     */
    String generateString(TemplateContext context) throws TemplateException;

    /**
     * Using the context specified the (in the engine) set template will be processed and a result
     * will be generated. This result will have all the configuration entries replaced as explained
     * in this interface's documentation. The result will be returned as an URL.
     * 
     * @param context The context to use
     * @return The result as an URL
     */
    URL generateURL(TemplateContext context) throws TemplateException;

    /**
     * Using the context specified the (in the engine) set template will be processed and a result
     * will be generated. This result will have all the configuration entries replaced as explained
     * in this interface's documentation. The result will be written in the target File.
     * 
     * @param context The context to use
     * @param targetFile The file to write the result to
     */
    void generateFile(TemplateContext context, File targetFile) throws TemplateException;

    /**
     * Using the context specified the (in the engine) set template will be processed and a result
     * will be generated. This result will have all the configuration entries replaced as explained
     * in this interface's documentation. The result will be streamed to the writer.
     * 
     * @param context The context to use
     * @param writer The writer the result will be streamed to
     */
    void generateStream(TemplateContext context, Writer writer) throws TemplateException;

}
