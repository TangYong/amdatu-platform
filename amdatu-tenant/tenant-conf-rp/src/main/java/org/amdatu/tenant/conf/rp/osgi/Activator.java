/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.conf.rp.osgi;

import java.util.Properties;

import org.amdatu.tenant.TenantFactoryConfiguration;
import org.amdatu.tenant.conf.rp.Processor;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.log.LogService;

/**
 * OSGi activator for the tenant factory configuratior resource processor.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, Processor.PID);
        manager.add(createComponent()
            .setInterface(ResourceProcessor.class.getName(), props)
            .setImplementation(Processor.class)
            .add(createServiceDependency()
                .setService(TenantFactoryConfiguration.class)
                .setCallbacks("add", null)
                .setRequired(false)
            )
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)
            )
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
