/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.conf.configadmin;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.amdatu.tenant.Constants;
import org.amdatu.tenant.TenantFactoryConfiguration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Configuration based for tenant factory configurator.
 *  
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Configurator implements ManagedServiceFactory, Runnable {
    private static final int DELAY = 2000;
    public static final String PID = "org.amdatu.tenant.factory";

    private volatile TenantFactoryConfiguration m_config;
    private volatile LogService m_log;
    private final Object m_lock = new Object();
    private final Map<String, Map<String, Object>> m_tenants = new HashMap<String, Map<String, Object>>();
    private ScheduledExecutorService m_executor;
    private ScheduledFuture<?> m_future;

    public void start() {
        m_executor = Executors.newSingleThreadScheduledExecutor();
    }

    public void stop() {
        m_executor.shutdown();
        try {
            m_executor.awaitTermination(DELAY, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException e) {
            m_log.log(LogService.LOG_WARNING, "Interrupted while waiting for executor to terminate.");
        }
    }

    public void run() {
        Map<String, Map<String, Object>> copy;
        synchronized (m_lock) {
            copy = copyOf(m_tenants);
        }
        m_config.update(copy);
    }

    public String getName() {
        return PID;
    }

    public void updated(String pid, Dictionary/* <String, Object> */properties) throws ConfigurationException {
        synchronized (m_lock) {
            m_tenants.put(pid, toMap(properties));
        }
        scheduleTask();
    }

    public void deleted(String pid) {
        synchronized (m_lock) {
            m_tenants.remove(pid);
        }
        scheduleTask();
    }

    private void scheduleTask() {
        // if we already had a task scheduled and it is still waiting, remove that task
        if (m_future != null && !m_future.isDone()) {
            m_future.cancel(false);
        }
        // schedule a task to execute in the future
        m_future = m_executor.schedule(this, DELAY, TimeUnit.MILLISECONDS);
    }

    private Map<String, Object> toMap(Dictionary properties) {
        Map<String, Object> map = new HashMap<String, Object>(properties.size());
        Enumeration enumeration = properties.keys();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            map.put(key, properties.get(key));
        }
        return map;
    }

    private Map<String, Map<String, Object>> copyOf(Map<String, Map<String, Object>> tenants) {
        Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>(tenants);
        // right now, by design we always have a platform tenant, so add it if not yet provided
        if (!map.containsKey(Constants.PID_VALUE_PLATFORM)) {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(Constants.NAME_KEY, Constants.NAME_VALUE_PLATFORM);
            map.put(Constants.PID_VALUE_PLATFORM, props);
        }
        return map;
    }
}
