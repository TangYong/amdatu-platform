/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant;

/**
 * Compile time constants for multi tenancy.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface Constants {

    /** The service-property used to distinguish different {@link Tenant}s. */
    String PID_KEY = "org.amdatu.tenant.pid";
    /** An optional service-property used to give a logical name to a {@link Tenant}. */
    String NAME_KEY = "org.amdatu.tenant.name";
    /** The PID of the platform tenant. */
    String PID_VALUE_PLATFORM = "org.amdatu.tenant.PLATFORM";
    /** The (human readable) name of platform tenant. */
    String NAME_VALUE_PLATFORM = "Platform Tenant";

    /** Binding property in the life cycle listener service properties. */
    String MULTITENANT_LIFECYCLELISTENER_BINDING_KEY = "org.amdatu.tenant.binding";

    /** Binding bits for the life cycle listener service binding property. */
    int MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM = 0x00000001;
    /** Binding bits for the life cycle listener service binding property. */
    int MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS = 0x00000002;
    /** Binding bits for the life cycle listener service binding property. */
    int MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH = MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM
        | MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS;
}
