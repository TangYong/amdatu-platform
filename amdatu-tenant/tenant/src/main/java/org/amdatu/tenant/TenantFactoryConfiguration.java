/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant;

import java.util.Map;

/**
 * Service interface to provide the tenant factory with its configuration. The tenant
 * factory implements this interface and expects someone to provide this configuration.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface TenantFactoryConfiguration {
    /**
     * Whenever the set of tenants changes, invoke this method to notify the
     * factory. Just provide it with the "new" set tenants and the factory will
     * compute the delta.
     * 
     * @param tenants a map containing the tenant PIDs as its keys, and any
     *     tenant specific properties as its values
     */
    public void update(Map<String, Map<String, Object>> tenants);
}
