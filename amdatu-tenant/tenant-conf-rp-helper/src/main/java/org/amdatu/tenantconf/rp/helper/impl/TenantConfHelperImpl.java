/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenantconf.rp.helper.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.amdatu.tenant.Constants;
import org.amdatu.tenantconf.rp.helper.TenantConfHelper;
import org.apache.ace.client.repository.helper.ArtifactPreprocessor;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.helper.ArtifactResource;
import org.apache.ace.client.repository.object.ArtifactObject;

/**
 * Provides an {@link ArtifactRecognizer} for tenant configuration files.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantConfHelperImpl implements ArtifactRecognizer, TenantConfHelper {
    private static final String[] KEYS = {KEY_FILENAME};

    /**
     * {@inheritDoc}
     */
    public boolean canHandle(String mimetype) {
        return MIMETYPE.equals(mimetype);
    }

    /**
     * {@inheritDoc}
     */
    public boolean canUse(ArtifactObject object) {
        return MIMETYPE.equals(object.getMimetype());
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> checkAttributes(Map<String, String> attributes) {
        String pid = attributes.get(Constants.PID_KEY);
        if (pid == null || "".equals(pid.trim())) {
            throw new IllegalArgumentException("Tenant configuration seems to be missing property '" 
                            +  Constants.PID_KEY + "'");
        }
        return attributes;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> extractMetaData(ArtifactResource artifact) throws IllegalArgumentException {
        Map<String, String> result = new HashMap<String, String>();
        result.put(ArtifactObject.KEY_PROCESSOR_PID, PROCESSOR);
        result.put(ArtifactObject.KEY_MIMETYPE, MIMETYPE);

        String name = getName(artifact.getURL());
        String key = KEY_FILENAME + "-";
        int idx = name.indexOf(key);
        if (idx > -1) {
            int endIdx = name.indexOf("-", idx + key.length());
            name = name.substring(idx + key.length(), (endIdx > -1) ? endIdx : (name.length() - EXTENSION.length()));
        }

        result.put(ArtifactObject.KEY_ARTIFACT_NAME, name);
        result.put(KEY_FILENAME, name);

        Properties config = getConfiguration(artifact);
        result.put(Constants.PID_KEY, config.getProperty(Constants.PID_KEY));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    public <TYPE extends ArtifactObject> String getAssociationFilter(TYPE obj, Map<String, String> properties) {
        return "(" + KEY_FILENAME + "=" + obj.getAttribute(KEY_FILENAME) + ")";
    }

    /**
     * {@inheritDoc}
     */
    public <TYPE extends ArtifactObject> int getCardinality(TYPE obj, Map<String, String> properties) {
        return Integer.MAX_VALUE;
    }

    /**
     * {@inheritDoc}
     */
    public Comparator<ArtifactObject> getComparator() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public String[] getDefiningKeys() {
        return KEYS;
    }

    /**
     * {@inheritDoc}
     */
    public String getExtension(ArtifactResource artifact) {
        String name = getName(artifact.getURL());
        if ((name != null) && (name.toLowerCase().endsWith(EXTENSION))) {
            return EXTENSION;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public String[] getMandatoryAttributes() {
        return KEYS;
    }

    /**
     * {@inheritDoc}
     */
    public ArtifactPreprocessor getPreprocessor() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public String recognize(ArtifactResource artifact) {
        if (EXTENSION.equals(getExtension(artifact))) {
            return MIMETYPE;
        }
        return null;
    }

    /**
     * Returns the file name part of the given URL.
     * 
     * @param artifact the artifact to get the filename for, cannot be <code>null</code>.
     * @return the file name, never <code>null</code>.
     */
    private String getName(URL artifact) {
        return new File(artifact.getFile()).getName();
    }

    /**
     * Retrieves the actual configuration for the given artifact.
     * 
     * @param artifact the artifact to retrieve as {@link Properties}, cannot be <code>null</code>.
     * @return a {@link Properties} file with the configuration, cannot be <code>null</code>.
     */
    private Properties getConfiguration(ArtifactResource artifact) {
        InputStream is = null;
        try {
            is = artifact.openStream();
            Properties props = new Properties();
            props.load(is);
            return props;
        }
        catch (IOException exception) {
            throw new IllegalArgumentException("Invalid tenant configuration! Failed to read properties file.", exception);
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException exception) {
                    // Ignore; nothing we can do about this...
                }
            }
        }
    }
}
