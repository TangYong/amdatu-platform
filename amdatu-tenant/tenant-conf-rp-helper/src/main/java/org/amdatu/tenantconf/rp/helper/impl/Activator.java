/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amdatu.tenantconf.rp.helper.impl;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.tenantconf.rp.helper.TenantConfHelper;
import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * Provides the bundle activator.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    /**
     * {@inheritDoc}
     */
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Dictionary<String, String> props = new Hashtable<String, String>();
        props.put(ArtifactObject.KEY_MIMETYPE, TenantConfHelper.MIMETYPE);

        TenantConfHelperImpl helperImpl = new TenantConfHelperImpl();
        manager.add(createComponent()
            .setInterface(new String[]{ArtifactHelper.class.getName(), ArtifactRecognizer.class.getName()}, props)
            .setImplementation(helperImpl)
            );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // No-op.
    }
}
