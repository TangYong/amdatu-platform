/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.adapter;

import static org.amdatu.tenant.adapter.Constants.MULTITENANT_BUNDLE_ACTIVATOR_KEY;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_VERSION_KEY;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_VERSION_VALUE;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.osgi.framework.Constants;

/**
 * Wrapper that modifies an input stream that points to a bundle. The resulting input stream represents that
 * same bundle, but makes it multi tenant aware. It does this by replacing the original bundle activator with
 * one that will create an instance of the original bundle activator for each tenant in the system. This
 * tenant specific instance will receive an adapter to the real bundle context when its start() and stop()
 * life cycle methods are invoked. The adapter in turn does some magic to make the bundle work in a
 * multi tenant environment by:
 * <ul>
 * <li>Adding a tenant ID to every service that is registered, to make it tenant specific.</li>
 * <li>Filtering out any service that has a tenant ID property that is different from the current
 * tenant when the bundle queries for services or registers as a listener.</li>
 * <li>Providing each tenant with a data area that can be exclusively used by that tenant.</li>
 * </ul>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MultiTenantBundleInputStream extends InputStream {

    private static final Name MULTITENANT_VERSION_NAME = new Attributes.Name(MULTITENANT_VERSION_KEY);
    private static final Name BUNDLE_ACTIVATOR_NAME = new Attributes.Name(Constants.BUNDLE_ACTIVATOR);
    private static final String TENANT_IMPORT_PACKAGE = MultiTenantBundleActivator.class.getPackage().getName()
        + ";version=\"[1.0,2)\"";

    private static final int MAX_MANIFEST_SIZE = 65536;
    private static final int BUFFER_SIZE = 4096;
    private final InputStream m_inputStream;

    public MultiTenantBundleInputStream(InputStream original) {
        try {
            original = new BufferedInputStream(original);
            original.mark(MAX_MANIFEST_SIZE);
            JarInputStream jis = new JarInputStream(original);
            Manifest manifest = jis.getManifest();
            Attributes attributes = manifest.getMainAttributes();
            // do not process the bundle if it already has a tenant bundle activator (somebody already preprocessed it)
            // or it has no bundle activator at all (there's nothing to make tenant aware)
            if ((attributes.containsKey(MULTITENANT_VERSION_NAME) && (attributes.getValue(MULTITENANT_VERSION_NAME)
                .equals(MULTITENANT_VERSION_VALUE))) || !attributes.containsKey(BUNDLE_ACTIVATOR_NAME)) {
                original.reset();
                m_inputStream = original;
            }
            else {
                String originalBundleActivator = attributes.getValue(Constants.BUNDLE_ACTIVATOR);
                attributes.putValue(Constants.BUNDLE_ACTIVATOR, MultiTenantBundleActivator.class.getName());
                attributes.putValue(MULTITENANT_BUNDLE_ACTIVATOR_KEY, originalBundleActivator);
                attributes.putValue(MULTITENANT_VERSION_KEY, MULTITENANT_VERSION_VALUE);

                String originalImportPackage = attributes.getValue(Constants.IMPORT_PACKAGE);
                if (originalImportPackage == null) {
                    attributes.putValue(Constants.IMPORT_PACKAGE, TENANT_IMPORT_PACKAGE);
                }
                else {
                    attributes.putValue(Constants.IMPORT_PACKAGE, TENANT_IMPORT_PACKAGE + "," + originalImportPackage);
                }

                String originalBundleName = attributes.getValue(Constants.BUNDLE_NAME);
                attributes.putValue(Constants.BUNDLE_NAME, originalBundleName + " (MT)");

                ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);
                JarOutputStream jos = new JarOutputStream(baos, manifest);
                JarEntry entry = jis.getNextJarEntry();
                byte[] buffer = new byte[BUFFER_SIZE];
                while (entry != null) {
                    jos.putNextEntry(new JarEntry(entry.getName()));
                    int bytes = jis.read(buffer);
                    while (bytes != -1) {
                        jos.write(buffer, 0, bytes);
                        bytes = jis.read(buffer);
                    }
                    jos.closeEntry();
                    jis.closeEntry();
                    entry = jis.getNextJarEntry();
                }
                jos.flush();
                jos.close();
                baos.close();
                jis.close();
                m_inputStream = new ByteArrayInputStream(baos.toByteArray());
            }
        }
        catch (Exception e) {
            throw new RuntimeException("Could not wrap bundle to preprocess it.", e);
        }
    }

    @Override
    public int available() throws IOException {
        return m_inputStream.available();
    }

    @Override
    public void close() throws IOException {
        m_inputStream.close();
    }

    @Override
    public boolean equals(Object other) {
        return m_inputStream.equals(other);
    }

    @Override
    public int hashCode() {
        return m_inputStream.hashCode();
    }

    @Override
    public void mark(int readlimit) {
        m_inputStream.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
        return m_inputStream.markSupported();
    }

    @Override
    public int read() throws IOException {
        return m_inputStream.read();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return m_inputStream.read(b, off, len);
    }

    @Override
    public int read(byte[] b) throws IOException {
        return m_inputStream.read(b);
    }

    @Override
    public void reset() throws IOException {
        m_inputStream.reset();
    }

    @Override
    public long skip(long n) throws IOException {
        return m_inputStream.skip(n);
    }

    @Override
    public String toString() {
        return m_inputStream.toString();
    }
}
