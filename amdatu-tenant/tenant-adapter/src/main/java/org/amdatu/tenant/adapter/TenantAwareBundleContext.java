/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.adapter;

import static org.amdatu.tenant.Constants.PID_KEY;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Wrapper class for the bundle context interface to make it tenant aware.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantAwareBundleContext implements BundleContext {
    private final BundleDataStore m_bundleDataStore;

    /**
     * Provides a tenant-aware framework event.
     */
    private static class ScopedFrameworkEvent extends FrameworkEvent {

        private static final long serialVersionUID = 1L;

        public ScopedFrameworkEvent(FrameworkEvent event) {
            super(event.getType(), event.getBundle(), event.getThrowable());
        }

        /**
         * @see org.osgi.framework.FrameworkEvent#getBundle()
         */
        @Override
        public Bundle getBundle() {
            return new TenantAwareBundle(super.getBundle());
        }
    }

    /**
     * Provides a tenant-aware service registration.
     */
    private static class ServiceRegistrationAdapter implements ServiceRegistration {

        private final TenantAwareBundleContext m_context;
        private final ServiceRegistration m_registration;

        public ServiceRegistrationAdapter(TenantAwareBundleContext context, ServiceRegistration registration) {
            m_context = context;
            m_registration = registration;
        }

        /**
         * @see org.osgi.framework.ServiceRegistration#getReference()
         */
        public ServiceReference getReference() {
            return m_registration.getReference();
        }

        /**
         * @see org.osgi.framework.ServiceRegistration#setProperties(java.util.Dictionary)
         */
        public void setProperties(Dictionary properties) {
            m_registration.setProperties(m_context.getScopedProperties(m_registration, properties));
        }

        /**
         * @see org.osgi.framework.ServiceRegistration#unregister()
         */
        public void unregister() {
            m_context.unregisterService(this);
        }

        /**
         * @return the original service registration, never <code>null</code>.
         */
        public ServiceRegistration getRegistration() {
            return m_registration;
        }
    }

    /**
     * Provides a tenant-aware bundle event wrapper.
     */
    private static class TenantAwareBundleEvent extends BundleEvent {

        private static final long serialVersionUID = 1L;

        public TenantAwareBundleEvent(BundleEvent event) {
            super(event.getType(), event.getBundle());
        }

        /**
         * @see org.osgi.framework.BundleEvent#getBundle()
         */
        @Override
        public Bundle getBundle() {
            return new TenantAwareBundle(super.getBundle());
        }
    }

    /**
     * Provides a tenant-aware bundle listener.
     */
    private static class TenantAwareBundleListener implements BundleListener {

        private final BundleListener m_listener;

        public TenantAwareBundleListener(BundleListener listener) {
            m_listener = listener;
        }

        /**
         * @see org.osgi.framework.BundleListener#bundleChanged(org.osgi.framework.BundleEvent)
         */
        public void bundleChanged(BundleEvent event) {
            m_listener.bundleChanged(new TenantAwareBundleEvent(event));
        }
    }

    /**
     * Provides a tenant-aware framework listener.
     */
    private static class TenantAwareFrameworkListener implements FrameworkListener {

        private final FrameworkListener m_listener;

        public TenantAwareFrameworkListener(FrameworkListener listener) {
            m_listener = listener;
        }

        /**
         * @see org.osgi.framework.FrameworkListener#frameworkEvent(org.osgi.framework.FrameworkEvent)
         */
        public void frameworkEvent(FrameworkEvent event) {
            m_listener.frameworkEvent(new ScopedFrameworkEvent(event));
        }
    }

    private final ConcurrentMap<BundleListener, TenantAwareBundleListener> m_bundleListeners;
    private final ConcurrentMap<FrameworkListener, TenantAwareFrameworkListener> m_frameworkListeners;
    private final ConcurrentMap<ServiceRegistrationAdapter, ServiceRegistration> m_serviceRegistrations;
    private final BundleContext m_parentBundleContext;
    private final String m_identifier;
    private final String m_lookupFilter;
    private final Filter m_visibilityFilter;

    /**
     * Creates a new {@link TenantAwareBundleContext}.
     * 
     * @param bundleContext the original bundle context to wrap;
     * @param identifier the tenant-identifier to use to partition the service space;
     * @param bundleDataStore
     * @param lookupFilter the (optional) service lookup filter to use for this context;
     * @param visibilityFilter the (optional) visibility filter to use for this context.
     */
    public TenantAwareBundleContext(BundleContext bundleContext, String identifier, BundleDataStore bundleDataStore,
        String lookupFilter, Filter visibilityFilter) {
        if (bundleContext == null) {
            throw new IllegalArgumentException("BundleContext cannot be null!");
        }
        m_parentBundleContext = bundleContext;

        if (identifier == null || identifier.trim().isEmpty()) {
            throw new IllegalArgumentException("Identifier cannot be null or empty!");
        }
        m_identifier = identifier;

        m_bundleDataStore = bundleDataStore;

        if (lookupFilter != null) {
            m_lookupFilter = lookupFilter;
        }
        else {
            m_lookupFilter = "";
        }

        if (visibilityFilter != null) {
            m_visibilityFilter = visibilityFilter;
        }
        else {
            m_visibilityFilter = null;
        }

        m_bundleListeners = new ConcurrentHashMap<BundleListener, TenantAwareBundleListener>();
        m_frameworkListeners = new ConcurrentHashMap<FrameworkListener, TenantAwareFrameworkListener>();
        m_serviceRegistrations = new ConcurrentHashMap<ServiceRegistrationAdapter, ServiceRegistration>();
    }

    /**
     * @see org.osgi.framework.BundleContext#addBundleListener(org.osgi.framework.BundleListener)
     */
    public void addBundleListener(BundleListener listener) {
        TenantAwareBundleListener scopedBundleListener = new TenantAwareBundleListener(listener);
        if (m_bundleListeners.putIfAbsent(listener, scopedBundleListener) == null) {
            m_parentBundleContext.addBundleListener(scopedBundleListener);
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#addFrameworkListener(org.osgi.framework.FrameworkListener)
     */
    public void addFrameworkListener(FrameworkListener listener) {
        TenantAwareFrameworkListener scopedFrameworkListener = new TenantAwareFrameworkListener(listener);
        if (m_frameworkListeners.putIfAbsent(listener, scopedFrameworkListener) == null) {
            m_parentBundleContext.addFrameworkListener(scopedFrameworkListener);
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#addServiceListener(org.osgi.framework.ServiceListener)
     */
    public void addServiceListener(ServiceListener listener) {
        try {
            m_parentBundleContext.addServiceListener(listener, getScopedLookupFilter(null));
        }
        catch (InvalidSyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#addServiceListener(org.osgi.framework.ServiceListener, java.lang.String)
     */
    public void addServiceListener(ServiceListener listener, String filter) throws InvalidSyntaxException {
        m_parentBundleContext.addServiceListener(listener, getScopedLookupFilter(filter));
    }

    /**
     * @see org.osgi.framework.BundleContext#createFilter(java.lang.String)
     */
    public Filter createFilter(String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.createFilter(filter);
    }

    /**
     * @see org.osgi.framework.BundleContext#getAllServiceReferences(java.lang.String, java.lang.String)
     */
    public ServiceReference[] getAllServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.getAllServiceReferences(clazz, getScopedLookupFilter(filter));
    }

    /**
     * @see org.osgi.framework.BundleContext#getBundle()
     */
    public Bundle getBundle() {
        return new TenantAwareBundle(this);
    }

    /**
     * @see org.osgi.framework.BundleContext#getBundle(long)
     */
    public Bundle getBundle(long id) {
        Bundle bundle = m_parentBundleContext.getBundle(id);
        if (bundle == null) {
            return null;
        }
        BundleContext bundleContext = bundle.getBundleContext();
        if (bundleContext != null) {
            TenantAwareBundleContext scopedBundleContext = createScopedBundleContext(bundleContext);
            return scopedBundleContext.getBundle();
        }
        return new TenantAwareBundle(bundle);
    }

    /**
     * @see org.osgi.framework.BundleContext#getBundles()
     */
    public Bundle[] getBundles() {
        Bundle[] bundles = m_parentBundleContext.getBundles();
        if (bundles == null) {
            return new Bundle[0];
        }
        for (int i = 0; i < bundles.length; i++) {
            BundleContext bundleContext = bundles[i].getBundleContext();
            if (bundleContext != null) {
                TenantAwareBundleContext scopedBundleContext = createScopedBundleContext(bundleContext);
                bundles[i] = scopedBundleContext.getBundle();
            }
            else {
                bundles[i] = new TenantAwareBundle(bundles[i]);
            }
        }
        return bundles;
    }

    /**
     * @see org.osgi.framework.BundleContext#getDataFile(java.lang.String)
     */
    public File getDataFile(String filename) {
        File dataFile = getScopedDataFile(m_identifier);
        if (dataFile == null) {
            return null;
        }
        if (!dataFile.exists()) {
            dataFile.mkdir();
        }
        if (filename == null || "".equals(filename)) {
            return dataFile;
        }
        return new File(dataFile, filename);
    }

    /**
     * @see org.osgi.framework.BundleContext#getProperty(java.lang.String)
     */
    public String getProperty(String key) {
        return m_parentBundleContext.getProperty(key);
    }

    /**
     * @see org.osgi.framework.BundleContext#getService(org.osgi.framework.ServiceReference)
     */
    public Object getService(ServiceReference reference) {
        return m_parentBundleContext.getService(reference);
    }

    /**
     * @see org.osgi.framework.BundleContext#getServiceReference(java.lang.String)
     */
    public ServiceReference getServiceReference(String clazz) {
        try {
            ServiceReference[] references =
                m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(null));
            if (references != null && references.length > 0) {
                Arrays.sort(references);
                return references[0];
            }
        }
        catch (InvalidSyntaxException e) {
            // Ignore...
        }
        return null;
    }

    /**
     * @see org.osgi.framework.BundleContext#getServiceReferences(java.lang.String, java.lang.String)
     */
    public ServiceReference[] getServiceReferences(String clazz, String filter) throws InvalidSyntaxException {
        return m_parentBundleContext.getServiceReferences(clazz, getScopedLookupFilter(filter));
    }

    /**
     * @see org.osgi.framework.BundleContext#installBundle(java.lang.String)
     */
    public Bundle installBundle(String location) throws BundleException {
        try {
            return m_parentBundleContext.installBundle(location,
                new MultiTenantBundleInputStream((new URL(location)).openStream()));
        }
        catch (IOException e) {
            throw new BundleException(
                "Could not convert bundle location to an input stream, wrapping it failed.", e);
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#installBundle(java.lang.String, java.io.InputStream)
     */
    public Bundle installBundle(String location, InputStream input) throws BundleException {
        return m_parentBundleContext.installBundle(location, new MultiTenantBundleInputStream(input));
    }

    /**
     * @see org.osgi.framework.BundleContext#registerService(java.lang.String, java.lang.Object, java.util.Dictionary)
     */
    public ServiceRegistration registerService(String clazz, Object service, Dictionary properties) {
        // If the visibility filter matches for the registered service;
        // add a new service-property to make it global visible...
        Dictionary scopedProperties = getScopedProperties(properties, clazz);

        ServiceRegistration registration = m_parentBundleContext.registerService(clazz, service, scopedProperties);

        ServiceRegistrationAdapter adapter = new ServiceRegistrationAdapter(this, registration);
        m_serviceRegistrations.putIfAbsent(adapter, registration);
        return adapter;
    }

    /**
     * @see org.osgi.framework.BundleContext#registerService(java.lang.String[], java.lang.Object, java.util.Dictionary)
     */
    public ServiceRegistration registerService(String[] clazzes, Object service, Dictionary properties) {
        // If the visibility filter matches for the registered service;
        // add a new service-property to make it global visible...
        Dictionary scopedProperties = getScopedProperties(properties, clazzes);

        ServiceRegistration registration = m_parentBundleContext.registerService(clazzes, service, scopedProperties);

        ServiceRegistrationAdapter adapter = new ServiceRegistrationAdapter(this, registration);
        m_serviceRegistrations.putIfAbsent(adapter, registration);
        return adapter;
    }

    /**
     * @see org.osgi.framework.BundleContext#removeBundleListener(org.osgi.framework.BundleListener)
     */
    public void removeBundleListener(BundleListener listener) {
        TenantAwareBundleListener scopedBundleListener = m_bundleListeners.remove(listener);
        if (scopedBundleListener != null) {
            m_parentBundleContext.removeBundleListener(scopedBundleListener);
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#removeFrameworkListener(org.osgi.framework.FrameworkListener)
     */
    public void removeFrameworkListener(FrameworkListener listener) {
        TenantAwareFrameworkListener scopedFrameworkListener = m_frameworkListeners.remove(listener);
        if (scopedFrameworkListener != null) {
            m_parentBundleContext.removeFrameworkListener(scopedFrameworkListener);
        }
    }

    /**
     * @see org.osgi.framework.BundleContext#removeServiceListener(org.osgi.framework.ServiceListener)
     */
    public void removeServiceListener(ServiceListener listener) {
        m_parentBundleContext.removeServiceListener(listener);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TenantAwareBundleContext(" + m_identifier + ")";
    }

    /**
     * @see org.osgi.framework.BundleContext#ungetService(org.osgi.framework.ServiceReference)
     */
    public boolean ungetService(ServiceReference reference) {
        try {
            return m_parentBundleContext.ungetService(reference);
        }
        catch (Exception e) {
            // Ignore...
        }
        return false;
    }

    /**
     * Returns the <em>original</em> (i.e., the one that was used to create the bundle) bundle context.
     * <p>
     * USE WITH CARE, THIS BUNDLE CONTEXT GOES OUTSIDE THE MULTI-TENANCY CONCEPT!
     * </p>
     * 
     * @return the original bundle context, never <code>null</code>.
     */
    final BundleContext getParent() {
        return m_parentBundleContext;
    }

    /**
     * Adapts (or creates) the given dictionary in such way that it always contains the tenant.pid property.
     * 
     * @param properties the original dictionary, may be null, in which case a new dictionary will be created.
     * @return the scoped dictionary, never <code>null</code>.
     */
    final Dictionary getScopedProperties(ServiceRegistration serviceRegistration, Dictionary properties) {
        if (properties == null) {
            properties = new Properties();
        }
        // Make sure the identifier of this tenant is registered with *all*
        // services the tenant-aware service registers itself...
        properties.put(PID_KEY, m_identifier);

        // Make sure that if the original service properties already contained
        // the "global visibility" flag, it also exists in the returned properties...
        if (m_visibilityFilter != null && m_visibilityFilter.match(serviceRegistration.getReference())) {
            properties.put("org.amdatu.tenant.global", Boolean.TRUE);
        }

        return properties;
    }

    /**
     * Unregisters the given service registration adapter.
     * 
     * @param serviceReg the service registration adapter to unregister, cannot be <code>null</code>.
     */
    final void unregisterService(ServiceRegistrationAdapter serviceReg) {
        m_serviceRegistrations.remove(serviceReg);
        serviceReg.getRegistration().unregister();
    }

    /**
     * @param scope
     * @return The location of the root folder where a tenant can store its data.
     */
    protected File getScopedDataFile(String scope) {
        return m_bundleDataStore.getRoot();
    }

    /**
     * Creates a filter for service-lookups that uses the correct scoping.
     * 
     * @param filter the input filter to scope, can be <code>null</code> or empty.
     * @return a filter clause, never <code>null</code>.
     */
    protected String getScopedLookupFilter(String filter) {
        if (filter == null || "".equals(filter)) {
            return m_lookupFilter;
        }
        return "(&" + filter + m_lookupFilter + ")";
    }

    /**
     * Crafts a service-property {@link Dictionary} based on the given {@link Dictionary} and
     * correctly scoped regarding the visibility of the service (if necessary).
     * 
     * @param originalProperties the original service properties;
     * @param serviceClasses the array with service classes the service will be registered under.
     */
    private Dictionary getScopedProperties(Dictionary originalProperties, String... serviceClasses) {
        if (originalProperties == null) {
            originalProperties = new Properties();
        }
        // Make sure the identifier of this tenant is registered with *all*
        // services the tenant-aware service registers itself...
        originalProperties.put(PID_KEY, m_identifier);

        if (m_visibilityFilter != null) {
            Properties visibilityServiceProps = new Properties();
            // Craft a dictionary which resembles the one that would be created
            // by the OSGi framework during the service registration...
            visibilityServiceProps.put(org.osgi.framework.Constants.OBJECTCLASS, serviceClasses);
            visibilityServiceProps.put(PID_KEY, m_identifier);

            Enumeration keySet = originalProperties.keys();
            while (keySet.hasMoreElements()) {
                Object key = keySet.nextElement();
                Object value = originalProperties.get(key);
                visibilityServiceProps.put(key, value);
            }

            if (m_visibilityFilter.match(visibilityServiceProps)) {
                // Ok; service needs to be global visible; make it so...
                originalProperties.put("org.amdatu.tenant.global", Boolean.TRUE);
            }
        }

        return originalProperties;
    }

    /**
     * Creates a bundle context that is scoped regarding the visibility, data store and lookup.
     * 
     * @param bundleContext the bundle context to wrap, cannot be <code>null</code>.
     * @return a new {@link TenantAwareBundleContext} instance, never <code>null</code>.
     */
    private TenantAwareBundleContext createScopedBundleContext(BundleContext bundleContext) {
        return new TenantAwareBundleContext(bundleContext, m_identifier, m_bundleDataStore, m_lookupFilter,
            m_visibilityFilter);
    }
}
