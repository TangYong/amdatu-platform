/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.adapter;

import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS;
import static org.amdatu.tenant.adapter.Constants.DEFAULT_BINDING;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_BINDING_KEY;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_BINDING_VALUE_BOTH;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_BINDING_VALUE_PLATFORM;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_BINDING_VALUE_TENANTS;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_VERSION_KEY;
import static org.amdatu.tenant.adapter.Constants.MULTITENANT_VERSION_VALUE;
import static org.amdatu.tenant.adapter.Constants.PLATFORM_BINDING_FILTER;
import static org.amdatu.tenant.adapter.Constants.TENANT_BINDING_FILTER;

import java.util.Properties;

import org.amdatu.tenant.Tenant;
import org.amdatu.tenant.TenantLifeCycleListener;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * Generic bundle activator that can be used to convert a normal bundle into a multi tenant one.
 * The process of converting a normal bundle starts with replacing the existing bundle activator
 * with this one. Via a set of extra manifest entries the normal bundle activator can be specified.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @see Constants
 */
public class MultiTenantBundleActivator extends DependencyActivatorBase {

    /**
     * Returns whether or not the bundle has the correct "marker" bundle header
     * ({@link Constants#MULTITENANT_VERSION_KEY MULTITENANT_VERSION_KEY} with
     * {@link Constants#MULTITENANT_VERSION_VALUE MULTITENANT_VERSION_VALUE})
     * denoting it as a valid multi-tenant aware bundle.
     * 
     * @param context the bundle context to use, cannot be <code>null</code>.
     * @return <code>true</code> if the bundle headers contain the correct "marker" header,
     *         <code>false</code> otherwise.
     */
    private static boolean isMultiTenantAwareBundle(BundleContext context) {
        String version = (String) context.getBundle().getHeaders().get(MULTITENANT_VERSION_KEY);
        return version != null && MULTITENANT_VERSION_VALUE.equals(version.trim());
    }

    /**
     * Determines the scope in which tenants should be created.
     * 
     * @return a scope, never <code>null</code>.
     */
    private static String determineScope(BundleContext context) {
        String scope = (String) context.getBundle().getHeaders().get(MULTITENANT_BINDING_KEY);
        if (scope == null
            || (!MULTITENANT_BINDING_VALUE_PLATFORM.equals(scope) && !MULTITENANT_BINDING_VALUE_BOTH.equals(scope))) {
            scope = DEFAULT_BINDING;
        }
        return scope;
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        // Only in case the correct marker bundle headers are found, we process this bundle further.
        // If those markers are not found, we don't do anything, assuming that somebody else might
        // be able to pick it up...
        if (isMultiTenantAwareBundle(context)) {
            String scope = determineScope(context);
            Properties props = new Properties();
            props.put(MULTITENANT_LIFECYCLELISTENER_BINDING_KEY, getBindingBitfield(scope));

            manager.add(createComponent()
                .setInterface(TenantLifeCycleListener.class.getName(), props)
                .setImplementation(MultiTenantBundleDataStore.class)
                );

            // For the individual tenants, we create an adapter that is called for each individually registered tenant.
            manager.add(createAdapterService(Tenant.class, getAdapterFilter(scope))
                .setImplementation(TenantAdapter.class)
                .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false)
                )
                );
        }
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

    /**
     * Returns the filter condition used to scope the tenant adapter.
     * 
     * @param binding the binding to use according to the bundle's manifest, cannot be <code>null</code>.
     * @return a filter condition, never <code>null</code>.
     */
    private String getAdapterFilter(String binding) {
        if (MULTITENANT_BINDING_VALUE_PLATFORM.equals(binding)) {
            // For platform tenants; only allow the _PLATFORM services to be seen...
            return PLATFORM_BINDING_FILTER;
        }
        else if (MULTITENANT_BINDING_VALUE_TENANTS.equals(binding)) {
            // For "real" tenants, only allow the non-_PLATFORM services to be seen...
            return TENANT_BINDING_FILTER;
        }
        // In case both the real and platform services are to be seen, we shouldn't filter at all...
        return null;
    }

    /** Returns the proper bits that match the provided binding string. */
    private Integer getBindingBitfield(String binding) {
        if (MULTITENANT_BINDING_VALUE_PLATFORM.equals(binding)) {
            return Integer.valueOf(MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM);
        }
        else if (MULTITENANT_BINDING_VALUE_TENANTS.equals(binding)) {
            return Integer.valueOf(MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS);
        }
        return Integer.valueOf(MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH);
    }
}
