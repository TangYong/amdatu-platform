/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.factory;

import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM;
import static org.amdatu.tenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS;
import static org.amdatu.tenant.Constants.PID_VALUE_PLATFORM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.amdatu.tenant.Constants;
import org.amdatu.tenant.Tenant;
import org.amdatu.tenant.TenantFactoryConfiguration;
import org.amdatu.tenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

/**
 * Amdatu core {@link ManagedServiceFactory} implementation responsible for
 * publishing {@link Tenant} services based on configuration provided under
 * factoryPid {@link #PID}.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 *
 */
public final class TenantServiceFactory implements TenantFactoryConfiguration {
    private final Object m_lock = new Object();
    private final Map<String, Component> m_components = new HashMap<String, Component>();
    private final Map<ServiceReference, TenantLifeCycleListener> m_listeners = new HashMap<ServiceReference, TenantLifeCycleListener>();
    private final Map<ServiceReference, TenantLifeCycleListener> m_platformListeners = new HashMap<ServiceReference, TenantLifeCycleListener>();
    private boolean m_initial = true;

    private volatile DependencyManager m_dependencyManager;
    private volatile Component m_component;
    private volatile LogService m_logService;

    public void add(ServiceReference reference, TenantLifeCycleListener listener) {
        String[] pids;
        Integer bindingBits = (Integer) reference.getProperty(MULTITENANT_LIFECYCLELISTENER_BINDING_KEY);
        int binding = bindingBits != null ? bindingBits.intValue() : MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH;
        synchronized (m_lock) {
            boolean isPlatformBinding = (binding & MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM) > 0;
            if (isPlatformBinding) {
                m_platformListeners.put(reference, listener);
            }
            boolean isTenantBinding = (binding & MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS) > 0;
            if (isTenantBinding) {
                m_listeners.put(reference, listener);
            }
            pids = new String[m_components.size()];
            Iterator<Component> iterator = m_components.values().iterator();
            int i = 0;
            while (i < pids.length && iterator.hasNext()) {
                String pid = (String) iterator.next().getServiceProperties().get(Constants.PID_KEY);
                boolean isPlatformPid = pid.equals(Constants.PID_VALUE_PLATFORM);
                if ((isPlatformBinding && isPlatformPid) || (isTenantBinding && !isPlatformPid)) {
                    pids[i++] = pid;
                }
            }
            // because of the binding, we might have ended up with an array of a smaller size
            // in practice: N, N-1, 1 or 0
            if (i < pids.length) {
                pids = Arrays.copyOf(pids, i);
            }
        }
        listener.initial(pids);
    }

    public void remove(ServiceReference reference, TenantLifeCycleListener listener) {
        synchronized (m_lock) {
            // instead of trying to figure out the binding, we simply try to remove the listener
            // from both lists
            m_platformListeners.remove(reference);
            m_listeners.remove(reference);
        }
    }

    /**
     * Creates a shallow copy of a given dictionary.
     *
     * @param dictionary the dictionary to copy, can be <code>null</code>.
     * @return a copy of the given dictionary, can only be <code>null</code> if the given dictionary was
     *         <code>null</code>.
     */
    private Dictionary copyProperties(Map<String, Object> dictionary) {
        Properties copy = null;
        if (dictionary != null) {
            copy = new Properties();
            for (Entry<String, Object> e : dictionary.entrySet()) {
                String key = e.getKey();
                // copy any key except the PID of a tenant, because you should not be able to
                // override that this way
                if (!Constants.PID_KEY.equals(key)) {
                    copy.put(key, e.getValue());
                }
            }
        }
        return copy;
    }

    public void update(Map<String, Map<String, Object>> tenants) {
        List<Update> updated = new ArrayList<Update>();
        List<Update> added = new ArrayList<Update>();
        List<Update> removed = new ArrayList<Update>();
        Set<String> existing;
        boolean initial;
        synchronized (m_lock) {
            initial = m_initial && tenants.size() > 0;
            if (initial) {
                m_initial = false;
            }
            existing = new HashSet(m_components.keySet());
            for (Entry<String, Map<String, Object>> entry : tenants.entrySet()) {
                String pid = entry.getKey();
                Dictionary serviceProperties = copyProperties(entry.getValue());
                serviceProperties.put(Constants.PID_KEY, pid);
                if (existing.remove(pid)) {
                    // pid already existed, so we need to update it
                    Component component = m_components.get(pid);
                    updated.add(new Update(pid, component, serviceProperties, null));
                }
                else {
                    // it's a new tenant
                    Component component = m_dependencyManager.createComponent()
                                    .setInterface(Tenant.class.getName(), serviceProperties)
                                    .setImplementation(new TenantService(serviceProperties));

                    m_components.put(pid, component);
                    added.add(new Update(pid, component, serviceProperties, getListeners(pid)));
                }
            }

            // anything that is still in the 'existing' list has been deleted because
            // it no longer is present in the new configuration
            for (String pid : existing) {
                Component component = m_components.remove(pid);
                removed.add(new Update(pid, component, null, getListeners(pid)));
            }
        }
        // at this point we've ended up with 3 lists: added, updated and removed
        // tenants, now (outside of our synchronized block) we perform the updates

        // remove tenants that are no longer there
        for (Update u : removed) {
            m_logService.log(LogService.LOG_DEBUG, "Unregistering tenant service with pid: '" + u.getPid() + "'");
            m_dependencyManager.remove(u.getComponent());
            for (TenantLifeCycleListener listener : u.getListeners()) {
                try {
                    listener.delete(u.getPid());
                }
                catch (Exception e) {
                    m_logService.log(LogService.LOG_WARNING, "Tenant life cycle listener returned with an exception when invoking delete for PID: " + u.getPid(), e);
                }
            }
        }

        // update the existing tenants that have somehow been modified
        for (Update u : updated) {
            m_logService.log(LogService.LOG_DEBUG, "Updating tenant service with pid: '" + u.getPid() + "'");
            u.getComponent().setServiceProperties(u.getProperties());
        }

        // now add the new tenants
        for (Update u : added) {
            m_logService.log(LogService.LOG_DEBUG, "Registering tenant service with pid: '" + u.getPid() + "'");
            for (TenantLifeCycleListener listener : u.getListeners()) {
                try {
                    listener.create(u.getPid());
                }
                catch (Exception e) {
                    m_logService.log(LogService.LOG_WARNING, "Tenant life cycle listener returned with an exception when invoking create for PID: " + u.getPid(), e);
                }
            }
            m_dependencyManager.add(u.getComponent());
        }

        // after receiving our initial, not empty configuration, we register as a listener for
        // tenant life cycle listeners so we can start notifying them
        if (initial) {
            m_component.add(m_dependencyManager.createServiceDependency()
                .setService(TenantLifeCycleListener.class)
                .setCallbacks("add", "remove")
                .setRequired(false));
        }
    }

    private TenantLifeCycleListener[] getListeners(String tenantPid) {
        Collection<TenantLifeCycleListener> values;
        if (PID_VALUE_PLATFORM.equals(tenantPid)) {
            values = m_platformListeners.values();
        }
        else {
            values = m_listeners.values();
        }
        return values.toArray(new TenantLifeCycleListener[values.size()]);
    }

    private static class Update {
        private final String m_pid;
        private final Component m_component;
        private final Dictionary m_properties;
        private final TenantLifeCycleListener[] m_listeners;

        public Update(String pid, Component component, Dictionary properties, TenantLifeCycleListener[] listeners) {
            m_pid = pid;
            m_component = component;
            m_properties = properties;
            m_listeners = listeners;
        }

        public String getPid() {
            return m_pid;
        }

        public Component getComponent() {
            return m_component;
        }

        public Dictionary getProperties() {
            return m_properties;
        }

        public TenantLifeCycleListener[] getListeners() {
            return m_listeners;
        }
    }
}
