/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tenant.factory;

import java.util.Dictionary;

import org.amdatu.tenant.Constants;
import org.amdatu.tenant.Tenant;

/**
 * Data-object representing a Tenant.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantService implements Tenant {
    private final String m_id;
    private String m_name;

    // TODO who uses this constructor???
    public TenantService(String id, String name) {
        if (id == null) {
            throw new NullPointerException("id cannot be null");
        }
        m_id = id;
        m_name = name;
    }

    /**
     * Convenience constructor to create a tenant service directly from a {@link Tenant}'s service properties.
     * 
     * @param serviceProperties the Tenant's service properties to use, cannot be <code>null</code>.
     */
    TenantService(Dictionary serviceProperties) {
        if (serviceProperties == null) {
            throw new NullPointerException("service properties cannot be null");
        }
        m_id = (String) serviceProperties.get(Constants.PID_KEY);
        m_name = (String) serviceProperties.get(Constants.NAME_KEY);
    }

    public String getPID() {
        return m_id;
    }

    public String getName() {
        return m_name;
    }

    public void setName(String name) {
        m_name = name;
    }

    @Override
    public String toString() {
        return m_name + " (" + m_id + ")";
    }

    @Override
    public int hashCode() {
        return m_id.hashCode() * 31;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TenantService other = (TenantService) obj;
        if (getPID() == null && other.getPID() != null) {
            return false;
        }
        if (getPID() != null && other.getPID() == null) {
            return false;
        }
        return getPID().equals(other.getPID());
    }
}
