/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Base implementation of a persistent store on disk that holds 0 or more
 * entities. Concrete implementations must extend this to implement their
 * marshal / unmarshal strategy.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public abstract class FSStoreBase<T extends FSStoreEntity> {

    private final File m_file;
    private final Map<String, T> m_entities;

    public FSStoreBase(File file) throws IOException {
        m_file = file;
        m_entities = new HashMap<String, T>();
        readEntities();
    }

    public T addEntity(final T entity) {
        return m_entities.put(entity.getId(), entity);
    }

    public T removeEntity(final String entityId) {
        return m_entities.remove(entityId);
    }

    public T getEntity(final String entityId) {
        return m_entities.get(entityId);
    }

    public void save() throws IOException {
        writeEntities();
    }

    private void readEntities() throws IOException {
        if (!m_file.exists()) {
            return;
        }
        m_entities.clear();
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(m_file);
            ois = new ObjectInputStream(fis);
            final int numberOfTenants = ois.readInt();
            for (int i = 0; i < numberOfTenants; i++) {
                final T entity = readEntity(ois);
                m_entities.put(entity.getId(), entity);
            }
        }
        finally {
            FSStorageUtil.closeInputStreamsSafely(ois, fis);
        }
    }

    private void writeEntities() throws IOException {
        if ((m_entities == null || m_entities.size() == 0) && m_file.exists()) {
            m_file.delete();
            return;
        }
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(m_file);
            oos = new ObjectOutputStream(fos);
            oos.writeInt(m_entities.size());
            for (T entity : m_entities.values()) {
                writeEntity(oos, entity);
            }
            oos.flush();
        }
        finally {
            FSStorageUtil.closeOutputStreamsSafely(oos, fos);
        }
    }

    protected abstract T readEntity(ObjectInputStream ois) throws IOException,
        UnsupportedEncodingException;

    protected abstract void writeEntity(ObjectOutputStream oos, T entity) throws UnsupportedEncodingException,
        IOException;
}
