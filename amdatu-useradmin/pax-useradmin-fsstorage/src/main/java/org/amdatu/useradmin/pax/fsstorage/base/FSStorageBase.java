/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Base generic implementation of the main storage component. Concrete implementations
 * must extend this specifying the concrete types and implementing the factory method.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public abstract class FSStorageBase<S extends FSStoreBase<E>, E extends FSStoreEntity> {

    private static final String DEFAULT_ENTITYLIST_FILENAME = "entityList.ser";
    private static final String DEFAULT_STOREFILE_PREFIX = "e_";
    private static final String DEFAULT_STOREFILE_POSTFIX = ".ser";

    private File m_dataDirectory;
    private String m_entityListFilename;
    private String m_storeFilePrefix;
    private String m_storeFilepostfix;

    private FSStoreList m_entityList;

    public FSStorageBase(File dataDirectory) throws IOException {
        this(dataDirectory, DEFAULT_ENTITYLIST_FILENAME, DEFAULT_STOREFILE_PREFIX, DEFAULT_STOREFILE_POSTFIX);
    }

    public FSStorageBase(File dataDirectory, String entityListFileName, String storeFilePrefix, String storeFilePostfix)
        throws IOException {
        m_entityListFilename = entityListFileName;
        m_storeFilePrefix = storeFilePrefix;
        m_storeFilepostfix = storeFilePostfix;
        m_dataDirectory = dataDirectory;
        m_entityList = new FSStoreList(new File(m_dataDirectory, m_entityListFilename));
    }

    public final File getDataDirectory() {
        return m_dataDirectory;
    }

    public final String getEntityListFileName() {
        return m_entityListFilename;
    }

    public final String getStoreFilePrefix() {
        return m_storeFilePrefix;
    }

    public final String getStoreFilePostfix() {
        return m_storeFilepostfix;
    }

    public final synchronized void addEntity(E entity) throws IOException {
        S store = getFSStore(entity.getId());
        E storedEntity = store.addEntity(entity);
        if (storedEntity == null) {
            m_entityList.addValue(entity.getId());
        }
        store.save();
    }

    public final synchronized E removeEntity(String entityId) throws IOException {
        S store = getFSStore(entityId);
        E storedEntity = store.removeEntity(entityId);
        if (storedEntity != null) {
            m_entityList.removeValue(entityId);
            store.save();
        }
        return storedEntity;
    }

    public final synchronized E getEntity(String entityId) throws IOException {
        S store = getFSStore(entityId);
        return store.getEntity(entityId);
    }

    public final synchronized List<E> getAll() throws IOException {
        List<String> entityIdList = m_entityList.getAll();
        final List<E> entityList = new LinkedList<E>();
        for (String entityId : entityIdList) {
            entityList.add(getEntity(entityId));
        }
        return entityList;
    }

    /**
     * Default implementation of returning an FSStore for a specific
     * entity.
     */
    protected S getFSStore(String entityId) throws IOException {
        return newFSStore(getStoreFile(entityId));
    }

    /**
     * Determine the filename for this entity's store relative to the data directory.
     * This default hashing strategy uses a directory depth of one with mod 50 hashing
     * over the entity id hashcode.
     */
    protected File getStoreFile(String entityId) throws IOException {
        int hash = Math.abs(entityId.hashCode());
        String subdirName = "" + (hash % 50);
        final File subdirFile = new File(m_dataDirectory, subdirName);
        if (!subdirFile.exists()) {
            subdirFile.mkdir();
        }
        return new File(subdirFile, m_storeFilePrefix + hash + m_storeFilepostfix);
    }

    /**
     * Subclasses must provide an concrete instance of the store implementation.
     */
    protected abstract S newFSStore(File file) throws IOException;
}
