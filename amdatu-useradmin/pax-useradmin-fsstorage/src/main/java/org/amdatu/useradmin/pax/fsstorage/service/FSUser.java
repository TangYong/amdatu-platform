/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.User;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class FSUser extends FSRole implements User {

    private Dictionary m_credentials;

    public FSUser(final User user) {
        m_name = user.getName();
        m_properties = user.getProperties();
        m_credentials = user.getCredentials();
    }

    public FSUser(final String name, final Dictionary properties, final Dictionary credentials) {
        m_name = name;
        m_properties = properties;
        m_credentials = credentials;
    }

    @Override
    public int getType() {
        return Role.USER;
    }

    public Object getcredential(final String key) {
        if (m_credentials != null) {
            return m_credentials.get(key);
        }
        return null;
    }

    public void setCredential(final String key, final Object value) {
        if (m_credentials == null) {
            m_credentials = new Hashtable();
        }
        m_credentials.put(key, value);
    }

    public Object removeCredential(final String key) {
        if (m_credentials != null) {
            return m_credentials.remove(key);
        }
        return null;
    }

    public Dictionary getCredentials() {
        return m_credentials;
    }

    public void setCredentials(Dictionary credentials) {
        m_credentials = credentials;
    }

    public boolean hasCredential(String key, Object value) {
        Object localValue = m_credentials.get(key);
        if (localValue == null) {
            return false;
        }
        return localValue.equals(value);
    }
}
