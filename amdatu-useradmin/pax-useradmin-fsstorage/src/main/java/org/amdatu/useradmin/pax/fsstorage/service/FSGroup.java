/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.Role;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class FSGroup extends FSUser implements Group {

    protected Map<String, Role> m_members;
    protected Map<String, Role> m_requiredMembers;

    public FSGroup(final Group group) {
        super(group);
        Role[] members = group.getMembers();
        if (m_members == null) {
            m_members = new HashMap<String, Role>();
        }
        if (members != null) {
            for (Role member : members) {
                m_members.put(member.getName(), member);
            }
        }
        Role[] requiredMembers = group.getRequiredMembers();
        if (m_requiredMembers == null) {
            m_requiredMembers = new HashMap<String, Role>();
        }
        if (requiredMembers != null) {
            for (Role member : requiredMembers) {
                m_requiredMembers.put(member.getName(), member);
            }
        }
    }

    public FSGroup(final String name, final Dictionary properties, final Dictionary credentials) {
        super(name, properties, credentials);
    }

    @Override
    public int getType() {
        return Role.GROUP;
    }

    public boolean addMember(Role role) {
        if (m_members == null) {
            m_members = new HashMap<String, Role>();
        }
        if (m_members.containsKey(role.getName())) {
            return false;
        }
        m_members.put(role.getName(), role);
        return true;
    }

    public boolean addRequiredMember(Role role) {
        if (m_requiredMembers == null) {
            m_requiredMembers = new HashMap<String, Role>();
        }
        if (m_requiredMembers.containsKey(role.getName())) {
            return false;
        }
        m_requiredMembers.put(role.getName(), role);
        return true;
    }

    public Role[] getMembers() {
        if (m_members == null) {
            return new Role[] {};
        }
        return m_members.values().toArray(new Role[m_members.size()]);
    }

    public void setMembers(final List<FSRole> members) {
        if (members == null) {
            m_members = null;
        }
        else {
            m_members = new HashMap<String, Role>();
            for (FSRole role : members) {
                m_members.put(role.getName(), role);
            }
        }
    }

    public Role[] getRequiredMembers() {
        if (m_requiredMembers == null) {
            return new Role[] {};
        }
        return m_requiredMembers.values().toArray(new Role[m_requiredMembers.size()]);
    }

    public void setRequiredMembers(final List<FSRole> requiredMembers) {
        if (requiredMembers == null) {
            m_requiredMembers = null;
        }
        else {
            m_requiredMembers = new HashMap<String, Role>();
            for (FSRole role : requiredMembers) {
                m_requiredMembers.put(role.getName(), role);
            }
        }
    }

    public boolean removeMember(final Role role) {
        boolean removed = false;
        if (role != null) {
            if (m_members != null && m_members.containsKey(role.getName())) {
                m_members.remove(role.getName());
                removed = true;
            }
            if (m_requiredMembers != null && m_requiredMembers.containsKey(role.getName())) {
                m_requiredMembers.remove(role.getName());
                removed = true;
            }
        }
        return removed;
    }
}
