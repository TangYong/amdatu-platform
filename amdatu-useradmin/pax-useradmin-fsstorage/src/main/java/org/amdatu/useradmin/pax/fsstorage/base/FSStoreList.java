/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of a persistent list of Strings on disk used for keeping
 * track of the entities in a a store.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public final class FSStoreList {

    private final File m_file;
    private List<String> m_stringList;

    public FSStoreList(final File file) throws IOException {
        m_file = file;
        m_stringList = new LinkedList<String>();
        readStringList();
    }

    public List<String> getAll() throws IOException {
        return new LinkedList<String>(m_stringList);
    }

    public void addValue(final String value) throws IOException {
        if (!m_stringList.contains(value)) {
            m_stringList.add(value);
            writeValueList();
        }
    }

    public void removeValue(final String value) throws IOException {
        if (m_stringList.contains(value)) {
            m_stringList.remove(value);
            writeValueList();
        }
    }

    private void readStringList() throws IOException {
        if (!m_file.exists()) {
            m_stringList.clear();
            return;
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(m_file);
            ois = new ObjectInputStream(fis);
            m_stringList = FSStorageUtil.readList(ois);
        }
        finally {
            FSStorageUtil.closeInputStreamsSafely(ois, fis);
        }
    }

    private void writeValueList() throws IOException {
        if (m_stringList.size() == 0 && m_file.exists()) {
            m_file.delete();
            return;
        }
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(m_file);
            oos = new ObjectOutputStream(fos);
            FSStorageUtil.writeList(oos, m_stringList);
            oos.flush();
        }
        finally {
            FSStorageUtil.closeOutputStreamsSafely(oos, fos);
        }
    }
}
