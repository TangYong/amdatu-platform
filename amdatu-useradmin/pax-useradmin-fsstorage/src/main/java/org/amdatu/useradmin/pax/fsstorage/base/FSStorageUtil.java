/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Some generic (but limited!) utility methods for assiting implementations with
 * common marshal / unmarshall tasks.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public final class FSStorageUtil {

    private FSStorageUtil() {
    }

    public static byte[] readBytes(final ObjectInputStream ois) throws IOException {
        final int bytesLength = ois.readInt();
        final byte[] bytes = new byte[bytesLength];
        ois.readFully(bytes);
        return bytes;
    }

    public static Dictionary<String, Object> readDictionary(final ObjectInputStream ois) throws IOException {
        final int numberOfEntries = ois.readInt();
        if (numberOfEntries == 0) {
            return null;
        }
        final Dictionary<String, Object> dictionary = new Hashtable<String, Object>();
        for (int j = 0; j < numberOfEntries; j++) {
            final String key = FSStorageUtil.readString(ois);
            final int type = ois.readInt();
            switch (type) {
                case 0:
                    byte[] byteValue = FSStorageUtil.readBytes(ois);
                    dictionary.put(key, byteValue);
                    break;
                case 1:
                    String stringValue = FSStorageUtil.readString(ois);
                    dictionary.put(key, stringValue);
                    break;
                default:
                    break;
            }
        }
        return dictionary;
    }

    public static String readString(final ObjectInputStream ois) throws IOException {
        final int keyLength = ois.readInt();
        final byte[] keyBytes = new byte[keyLength];
        ois.readFully(keyBytes);
        return new String(keyBytes, "utf-8");
    }

    public static List<String> readList(final ObjectInputStream ois) throws IOException {
        final int listLength = ois.readInt();
        List<String> list = new ArrayList<String>(listLength);
        for (int i = 0; i < listLength; i++) {
            list.add(readString(ois));
        }
        return list;
    }

    public static Map<String, String> readProperties(final ObjectInputStream ois) throws IOException {
        final int numberOfProperties = ois.readInt();
        if (numberOfProperties == 0) {
            return null;
        }
        final Map<String, String> properties = new HashMap<String, String>();
        for (int i = 0; i < numberOfProperties; i++) {
            final String key = readString(ois);
            final String value = readString(ois);
            properties.put(key, value);
        }
        return properties;
    }

    public static void writeBytes(final ObjectOutputStream oos, final byte[] bytes) throws IOException {
        oos.writeInt(bytes.length);
        oos.write(bytes);
    }

    public static void writeDictionary(final ObjectOutputStream oos, final Dictionary<String, Object> dictionary)
        throws IOException {
        if (dictionary == null) {
            oos.writeInt(0);
        }
        else {
            oos.writeInt(dictionary.size());
            Enumeration<String> keys = dictionary.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                FSStorageUtil.writeString(oos, key);
                Object value = dictionary.get(key);
                if (value instanceof byte[]) {
                    oos.writeInt(0);
                    FSStorageUtil.writeBytes(oos, (byte[]) value);
                }
                else if (value instanceof String) {
                    oos.writeInt(1);
                    FSStorageUtil.writeString(oos, (String) value);
                }
            }
        }
    }

    public static void writeString(final ObjectOutputStream oos, final String value) throws IOException {
        String mvalue = value;
        if (mvalue == null) {
            mvalue = "";
        }
        final byte[] bytes = mvalue.getBytes("utf-8");
        oos.writeInt(bytes.length);
        oos.write(bytes);
    }

    public static void writeList(final ObjectOutputStream oos, final List<String> values) throws IOException {
        oos.writeInt(values.size());
        for (String value : values) {
            writeString(oos, value);
        }
    }

    public static void writeProperties(final ObjectOutputStream oos, final Map<String, String> properties)
        throws IOException {
        if (properties == null) {
            oos.writeInt(0);
            return;
        }
        oos.writeInt(properties.size());
        for (final String key : properties.keySet()) {
            writeString(oos, key);
            writeString(oos, properties.get(key));
        }
    }

    public static void closeInputStreamsSafely(ObjectInputStream ois, FileInputStream fis) throws IOException {
        try {
            if (ois != null) {
                ois.close();
            }
        }
        catch (IOException e) {
            if (fis != null) {
                fis.close();
            }
            throw e;
        }
    }

    public static void closeOutputStreamsSafely(ObjectOutputStream oos, FileOutputStream fos) throws IOException {
        try {
            if (oos != null) {
                oos.close();
            }
        }
        catch (IOException e) {
            if (fos != null) {
                fos.close();
            }
            throw e;
        }
    }

}
