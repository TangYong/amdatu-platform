/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.useradmin.pax.fsstorage.base.FSStoreEntity;
import org.osgi.service.useradmin.Role;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 *
 */
public abstract class FSRole implements Role, FSStoreEntity {

    protected String m_name;
    protected Dictionary m_properties;

    public String getId() {
        return m_name;
    }

    public String getName() {
        return m_name;
    }

    public Dictionary getProperties() {
        return m_properties;
    }

    public int getType() {
        return Role.ROLE;
    }

    public void setProperties(Dictionary dictionary) {
        m_properties = dictionary;
    }

    public Object getProperty(final String key) {
        if (m_properties != null) {
            return m_properties.get(key);
        }
        return null;
    }

    public void setProperty(String key, Object value) {
        if (m_properties == null) {
            m_properties = new Hashtable();
        }
        m_properties.put(key, value);
    }

    public Object removeProperty(final String key) {
        if (m_properties != null) {
            return m_properties.remove(key);
        }
        return null;
    }
}
