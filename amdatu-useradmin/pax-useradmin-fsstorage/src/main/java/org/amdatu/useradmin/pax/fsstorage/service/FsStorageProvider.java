/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ops4j.pax.useradmin.service.spi.StorageException;
import org.ops4j.pax.useradmin.service.spi.StorageProvider;
import org.ops4j.pax.useradmin.service.spi.UserAdminFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.log.LogService;
import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.User;

/**
 * Filesystem backed implementation of the PAX <code>StorageProvider</code> SPI.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class FsStorageProvider implements StorageProvider {

    private volatile BundleContext m_bundleContext;
    private volatile LogService m_logService;

    private FSRoleStorage m_storage;

    public void start() throws Exception {
        File storageDir = m_bundleContext.getDataFile("");
        if (storageDir == null) {
            throw new Exception("Local storage not supported");
        }
        m_storage = new FSRoleStorage(storageDir);
    }

    /*
     * JUnit test support
     */

    void setBundleContext(BundleContext bundleContext) throws Exception {
        m_bundleContext = bundleContext;
    }

    void setLogService(LogService logService) throws Exception {
        m_logService = logService;
    }

    /*
     * PAX StorageProvider API
     */

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#addMember(org.osgi.service.useradmin.Group, org.osgi.service.useradmin.Role)
     */
    public boolean addMember(final Group group, final Role role) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(group.getName());
            if (internalRole != null && internalRole.getType() == Role.GROUP) {
                ((FSGroup) internalRole).addMember(role);
                m_storage.addEntity(internalRole);
                return true;
            }
            return false;
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#addRequiredMember(org.osgi.service.useradmin.Group, org.osgi.service.useradmin.Role)
     */
    public boolean addRequiredMember(Group group, Role role) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(group.getName());
            if (internalRole != null && internalRole.getType() == Role.GROUP) {
                ((FSGroup) internalRole).addRequiredMember(role);
                m_storage.addEntity(internalRole);
                return true;
            }
            return false;
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#clearRoleAttributes(org.osgi.service.useradmin.Role)
     */
    public void clearRoleAttributes(final Role role) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(role.getName());
            if (internalRole != null && internalRole.getProperties() != null) {
                internalRole.setProperties(null);
                m_storage.addEntity(internalRole);
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#clearUserCredentials(org.osgi.service.useradmin.User)
     */
    public void clearUserCredentials(final User user) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(user.getName());
            if (internalRole != null && internalRole.getType() == Role.USER
                && ((FSUser) internalRole).getCredentials() != null) {
                ((FSUser) internalRole).setCredentials(null);
                m_storage.addEntity(internalRole);
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#createGroup(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, java.lang.String)
     */
    public Group createGroup(final UserAdminFactory userAdminFactory, final String groupName)
        throws StorageException {
        checkState();
        final Group group = userAdminFactory.createGroup(groupName, null, null);
        try {
            m_storage.addEntity(new FSGroup(group));
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return group;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#createUser(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, java.lang.String)
     */
    public User createUser(final UserAdminFactory userAdminFactory, final String userName)
        throws StorageException {
        checkState();
        final User user = userAdminFactory.createUser(userName, null, null);
        try {
            m_storage.addEntity(new FSUser(user));
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return user;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#deleteRole(org.osgi.service.useradmin.Role)
     */
    public boolean deleteRole(Role role) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.removeEntity(role.getName());
            if (role.getType() == Role.USER) {
                removeUserFromAllGroups(role);
            }
            if (internalRole != null) {
                return true;
            }
            return false;
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#findRoles(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, java.lang.String)
     */
    public Collection<Role> findRoles(final UserAdminFactory userAdminFactory, final String filterString)
        throws StorageException {
        checkState();
        List<Role> matchingRoles = new LinkedList<Role>();
        try {
            List<FSRole> internalRoles = m_storage.getAll();
            Filter filter = null;
            if (filterString != null) {
                filter = FrameworkUtil.createFilter(filterString);
            }
            for (FSRole internalRole : internalRoles) {
                if (filter == null || filter.match(internalRole.getProperties())) {
                    if (internalRole.getType() == Role.USER) {
                        User newuser =
                            userAdminFactory.createUser(internalRole.getName(),
                                dictionaryToMap(internalRole.getProperties()),
                                dictionaryToMap(((User) internalRole).getCredentials()));
                        matchingRoles.add(newuser);
                    }
                    else if (internalRole.getType() == Role.GROUP) {
                        Group newgroup =
                            userAdminFactory.createGroup(internalRole.getName(),
                                dictionaryToMap(internalRole.getProperties()),
                                dictionaryToMap(((Group) internalRole).getCredentials()));
                        matchingRoles.add(newgroup);
                    }
                }
            }
        }
        catch (InvalidSyntaxException e) {
            throw new StorageException(e.getMessage());
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return matchingRoles;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#getMembers(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, org.osgi.service.useradmin.Group)
     */
    public Collection<Role> getMembers(final UserAdminFactory userAdminFactory, final Group group)
        throws StorageException {
        checkState();
        Set<Role> members = new HashSet<Role>();
        try {
            FSRole internalRole = m_storage.getEntity(group.getName());
            if (internalRole != null && internalRole.getType() == Role.GROUP) {
                for (Role role : ((FSGroup) internalRole).getMembers()) {
                    if (role.getType() == Role.USER) {
                        User newuser =
                            userAdminFactory.createUser(role.getName(), dictionaryToMap(role.getProperties()),
                                dictionaryToMap(((User) role).getCredentials()));
                        members.add(newuser);
                    }
                    else if (role.getType() == Role.GROUP) {
                        Group newgroup =
                            userAdminFactory.createGroup(role.getName(), dictionaryToMap(role.getProperties()),
                                dictionaryToMap(((Group) role).getCredentials()));
                        members.add(newgroup);
                    }
                }
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return members;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#getRequiredMembers(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, org.osgi.service.useradmin.Group)
     */
    public Collection<Role> getRequiredMembers(final UserAdminFactory userAdminFactory, final Group group)
        throws StorageException {
        checkState();
        Set<Role> members = new HashSet<Role>();
        try {
            FSRole internalRole = m_storage.getEntity(group.getName());
            if (internalRole != null && internalRole.getType() == Role.GROUP) {
                for (Role role : ((FSGroup) internalRole).getRequiredMembers()) {
                    if (role.getType() == Role.USER) {
                        User newuser =
                            userAdminFactory.createUser(role.getName(), dictionaryToMap(role.getProperties()),
                                dictionaryToMap(((User) role).getCredentials()));
                        members.add(newuser);
                    }
                    else if (role.getType() == Role.GROUP) {

                        Group newgroup = userAdminFactory.createGroup(internalRole.getName(),
                            dictionaryToMap(internalRole.getProperties()),
                            dictionaryToMap(((Group) internalRole).getCredentials()));
                        for (Role member : ((FSGroup) internalRole).getMembers()) {
                            newgroup.addMember(getRole(userAdminFactory, member.getName()));
                        }
                        for (Role member : ((FSGroup) internalRole).getRequiredMembers()) {
                            newgroup.addRequiredMember(getRole(userAdminFactory, member.getName()));
                        }
                        members.add(newgroup);
                    }
                }
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return members;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#getRole(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, java.lang.String)
     */
    public Role getRole(final UserAdminFactory userAdminFactory, final String roleName)
        throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(roleName);
            if (internalRole != null) {
                if (internalRole.getType() == Role.USER) {
                    return userAdminFactory.createUser(internalRole.getName(),
                        dictionaryToMap(internalRole.getProperties()),
                        dictionaryToMap(((User) internalRole).getCredentials()));
                }
                else if (internalRole.getType() == Role.GROUP) {
                    Group newgroup = userAdminFactory.createGroup(internalRole.getName(),
                        dictionaryToMap(internalRole.getProperties()),
                        dictionaryToMap(((Group) internalRole).getCredentials()));
                    for (Role member : ((FSGroup) internalRole).getMembers()) {
                        newgroup.addMember(getRole(userAdminFactory, member.getName()));
                    }
                    for (Role member : ((FSGroup) internalRole).getRequiredMembers()) {
                        newgroup.addRequiredMember(getRole(userAdminFactory, member.getName()));
                    }
                    return newgroup;
                }
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        return null;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#getUser(org.ops4j.pax.useradmin.service.spi.UserAdminFactory, java.lang.String, java.lang.String)
     */
    public User getUser(final UserAdminFactory userAdminFactory, final String key, final String value)
        throws StorageException {
        checkState();
        List<User> matchingUsers = new LinkedList<User>();
        try {
            Filter filter = FrameworkUtil.createFilter("(" + key + "=" + value + ")");
            List<FSRole> fsRoles = m_storage.getAll();
            for (FSRole fsRole : fsRoles) {
                Role role = getRole(userAdminFactory, fsRole.getId());
                if (role.getType() == Role.USER && filter.match(role.getProperties())) {
                    matchingUsers.add((User) role);
                }
            }
        }
        catch (InvalidSyntaxException e) {
            throw new StorageException(e.getMessage());
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
        if (matchingUsers.size() == 1) {
            return matchingUsers.get(0);
        }
        return null;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#removeMember(org.osgi.service.useradmin.Group, org.osgi.service.useradmin.Role)
     */
    public boolean removeMember(final Group group, final Role role) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(group.getName());
            if (internalRole != null && internalRole.getType() == Role.GROUP
                && ((FSGroup) internalRole).removeMember(role)) {
                m_storage.addEntity(internalRole);
                return true;
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());

        }
        return false;
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#removeRoleAttribute(org.osgi.service.useradmin.Role, java.lang.String)
     */
    public void removeRoleAttribute(final Role role, final String key) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(role.getName());
            if (internalRole != null) {
                Object value = internalRole.getProperties().remove(key);
                if (value != null) {
                    m_storage.addEntity(internalRole);
                }
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#removeUserCredential(org.osgi.service.useradmin.User, java.lang.String)
     */
    public void removeUserCredential(final User user, final String key) throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(user.getName());
            if (internalRole != null && internalRole.getType() == Role.USER) {
                Object value = ((FSUser) internalRole).removeCredential(key);
                if (value != null) {
                    m_storage.addEntity(internalRole);
                }
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#setRoleAttribute(org.osgi.service.useradmin.Role, java.lang.String, java.lang.Object)
     */
    public void setRoleAttribute(final Role role, final String key, final Object value)
        throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(role.getName());
            if (internalRole != null) {
                internalRole.setProperty(key, value);
                m_storage.addEntity(internalRole);
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    /**
     * @see org.ops4j.pax.useradmin.service.spi.StorageProvider#setUserCredential(org.osgi.service.useradmin.User, java.lang.String, java.lang.Object)
     */
    public void setUserCredential(final User user, final String key, final Object value)
        throws StorageException {
        checkState();
        try {
            FSRole internalRole = m_storage.getEntity(user.getName());
            if (internalRole != null && internalRole.getType() == Role.USER) {
                ((FSUser) internalRole).setCredential(key, value);
                m_storage.addEntity(internalRole);
            }
        }
        catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }

    private Map<String, Object> dictionaryToMap(final Dictionary<String, Object> dictionary) {
        if (dictionary == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Enumeration<String> keys = dictionary.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            map.put(key, dictionary.get(key));
        }
        return map;
    }

    private void checkState() throws StorageException {
        if (m_storage == null) {
            throw new StorageException("Storage backend is not configured");
        }
    }

    private void removeUserFromAllGroups(Role user) throws IOException {
        List<FSRole> fsRoles = m_storage.getAll();
        for (FSRole fsRole : fsRoles) {
            if (fsRole instanceof FSGroup) {
                // This is a group, remove the user as member of this group (if it is a member)
                if (((FSGroup) fsRole).removeMember(user)) {
                    m_storage.addEntity(fsRole);
                }
            }
        }
    }
}
