/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Dictionary;
import java.util.LinkedList;
import java.util.List;

import org.amdatu.useradmin.pax.fsstorage.base.FSStorageUtil;
import org.amdatu.useradmin.pax.fsstorage.base.FSStoreBase;
import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.User;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 *
 */
public final class FSRoleStore extends FSStoreBase<FSRole> {

    public FSRoleStore(File file) throws IOException {
        super(file);
    }

    protected FSRole readEntity(ObjectInputStream ois) throws IOException,
        UnsupportedEncodingException {
        int roleType = ois.readInt();
        String name = FSStorageUtil.readString(ois);
        Dictionary<String, Object> properties = FSStorageUtil.readDictionary(ois);
        Dictionary<String, Object> credentials = FSStorageUtil.readDictionary(ois);

        if (roleType == Role.USER) {
            return new FSUser(name, properties, credentials);
        }
        else if (roleType == Role.GROUP) {
            FSGroup group = new FSGroup(name, properties, credentials);
            group.setMembers(readMembers(ois));
            group.setRequiredMembers(readMembers(ois));
            return group;
        }
        else {
            throw new IllegalStateException("Deserialization error: illegal roletype " + roleType);
        }
    }

    protected void writeEntity(ObjectOutputStream oos, FSRole role) throws UnsupportedEncodingException,
        IOException {
        oos.writeInt(role.getType());
        FSStorageUtil.writeString(oos, role.getName());
        FSStorageUtil.writeDictionary(oos, role.getProperties());
        FSStorageUtil.writeDictionary(oos, ((User) role).getCredentials());
        if (role.getType() == Role.GROUP) {
            writeMembers(oos, ((Group) role).getMembers());
            writeMembers(oos, ((Group) role).getRequiredMembers());
        }
    }

    private List<FSRole> readMembers(final ObjectInputStream ois) throws IOException {
        final int numberOfMembers = ois.readInt();
        if (numberOfMembers == 0) {
            return null;
        }
        final List<FSRole> members = new LinkedList<FSRole>();
        for (int i = 0; i < numberOfMembers; i++) {
            final int memberType = ois.readInt();
            if (memberType == Role.USER) {
                members.add(new FSUser(FSStorageUtil.readString(ois), null, null));
            }
            else if (memberType == Role.GROUP) {
                members.add(new FSGroup(FSStorageUtil.readString(ois), null, null));
            }
            else {
                throw new IllegalStateException("Deserialization error: illegal membertype " + memberType);
            }
        }
        return members;
    }

    private void writeMembers(final ObjectOutputStream oos, final Role[] members) throws IOException {
        if (members == null) {
            oos.writeInt(0);
        }
        else {
            oos.writeInt(members.length);
            for (Role member : members) {
                oos.writeInt(member.getType());
                FSStorageUtil.writeString(oos, member.getName());
            }
        }
    }
}
