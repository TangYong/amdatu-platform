/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base.test;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.amdatu.useradmin.pax.fsstorage.base.FSStorageUtil;
import org.amdatu.useradmin.pax.fsstorage.base.FSStoreBase;

public final class FSTestStore extends FSStoreBase<FSTestEntity> {

    public FSTestStore(File file) throws IOException {
        super(file);
    }

    protected FSTestEntity readEntity(ObjectInputStream ois) throws IOException,
        UnsupportedEncodingException {
        String id = FSStorageUtil.readString(ois);
        Map<String, String> props = FSStorageUtil.readProperties(ois);
        return new FSTestEntity(id, props);
    }

    protected void writeEntity(ObjectOutputStream oos, FSTestEntity entity) throws UnsupportedEncodingException,
        IOException {
        FSStorageUtil.writeString(oos, entity.getId());
        FSStorageUtil.writeProperties(oos, entity.getProperties());
    }
}
