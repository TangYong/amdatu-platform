/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service.mock;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

import org.ops4j.pax.useradmin.service.spi.UserAdminFactory;
import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.User;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class MockUserAdminFactory implements UserAdminFactory {

    public User createUser(String name, Map<String, Object> properties, Map<String, Object> credentials) {
        Dictionary propertiesDictionary = null;
        if (properties != null) {
            propertiesDictionary = new Hashtable(properties);
        }
        Dictionary credentialsDictionary = null;
        if (credentials != null) {
            credentialsDictionary = new Hashtable(credentials);
        }
        return new MockUser(name, propertiesDictionary, credentialsDictionary);
    }

    public Group createGroup(String name, Map<String, Object> properties, Map<String, Object> credentials) {
        Dictionary propertiesDictionary = null;
        if (properties != null) {
            propertiesDictionary = new Hashtable(properties);
        }
        Dictionary credentialsDictionary = null;
        if (credentials != null) {
            credentialsDictionary = new Hashtable(credentials);
        }
        return new MockGroup(name, propertiesDictionary, credentialsDictionary);
    }
}
