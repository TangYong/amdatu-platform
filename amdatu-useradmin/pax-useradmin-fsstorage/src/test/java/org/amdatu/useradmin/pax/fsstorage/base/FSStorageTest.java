/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.amdatu.useradmin.pax.fsstorage.base.test.FSTestEntity;
import org.amdatu.useradmin.pax.fsstorage.base.test.FSTestStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class FSStorageTest {

    private final static String JAVA_IO_TMPDIR = System.getProperty("java.io.tmpdir");

    private static File m_absoluteTestRootDirectory;
    private static String m_relativeTestRootDirectory;

    @Rule
    public TestName m_testName = new TestName();

    private File m_testDirectory;
    private FSTestStorage m_testStorage;

    @BeforeClass
    public static void setUpOnce() throws IOException {
        Random rand = new Random();
        int randomInt = 1 + Math.abs(rand.nextInt());
        m_relativeTestRootDirectory = FSStorageTest.class.getSimpleName() + "_" + randomInt;
        m_absoluteTestRootDirectory =
            new File(JAVA_IO_TMPDIR + File.separator + m_relativeTestRootDirectory);
        m_absoluteTestRootDirectory.mkdirs();
    }

    @Before
    public void setUp() throws IOException {
        m_testDirectory = new File(m_absoluteTestRootDirectory, m_testName.getMethodName());
        m_testDirectory.mkdir();
        m_testStorage = new FSTestStorage(m_testDirectory);
    }

    @Test
    public void testTheTestStoreWithOneEntity() throws IOException {

        FSTestEntity e1 = new FSTestEntity();
        m_testStorage.addEntity(e1);
        Assert.assertEquals(e1, m_testStorage.getEntity(e1.getId()));

        e1 = m_testStorage.getEntity(e1.getId());
        Assert.assertNotNull(e1);

        List<FSTestEntity> eList = m_testStorage.getAll();
        Assert.assertEquals(1, eList.size());

        e1 = m_testStorage.removeEntity(e1.getId());
        Assert.assertNotNull(e1);

        e1 = m_testStorage.getEntity(e1.getId());
        Assert.assertNull(e1);

        eList = m_testStorage.getAll();
        Assert.assertEquals(0, eList.size());

    }

    @Test
    public void testTheTestStoreWithMultipleEntity() throws IOException {

        FSTestEntity e1 = new FSTestEntity();
        m_testStorage.addEntity(e1);
        Assert.assertEquals(e1, m_testStorage.getEntity(e1.getId()));

        FSTestEntity e3 = new FSTestEntity();
        m_testStorage.addEntity(e3);
        Assert.assertEquals(e3, m_testStorage.getEntity(e3.getId()));

        FSTestEntity e2 = new FSTestEntity();
        m_testStorage.addEntity(e2);
        Assert.assertEquals(e2, m_testStorage.getEntity(e2.getId()));

        List<FSTestEntity> eList = m_testStorage.getAll();
        Assert.assertEquals(3, eList.size());

        Assert.assertEquals(e1, m_testStorage.removeEntity(e1.getId()));

        eList = m_testStorage.getAll();
        Assert.assertEquals(2, eList.size());
    }

    @Test
    public void testTheTestStoreWithProperties() throws IOException {

        Map<String, String> props = new HashMap<String, String>();
        props.put("key1", "value1");
        props.put("key2", "value2");

        FSTestEntity e1 = new FSTestEntity("123", props);
        m_testStorage.addEntity(e1);

        e1 = m_testStorage.getEntity(e1.getId());

        props = e1.getProperties();
        Assert.assertNotNull(props);
        Assert.assertEquals(2, props.size());
        Assert.assertEquals("value1", props.get("key1"));
        Assert.assertEquals("value2", props.get("key2"));
    }

    /**
     * Testing behavior on tenantEntities with same hashcode() for the id. This is based on knowledge of the
     * implementation but as it is a likely pitfall let's test it anyway to catch future mistakes.
     * 
     * @throws TenantStorageException
     */
    @Test
    public void testWithEqualHashcodes() throws IOException {

        // Assert the reason for this test
        Assert.assertEquals("BB".hashCode(), "Aa".hashCode());

        FSTestEntity e1 = new FSTestEntity("BB", null);
        m_testStorage.addEntity(e1);
        Assert.assertEquals(e1, m_testStorage.getEntity(e1.getId()));

        FSTestEntity e3 = new FSTestEntity("Aa", null);
        m_testStorage.addEntity(e3);
        Assert.assertEquals(e3, m_testStorage.getEntity(e3.getId()));

        List<FSTestEntity> eList = m_testStorage.getAll();
        Assert.assertEquals(2, eList.size());
    }
}
