/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service.mock;

import java.util.Dictionary;
import java.util.HashSet;
import java.util.Set;

import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.Role;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class MockGroup extends MockUser implements Group {

    protected Set<Role> m_members;
    protected Set<Role> m_requiredMembers;

    public MockGroup(final String name, final Dictionary properties, final Dictionary credentials) {
        super(name, properties, credentials);
    }

    @Override
    public int getType() {
        return Role.GROUP;
    }

    public boolean addMember(Role role) {
        if (m_members == null) {
            m_members = new HashSet<Role>();
        }
        return m_members.add(role);
    }

    public boolean addRequiredMember(final Role role) {
        if (m_requiredMembers == null) {
            m_requiredMembers = new HashSet<Role>();
        }
        return m_requiredMembers.add(role);
    }

    public Role[] getMembers() {
        if (m_members == null) {
            return new Role[] {};
        }
        return m_members.toArray(new Role[m_members.size()]);
    }

    public Role[] getRequiredMembers() {
        if (m_requiredMembers == null) {
            return new Role[] {};
        }
        return m_requiredMembers.toArray(new Role[m_requiredMembers.size()]);
    }

    public boolean removeMember(final Role role) {
        if (role != null) {
            boolean removed = false;
            if (m_members != null) {
                removed = m_members.remove(role);
            }
            if (m_requiredMembers != null) {
                removed = removed || m_requiredMembers.remove(role);
            }
            return removed;
        }
        return false;
    }
}
