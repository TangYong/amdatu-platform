/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service.mock;

import java.util.Dictionary;

import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.User;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class MockUser extends MockRole implements User {

    private Dictionary m_credentials;

    public MockUser(final String name, final Dictionary properties, final Dictionary credentials) {
        m_type = Role.USER;
        m_name = name;
        m_properties = properties;
        m_credentials = credentials;
    }

    public Dictionary getCredentials() {
        return m_credentials;
    }

    public boolean hasCredential(String key, Object value) {
        if (m_credentials == null) {
            return false;
        }
        Object localValue = m_credentials.get(key);
        if (localValue == null) {
            return false;
        }
        return localValue.equals(value);
    }
}
