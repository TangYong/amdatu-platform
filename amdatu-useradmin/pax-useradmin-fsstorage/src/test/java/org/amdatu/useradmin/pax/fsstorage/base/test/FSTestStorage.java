/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.base.test;

import java.io.File;
import java.io.IOException;

import org.amdatu.useradmin.pax.fsstorage.base.FSStorageBase;

public class FSTestStorage extends FSStorageBase<FSTestStore, FSTestEntity> {

    private static final String ENTITYLIST_FILENAME = "testIdList.ser";
    private static final String STORAGEFILE_PREFIX = "t_";
    private static final String STORAGEFILE_POSTFIX = ".ser";

    public FSTestStorage(File dataDirectory) throws IOException {
        super(dataDirectory, ENTITYLIST_FILENAME, STORAGEFILE_PREFIX, STORAGEFILE_POSTFIX);
    }

    @Override
    protected FSTestStore newFSStore(File file) throws IOException {
        return new FSTestStore(file);
    }
}
