/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.useradmin.pax.fsstorage.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Random;

import junit.framework.Assert;

import org.amdatu.useradmin.pax.fsstorage.service.mock.MockRole;
import org.amdatu.useradmin.pax.fsstorage.service.mock.MockUserAdminFactory;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.ops4j.pax.useradmin.service.spi.StorageException;
import org.ops4j.pax.useradmin.service.spi.UserAdminFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.osgi.service.useradmin.Group;
import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.User;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public class FSUserAdminStorageProviderTest {

    private final static String JAVA_IO_TMPDIR = System.getProperty("java.io.tmpdir");
    private static File m_testBaseDirectory;

    @Rule
    public TestName m_testName = new TestName();

    private Mockery mockContext = new Mockery();
    private File m_testDirectory;
    private UserAdminFactory m_userAdminFactory;
    private FsStorageProvider m_userAdminStorageProvider;

    @BeforeClass
    public static void setUpOnce() throws Exception {
        m_testBaseDirectory =
            new File(JAVA_IO_TMPDIR + File.separator
                + FsStorageProvider.class.getSimpleName() + "_"
                + (1 + Math.abs(new Random().nextInt())));
        m_testBaseDirectory.mkdirs();
    }

    @Before
    public void setUp() throws Exception {

        m_testDirectory = new File(m_testBaseDirectory, m_testName.getMethodName());
        m_testDirectory.mkdir();

        final BundleContext bundleContext = mockContext.mock(BundleContext.class);
        final LogService logService = mockContext.mock(LogService.class);

        mockContext.checking(new Expectations() {
            {
                allowing(logService);
                allowing(bundleContext).getDataFile(with(any(String.class)));
                will(returnValue(m_testDirectory));
            }
        });

        m_userAdminStorageProvider = new FsStorageProvider();
        m_userAdminStorageProvider.setBundleContext(bundleContext);
        m_userAdminStorageProvider.setLogService(logService);
        m_userAdminStorageProvider.start();
        m_userAdminFactory = new MockUserAdminFactory();
    }

    /**
     * Comprehensive test of crud operations on tenants.
     * 
     * @throws UnsupportedEncodingException
     * 
     * @throws TenantStorageException
     */
    @Test
    public void testUserCreateDelete() throws StorageException, UnsupportedEncodingException {

        User user1 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Aa");
        Assert.assertNotNull(user1);
        Assert.assertEquals("Aa", user1.getName());
        user1 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Aa");
        Assert.assertNotNull(user1);
        Assert.assertEquals("Aa", user1.getName());

        User user2 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "BB");
        Assert.assertNotNull(user2);
        Assert.assertEquals("BB", user2.getName());
        user2 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "BB");
        Assert.assertNotNull(user2);
        Assert.assertEquals("BB", user2.getName());

        Assert.assertTrue(m_userAdminStorageProvider.deleteRole(user2));
        Assert.assertFalse(m_userAdminStorageProvider.deleteRole(user2));

        user2 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "BB");
        Assert.assertNull(user2);
    }

    @Test
    public void testUserProperties() throws StorageException, UnsupportedEncodingException {
        User user = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Bram");

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getProperties());

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.setRoleAttribute(user, "haarkleur", "blond");
        m_userAdminStorageProvider.setRoleAttribute(user, "oogkleur", "blauw");

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertEquals("blond", user.getProperties().get("haarkleur"));
        Assert.assertEquals("blauw", user.getProperties().get("oogkleur"));

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.removeRoleAttribute(user, "haarkleur");

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getProperties().get("haarkleur"));
        Assert.assertEquals("blauw", user.getProperties().get("oogkleur"));

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.clearRoleAttributes(user);

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getProperties());
    }

    @Test
    public void testUserCredentials() throws StorageException, UnsupportedEncodingException {
        User user = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Bram");

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getCredentials());

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.setUserCredential(user, "password", "test123");
        m_userAdminStorageProvider.setUserCredential(user, "token", "test123".getBytes("utf-8"));

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertEquals("test123", user.getCredentials().get("password"));
        Assert.assertEquals("test123", new String((byte[]) user.getCredentials().get("token"), "utf-8"));

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.removeUserCredential(user, "token");

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertEquals("test123", user.getCredentials().get("password"));
        Assert.assertNull(user.getCredentials().get("token"));

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.clearUserCredentials(user);

        user = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Bram");
        Assert.assertNotNull(user);
        Assert.assertNull(user.getCredentials());
    }

    @Test
    public void testGroupCreateDelete() throws StorageException {
        Group group = m_userAdminStorageProvider.createGroup(m_userAdminFactory, "amdatu-dev");
        Assert.assertNotNull(group);
        Assert.assertEquals("amdatu-dev", group.getName());

        User user1 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Bram");
        User user2 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Ivo");

        m_userAdminStorageProvider.addMember(group, user1);
        m_userAdminStorageProvider.addMember(group, user2);

        User user3 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Marcel");
        User user4 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Angelo");
        User user5 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Mark");

        m_userAdminStorageProvider.addRequiredMember(group, user3);
        m_userAdminStorageProvider.addRequiredMember(group, user4);
        m_userAdminStorageProvider.addRequiredMember(group, user5);

        group = (Group) m_userAdminStorageProvider.getRole(m_userAdminFactory, "amdatu-dev");
        Assert.assertEquals(2, group.getMembers().length);
        Assert.assertEquals(3, group.getRequiredMembers().length);

        Collection<Role> members = m_userAdminStorageProvider.getMembers(m_userAdminFactory, group);
        Assert.assertEquals(2, members.size());
        Collection<Role> requiredMembers = m_userAdminStorageProvider.getRequiredMembers(m_userAdminFactory, group);
        Assert.assertEquals(3, requiredMembers.size());

        Assert.assertTrue(m_userAdminStorageProvider.removeMember(group, user2));
        Assert.assertFalse(m_userAdminStorageProvider.removeMember(group, user2));

        Assert.assertTrue(m_userAdminStorageProvider.removeMember(group, user4));
        Assert.assertFalse(m_userAdminStorageProvider.removeMember(group, user4));

        members = m_userAdminStorageProvider.getMembers(m_userAdminFactory, group);
        Assert.assertEquals(1, members.size());
        requiredMembers = m_userAdminStorageProvider.getRequiredMembers(m_userAdminFactory, group);
        Assert.assertEquals(2, requiredMembers.size());

        group = (Group) m_userAdminStorageProvider.getRole(m_userAdminFactory, "amdatu-dev");
        Assert.assertTrue(m_userAdminStorageProvider.deleteRole(group));
        Assert.assertFalse(m_userAdminStorageProvider.deleteRole(group));

    }

    @Test
    public void testFindRolesByFilter() throws StorageException {
        User user1 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.setRoleAttribute(user1, "bedrijf", "GX");
        m_userAdminStorageProvider.setRoleAttribute(user1, "haarkleur", "blond");
        m_userAdminStorageProvider.setRoleAttribute(user1, "oogkleur", "blauw");

        User user2 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Ivo");
        m_userAdminStorageProvider.setRoleAttribute(user2, "bedrijf", "GX");
        m_userAdminStorageProvider.setRoleAttribute(user2, "haarkleur", "zwart");
        m_userAdminStorageProvider.setRoleAttribute(user2, "oogkleur", "bruin");

        Collection<Role> matches1 = m_userAdminStorageProvider.findRoles(m_userAdminFactory, "(haarkleur=blond)");
        Assert.assertEquals("Find returned unexpected amount of roles", 1, matches1.size());

        Collection<Role> matches2 = m_userAdminStorageProvider.findRoles(m_userAdminFactory, "(bedrijf=GX)");
        Assert.assertEquals("Find returned unexpected amount of roles", 2, matches2.size());

        Collection<Role> matches3 = m_userAdminStorageProvider.findRoles(m_userAdminFactory, "(status=n00b)");
        Assert.assertEquals("Find returned unexpected amount of roles", 0, matches3.size());

        // AMDATU-404 check that null filter return all roles
        Collection<Role> matches4 = m_userAdminStorageProvider.findRoles(m_userAdminFactory, null);
        Assert.assertEquals("Find returned unexpected amount of roles", 2, matches4.size());

        // AMDATU-407 check that returned roles are created by factory
        Collection<Role> matches5 = m_userAdminStorageProvider.findRoles(m_userAdminFactory, null);
        for (Role match : matches5) {
            Assert.assertTrue("Roles returned are no created using provided factory",
                MockRole.class.isAssignableFrom(match.getClass()));
        }
    }

    @Test
    public void testGetUserByProperty() throws StorageException {
        User user1 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Bram");
        m_userAdminStorageProvider.setRoleAttribute(user1, "bedrijf", "GX");
        m_userAdminStorageProvider.setRoleAttribute(user1, "haarkleur", "blond");
        m_userAdminStorageProvider.setRoleAttribute(user1, "oogkleur", "blauw");

        User user2 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Ivo");
        m_userAdminStorageProvider.setRoleAttribute(user2, "bedrijf", "GX");
        m_userAdminStorageProvider.setRoleAttribute(user2, "haarkleur", "zwart");
        m_userAdminStorageProvider.setRoleAttribute(user2, "oogkleur", "bruin");

        User matchingUser1 = m_userAdminStorageProvider.getUser(m_userAdminFactory, "haarkleur", "blond");
        Assert.assertNotNull("Expected a matching user", matchingUser1);
        Assert.assertEquals("Expected to find Bram", "Bram", matchingUser1.getName());

        User matchingUser2 = m_userAdminStorageProvider.getUser(m_userAdminFactory, "oogkleur", "bruin");
        Assert.assertNotNull("Expected a matching user", matchingUser2);
        Assert.assertEquals("Expected to find Ivo", "Ivo", matchingUser2.getName());

        User matchingUser3 = m_userAdminStorageProvider.getUser(m_userAdminFactory, "oogkleur", "groen");
        Assert.assertNull("Expected no matching user", matchingUser3);
    }

    /**
     * Testing behavior on tenantEntities with same hashcode() for the id. This is based on knowledge of the
     * implementation but as it is a likely pitfall let's test it anyway to catch future mistakes.
     * 
     * @throws StorageException
     */
    @Test
    public void testWithEqualHashcodes() throws StorageException {

        // Assert the reason for this test
        Assert.assertEquals("BB".hashCode(), "Aa".hashCode());

        User user1 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "BB");
        m_userAdminStorageProvider.setRoleAttribute(user1, "check", "BB user");

        User user2 = m_userAdminStorageProvider.createUser(m_userAdminFactory, "Aa");
        m_userAdminStorageProvider.setRoleAttribute(user2, "check", "Aa user");

        User user3 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "BB");
        Assert.assertNotNull(user3);
        Assert.assertEquals("BB user", user3.getProperties().get("check"));

        User user4 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Aa");
        Assert.assertNotNull(user4);
        Assert.assertEquals("Aa user", user4.getProperties().get("check"));

        m_userAdminStorageProvider.deleteRole(user3);

        User user5 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "BB");
        Assert.assertNull(user5);

        User user6 = (User) m_userAdminStorageProvider.getRole(m_userAdminFactory, "Aa");
        Assert.assertNotNull(user6);
        Assert.assertEquals("Aa user", user6.getProperties().get("check"));
    }

}
