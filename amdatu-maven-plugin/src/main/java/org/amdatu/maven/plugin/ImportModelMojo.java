/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.maven.plugin;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.amdatu.ace.client.AceClient;
import org.amdatu.ace.client.AceClientException;
import org.amdatu.ace.client.AceClientUtil;
import org.amdatu.ace.client.AceClientWorkspace;
import org.amdatu.ace.client.model.Artifact;
import org.amdatu.ace.client.model.Artifact2Feature;
import org.amdatu.ace.client.model.Distribution;
import org.amdatu.ace.client.model.Distribution2Target;
import org.amdatu.ace.client.model.Feature;
import org.amdatu.ace.client.model.Feature2Distribution;
import org.amdatu.ace.client.model.Model;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Goal that imports an Amdatu provisioning model into a configured Amdatu client endpoint.
 * <p/>
 * Sample usage:
 * <pre>
 *  &lt;plugin&gt;
 *    &lt;groupId&gt;org.amdatu.maven&lt;/groupId&gt;
 *    &lt;artifactId&gt;org.amdatu.maven.plugin&lt;/artifactId&gt;
 *    &lt;version&gt;1.0-SNAPSHOT&lt;/version&gt;
 *    &lt;executions&gt;
 *      &lt;execution&gt;
 *        &lt;id&gt;importModel&lt;/id&gt;
 *        &lt;phase&gt;package&lt;/phase&gt;
 *        &lt;goals&gt;
 *          &lt;goal&gt;importModel&lt;/goal&gt;
 *        &lt;/goals&gt;
 *      &lt;/execution&gt;
 *    &lt;/executions&gt;
 * &lt;/plugin&gt;
 * </pre>
 * 
 * @goal importModel
 * @requiresProject false
 */
public class ImportModelMojo extends AbstractMojo {

    // @checkstyle:off

    // Member names do not conform to checkstyle criteria cause
    // maven uses reflection to map them.

    /**
     * Client endpoint URL.
     * 
     * @parameter
     */
    private URL clientEndpoint;

    /**
     * Input filename for model.
     * 
     * @parameter
     * @required
     */
    private File inputFile;

    /**
     * Override flag.
     * 
     * @parameter
     */
    private boolean override;

    // @checkstyle:on

    public void execute() throws MojoExecutionException {

        AceClient client = new AceClient(clientEndpoint.toExternalForm());
        AceClientWorkspace workspace = null;
        try {
            String json = FileUtils.readFileToString(inputFile);
            Model model = AceClientUtil.fromJson(json, Model.class);

            workspace = client.createNewWorkspace();
            if (override) {
                workspace.deleteResources(Artifact.class);
                workspace.deleteResources(Feature.class);
                workspace.deleteResources(Distribution.class);
                // w.deleteResources(Target.class);
                workspace.deleteResources(Artifact2Feature.class);
                workspace.deleteResources(Feature2Distribution.class);
                workspace.deleteResources(Distribution2Target.class);
            }
            workspace.importModel(model);
            workspace.commit();
            workspace.remove();
            workspace = null;

            getLog().info("Imported Amdatu provisioning model from " + inputFile.getAbsolutePath());
        }
        catch (AceClientException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        finally {
            if (workspace != null) {
                try {
                    workspace.remove();
                }
                catch (AceClientException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
        }
    }
}
