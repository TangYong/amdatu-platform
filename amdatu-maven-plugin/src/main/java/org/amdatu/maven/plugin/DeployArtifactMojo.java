/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.maven.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.amdatu.ace.client.AceClient;
import org.amdatu.ace.client.AceClientException;
import org.amdatu.ace.client.AceClientWorkspace;
import org.amdatu.ace.client.model.Artifact;
import org.amdatu.ace.client.model.ArtifactBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.osgi.framework.Constants;

/**
 * Goal that can upload files to the Amdatu OBR and create Amdatu client artifacts
 * based on configuration.
 * <p/>
 * Sample usage:
 * <pre>
 * &lt;plugin&gt;
 *   &lt;groupId&gt;org.amdatu.maven&lt;/groupId&gt;
 *   &lt;artifactId&gt;org.amdatu.maven.plugin&lt;/artifactId&gt;
 *   &lt;executions&gt;
 *     &lt;execution&gt;
 *       &lt;id&gt;uploadToAmdatu&lt;/id&gt;
 *       &lt;phase&gt;package&lt;/phase&gt;
 *         &lt;goals&gt;
 *           &lt;goal&gt;deployArtifact&lt;/goal&gt;
 *         &lt;/goals&gt;
 *       &lt;configuration&gt;
 *         &lt;clientEndpoint&gt;http://localhost:8080/client/work&lt;/clientEndpoint&gt;
 *         &lt;obrEndpoint&gt;http://localhost:8080/client/work&lt;/obrEndpoint&gt;
 *       &lt;/configuration&gt;
 *     &lt;/execution&gt;
 *   &lt;/executions&gt;
 * &lt;/plugin&gt;
 * </pre>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @goal deployArtifact
 * @requiresProject false
 * @description deploy an artifact to Amdatu
 * @threadSafe
 */
public final class DeployArtifactMojo extends AbstractMojo {

    // @checkstyle:off

    // Member names do not conform to checkstyle criteria cause
    // maven uses reflection to map them.

    /**
     * Client endpoint URL. Required when {@code createClientArtifact} is set to {@code true}.
     * 
     * @parameter
     */
    private URL clientEndpoint;

    /**
     * OBR endpoint URL. Required when {@code uploadToObr} is set to {@code true}.
     * 
     * @parameter
     */
    private URL obrEndpoint;

    /**
     * Create artifact on the client.
     * 
     * @parameter default-value="true"
     */
    private boolean createOnClient;

    /**
     * Upload artifact to the obr.
     * 
     * @parameter default-value="true"
     */
    private boolean uploadToObr;

    /**
     * Replace SNAPSHOT with a tstamp.
     * 
     * @parameter default-value="true"
     */
    private boolean tstampSnapshot;

    /**
     * Name of the artifact.
     * 
     * @parameter default-value="${project.name}"
     * @required
     */
    private String artifactName;

    /**
     * Description of the artifact.
     * 
     * @parameter default-value="${project.description}
     * @required
     */
    private String artifactDescription;

    /**
     * Version of the artifact.
     * 
     * @parameter default-value="${project.version}
     * @required
     */
    private String artifactVersion;

    /**
     * File of the artifact to use.
     * 
     * @parameter default-value="${project.artifact.file}
     */
    private File artifactFile;

    /**
     * Artifact URL.
     * 
     * @parameter
     */
    private String artifactProcessor;

    /**
     * Bundle Name (only relevant for bundles).
     * 
     * @parameter default-value="${project.name}
     * @required
     */
    private String bundleName;

    /**
     * Bundle SymbolicName (only relevant for bundles).
     * 
     * @parameter default-value="${project.artifactId}
     * @required
     */
    private String bundleSymbolicName;

    /**
     * Additional attributes.
     * 
     * @parameter
     */
    private Map<String, String> attributes;

    /**
     * Additional attributes.
     * 
     * @parameter
     */
    private Map<String, String> tags;

    // @checkstyle:on

    public void execute() throws MojoExecutionException {

        // checks
        if (uploadToObr && obrEndpoint == null) {
            throw new MojoExecutionException("No obrEndpoint configured while uploadToObr is true.");
        }
        if (createOnClient && clientEndpoint == null) {
            throw new MojoExecutionException("No clientEndpoint configured while createOnClient is true.");
        }
        if (artifactFile == null) {
            throw new MojoExecutionException("No valid artifactFile configured found.");
        }

        // defaults
        String outputVersion = artifactVersion;
        File outputFile = artifactFile;

        // snapshots
        if (isSnapshotVersion(artifactVersion) && tstampSnapshot) {
            outputVersion = createTstampVersion(artifactVersion);
            outputFile = new File(artifactFile.getAbsolutePath().replace(artifactVersion, outputVersion));
            if (isBundle(artifactFile)) {
                createNewBundleArtifactFile(artifactFile, outputFile, outputVersion);
            }
            else {
                copyArtifactFile(artifactFile, outputFile);
            }
        }

        // uploads
        URL artifactURL = getUrlFromFile(outputFile);
        getLog().info(artifactURL.toExternalForm());
        if (uploadToObr) {
            artifactURL = uploadToObr(outputFile);
            getLog().info(artifactURL.toExternalForm());
        }

        // creates
        if (createOnClient) {
            createArtifactOnClient(artifactURL, outputVersion);
            getLog().info(artifactURL.toExternalForm());
        }
    }

    private boolean isSnapshotVersion(String version) {
        return version.endsWith("-SNAPSHOT");
    }

    private String createTstampVersion(String version) {
        return version.replace("-SNAPSHOT", "." + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
    }

    private boolean isBundle(File file) throws MojoExecutionException {
        JarInputStream jis = null;
        Manifest manifest = null;
        try {
            jis = new JarInputStream(new FileInputStream(file));
            manifest = jis.getManifest();
        }
        catch (FileNotFoundException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        finally {
            if (jis != null) {
                try {
                    jis.close();
                }
                catch (IOException e) {

                }
            }
        }
        return (manifest != null && manifest.getMainAttributes().getValue(Constants.BUNDLE_SYMBOLICNAME) != null);
    }

    private URL getUrlFromFile(File file) throws MojoExecutionException {
        URL url = null;
        try {
            url = file.toURI().toURL();
        }
        catch (MalformedURLException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        return url;
    }

    private void copyArtifactFile(File from, File to) throws MojoExecutionException {
        try {
            FileUtils.copyFile(from, to);
        }
        catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    private void createNewBundleArtifactFile(File oldFile, File newFile, String newVersion)
        throws MojoExecutionException {
        JarInputStream jis = null;
        JarOutputStream jos = null;

        try {
            jis = new JarInputStream(new FileInputStream(oldFile));

            Manifest manifest = jis.getManifest();
            Attributes mfattributes = manifest.getMainAttributes();
            mfattributes.putValue(Constants.BUNDLE_VERSION, newVersion);

            jos = new JarOutputStream(new FileOutputStream(newFile), manifest);

            byte[] buffer = new byte[4096];
            JarEntry entry = jis.getNextJarEntry();
            while (entry != null) {
                jos.putNextEntry(new JarEntry(entry.getName()));
                int bytes = jis.read(buffer);
                while (bytes != -1) {
                    jos.write(buffer, 0, bytes);
                    bytes = jis.read(buffer);
                }
                jos.closeEntry();
                jis.closeEntry();
                entry = jis.getNextJarEntry();
            }
            jos.flush();
            jos.close();
            jis.close();
        }
        catch (IOException e) {
            throw new MojoExecutionException("File to create bundle artifact file", e);
        }
        finally {
            if (jis != null) {
                try {
                    jis.close();
                }
                catch (IOException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
            if (jos != null) {
                try {
                    jos.close();
                }
                catch (IOException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
        }
    }

    private URL uploadToObr(File file) throws MojoExecutionException {

        URL artifactObrUrl = null;
        OutputStream out = null;
        InputStream in = null;
        try {
            artifactObrUrl = new URL(obrEndpoint.toExternalForm() + "/" + file.getName());
            URLConnection connection = artifactObrUrl.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);

            URL artifactFileUrl = file.toURI().toURL();
            in = artifactFileUrl.openStream();
            out = connection.getOutputStream();
            byte[] buf = new byte[8 * 1024];
            for (int i = in.read(buf); i != -1; i = in.read(buf)) {
                out.write(buf, 0, i);
            }
            out.flush();
            out.close();

            HttpURLConnection conn = (HttpURLConnection) connection;
            int status = conn.getResponseCode();
            if (status != 200) {
                throw new MojoExecutionException("Failed to upload file to the OBR at "
                    + artifactObrUrl.toExternalForm() + " (responsecode: " + status + ").");
            }
        }
        catch (MalformedURLException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                }
                catch (IOException e) {
                    throw new MojoExecutionException(e.getMessage(), e);

                }
            }
            if (out != null) {
                try {
                    out.close();
                }
                catch (IOException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
        }
        return artifactObrUrl;
    }

    private void createArtifactOnClient(URL url, String version) throws MojoExecutionException {
        AceClientWorkspace workspace = null;
        try {

            ArtifactBuilder builder = new ArtifactBuilder()
                .setName(artifactName)
                .setDescription(artifactDescription)
                .setUrl(url.toExternalForm())
                .setProcessorPid(artifactProcessor);

            if (isBundle(artifactFile)) {
                builder.isBundle()
                    .setBundleName(bundleName)
                    .setBundleSymbolicName(bundleSymbolicName)
                    .setBundleVersion(version);
            }
            else {
                // best guess for now
                builder.isConfig()
                    .setConfigFileName(artifactFile.getName());
            }

            if (attributes != null) {
                for (Entry<String, String> entry : attributes.entrySet()) {
                    builder.setAttribute(entry.getKey(), entry.getValue());
                }
            }

            if (tags != null) {
                for (Entry<String, String> entry : tags.entrySet()) {
                    builder.setTag(entry.getKey(), entry.getValue());
                }
            }

            AceClient client = new AceClient(clientEndpoint.toExternalForm());
            workspace = client.createNewWorkspace();
            Artifact artifact = builder.build();
            workspace.createResource(artifact);
            workspace.commit();
        }
        catch (AceClientException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
        finally {
            if (workspace != null) {
                try {
                    workspace.remove();
                }
                catch (AceClientException e) {
                    throw new MojoExecutionException(e.getMessage(), e);
                }
            }
        }
    }
}
