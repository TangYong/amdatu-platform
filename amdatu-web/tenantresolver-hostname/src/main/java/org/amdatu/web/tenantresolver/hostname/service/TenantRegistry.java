/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.tenantresolver.hostname.service;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.amdatu.tenant.Tenant;
import org.amdatu.web.tenantresolver.hostname.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * {@code HostnameTenantMappingRegistry} keeps a mapping of available {@link Tenant} services using their hostnames
 * as key. This provides fast lookups and reduces contention on the service registry.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public final class TenantRegistry {

    // maps current mappings
    private final ConcurrentMap<ServiceReference, Tenant> m_referenceToMapping =
        new ConcurrentHashMap<ServiceReference, Tenant>();
    // maps fast hostname lookup
    private final ConcurrentMap<String, Tenant> m_hostNameToTenantMapping = new ConcurrentHashMap<String, Tenant>();

    private final ReentrantReadWriteLock m_lock = new ReentrantReadWriteLock();

    private volatile LogService m_logService;

    public Tenant getTenant(String hostname) {
        m_lock.readLock().lock();
        try {
            return m_hostNameToTenantMapping.get(hostname);
        }
        finally {
            m_lock.readLock().unlock();
        }
    }

    public void addTenant(ServiceReference reference, Tenant tenant) {
        m_lock.writeLock().lock();
        try {
            addMapping(reference, tenant);
        }
        finally {
            m_lock.writeLock().unlock();
        }
    }

    public void updateTenant(ServiceReference reference, Tenant tenant) {
        m_lock.writeLock().lock();
        try {
            removeMapping(reference);
            addMapping(reference, tenant);
        }
        finally {
            m_lock.writeLock().unlock();
        }
    }

    public void removeTenant(ServiceReference reference, Tenant tenant) {
        m_lock.writeLock().lock();
        try {
            removeMapping(reference);
        }
        finally {
            m_lock.writeLock().unlock();
        }
    }

    private void addMapping(ServiceReference reference, Tenant tenant) {
        String[] hostnames = getHostnames(reference);
        if (hostnames == null || hostnames.length == 0) {
            return;
        }

        m_referenceToMapping.put(reference, tenant);
        for (String hostname : hostnames) {
            hostname = hostname.trim();
            if ("".equals(hostname)) {
                m_logService.log(LogService.LOG_WARNING, "Tenant " + tenant.getPID()
                    + " specifies illegal hostname mapping to " + hostname);
                continue;
            }

            Tenant oldTenant = m_hostNameToTenantMapping.put(hostname, tenant);
            if (oldTenant != null) {
                m_logService.log(LogService.LOG_WARNING, "Tenant " + tenant.getPID()
                    + " overwrites hostname mapping for " + hostname + " previously mapped to "
                    + oldTenant.getPID());
            }
        }
    }

    private void removeMapping(ServiceReference reference) {
        Tenant mapping = m_referenceToMapping.remove(reference);
        if (mapping == null) {
            return;
        }

        Set<String> removeHostnames = new HashSet<String>();
        for (Entry<String, Tenant> entry : m_hostNameToTenantMapping.entrySet()) {
            if (entry.getValue() == mapping) {
                removeHostnames.add(entry.getKey());
            }
        }
        for (String hostname : removeHostnames) {
            m_hostNameToTenantMapping.remove(hostname);
        }
    }

    private String[] getHostnames(ServiceReference reference) {
        Object hostnameProp = reference.getProperty(Constants.HOSTNAMES_KEY);
        if (hostnameProp != null && hostnameProp instanceof String) {
            return ((String) hostnameProp).split(",");
        }
        if (hostnameProp != null && hostnameProp instanceof String[]) {
            return (String[]) hostnameProp;
        }
        return null;
    }
}