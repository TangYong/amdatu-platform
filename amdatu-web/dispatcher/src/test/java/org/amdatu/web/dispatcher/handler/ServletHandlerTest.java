/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.util.Arrays;

import javax.servlet.Servlet;

import junit.framework.Assert;

import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

public class ServletHandlerTest {

    private Mockery m_context;

    @Before
    public void setUp() {
        m_context = new Mockery();
    }

    @Test
    public void testServletHandlerCreate() {

//        ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Servlet servlet = m_context.mock(Servlet.class);

        ServletHandler sh1 = new ServletHandler(1, 2, contextId, servlet, "/helloworld", "t1");
        Assert.assertEquals(1, sh1.getId());
        Assert.assertEquals(sh1.getTenantPID(), "t1");
        Assert.assertEquals(sh1.getContextId(), contextId);
        Assert.assertEquals(sh1.getRanking(), 2);
        Assert.assertEquals(sh1.getAlias(), "/helloworld");
        Assert.assertEquals(sh1.getServlet(), servlet);
    }

    @Test
    public void testServletHandlerMatching() {

//      ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Servlet servlet = m_context.mock(Servlet.class);

        ServletHandler sh1 = new ServletHandler(1, 0, contextId, servlet, "", "");
        Assert.assertTrue(sh1.matches(null));
        Assert.assertTrue(sh1.matches("/"));
        Assert.assertTrue(sh1.matches("/whatever"));

        ServletHandler sh2 = new ServletHandler(1, 0, contextId, servlet, "/", "");
        Assert.assertTrue(sh2.matches(null));
        Assert.assertTrue(sh2.matches("/"));
        Assert.assertTrue(sh2.matches("/whatever"));

        ServletHandler sh3 = new ServletHandler(1, 0, contextId, servlet, "/hello", "");
        Assert.assertTrue(sh3.matches("/hello"));
        Assert.assertTrue(sh3.matches("/hello/"));
        Assert.assertTrue(sh3.matches("/hello/whatever"));
        Assert.assertFalse(sh3.matches("/hell0"));
        Assert.assertFalse(sh3.matches("/hell0/"));
    }

    @Test
    public void testServletHandlerSorting() {

//      ExtServletContext context = m_context.mock(ExtServletContext.class);
        String contextId = "123";
        Servlet servlet = m_context.mock(Servlet.class);

        ServletHandler sh1 = new ServletHandler(1, 0, contextId, servlet, "/hello", "");
        ServletHandler sh2 = new ServletHandler(2, 8, contextId, servlet, "/hello", "");
        ServletHandler sh3 = new ServletHandler(3, 0, contextId, servlet, "/hello/world", "");
        ServletHandler sh4 = new ServletHandler(4, 0, contextId, servlet, "/hello/world", "");
        ServletHandler sh5 = new ServletHandler(5, 2, contextId, servlet, "/hello/world", "");

        // specific path rules
        // higher service.ranking rules when path equals
        // lower service.id rules when service.ranking equals
        ServletHandler[] expectedArray = new ServletHandler[] { sh5, sh3, sh4, sh2, sh1 };
        ServletHandler[] sortedArray = new ServletHandler[] { sh1, sh2, sh3, sh4, sh5 };
        Arrays.sort(sortedArray);
        Assert.assertTrue(Arrays.equals(expectedArray, sortedArray));
    }
}
