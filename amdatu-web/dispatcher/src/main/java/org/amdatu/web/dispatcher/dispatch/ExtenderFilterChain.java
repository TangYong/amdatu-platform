/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.dispatch;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.dispatcher.handler.FilterHandler;

/**
 * Custom <code>FilterChain</code> implementation that invokes an array of <code>FilterHandler</code>
 * instances wrapping <code>ExtenderFilter</code> objects and continues on to the specified <code>
 * TenantFilterPipeline</code>.
 */
public final class ExtenderFilterChain extends HttpFilterChain {

    private final FilterHandler[] m_filterHandlers;
    private final CustomFilterPipeline m_tenantFilterPipeline;
    private final FilterChain m_proceedingChain;
    private int m_index = -1;

    public ExtenderFilterChain(FilterHandler[] filterHandlers, CustomFilterPipeline tenantFilterPipeline,
        FilterChain proceedingChain) {
        m_filterHandlers = filterHandlers;
        m_tenantFilterPipeline = tenantFilterPipeline;
        m_proceedingChain = proceedingChain;
    }

    protected void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        throws IOException, ServletException {
        m_index++;
        if (m_index < m_filterHandlers.length) {
            m_filterHandlers[m_index].handle(httpServletRequest, httpServletResponse, this);
        }
        else {
            m_tenantFilterPipeline.dispatch(httpServletRequest, httpServletResponse, m_proceedingChain);
        }
    }
}
