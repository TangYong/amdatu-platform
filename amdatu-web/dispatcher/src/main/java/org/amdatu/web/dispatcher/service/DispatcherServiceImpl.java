/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.service;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.dispatcher.DispatchExtenderFilter;
import org.amdatu.web.dispatcher.context.DefaultHttpContext;
import org.amdatu.web.dispatcher.dispatch.CustomFilterPipeline;
import org.amdatu.web.dispatcher.dispatch.ExtenderFilterPipeline;
import org.amdatu.web.dispatcher.filter.DispatchInterceptFilter;
import org.amdatu.web.dispatcher.handler.FilterHandlerRegistry;
import org.amdatu.web.dispatcher.handler.ServletHandlerRegistry;
import org.apache.felix.http.api.ExtHttpService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

public class DispatcherServiceImpl implements LogService {

    // Injected by DM
    private volatile BundleContext m_bundleContext;
    private volatile ExtHttpService m_httpService;
    private volatile LogService m_logService;

    private DispatchInterceptFilter m_interceptorFilter;
    private ServletHandlerRegistry m_servletHandlerRegistry;
    private FilterHandlerRegistry m_filterHandlerRegistry;
    private ServletContext m_extServletContext;

    /**
     * Called by the dependency manager when this component is ready to start.
     * 
     * @throws RuntimeException in case this service failed to start.
     */
    public void start() throws RuntimeException {
        m_interceptorFilter = new DispatchInterceptFilter();
        m_interceptorFilter.setDispatcherService(this);
        try {
            m_httpService.registerFilter(m_interceptorFilter, ".*", new Hashtable<String, Object>(), 0,
                new DefaultHttpContext(m_bundleContext.getBundle()));
            m_extServletContext = m_interceptorFilter.getServletContext();

            m_servletHandlerRegistry = new ServletHandlerRegistry();
            m_servletHandlerRegistry.setLogService(this);
            m_servletHandlerRegistry.setServletContext(m_extServletContext);

            m_filterHandlerRegistry = new FilterHandlerRegistry();
            m_filterHandlerRegistry.setLogService(this);
            m_filterHandlerRegistry.setServletContext(m_extServletContext);
        }
        catch (ServletException e) {
            throw new RuntimeException(DispatcherServiceImpl.class.getSimpleName() + " failed to start", e);
        }
    }

    /**
     * Called by the dependency manager when this service is stopped.
     */
    public void stop() {
        m_httpService.unregisterFilter(m_interceptorFilter);

        m_servletHandlerRegistry = null;
        m_filterHandlerRegistry = null;

        m_interceptorFilter.setDispatcherService(null);
        m_interceptorFilter = null;
    }

    /**
     * Called by the dependency manager when a new extender filter becomes available.
     * 
     * @param serviceReference the service reference of the extender filter;
     * @param extenderFilter the extender filter to add.
     */
    public void addExtenderFilter(ServiceReference serviceReference, DispatchExtenderFilter extenderFilter) {
        m_filterHandlerRegistry.addDispatchExtenderFilter(serviceReference, extenderFilter);
    }

    /**
     * Called by the dependency manager when an extender filter goes away.
     * 
     * @param serviceReference the service reference of the extender filter;
     * @param extenderFilter the extender filter to remove.
     */
    public void removeExtenderFilter(ServiceReference serviceReference, DispatchExtenderFilter extenderFilter) {
        m_filterHandlerRegistry.removeDispatchExtenderFilter(serviceReference, extenderFilter);
    }

    /**
     * Called by the dependency manager when a new filter becomes available.
     * 
     * @param serviceReference the service reference of the filter;
     * @param filter the filter to add.
     */
    public void addFilter(ServiceReference serviceReference, Filter filter) {
        m_filterHandlerRegistry.addFilterHandler(serviceReference, filter);
    }

    /**
     * Called by the dependency manager when a filter goes away.
     * 
     * @param serviceReference the service reference of the filter;
     * @param filter the filter to remove.
     */
    public void removeFilter(ServiceReference serviceReference, Filter filter) {
        m_filterHandlerRegistry.removeFilterHandler(serviceReference, filter);
    }

    /**
     * Called by the dependency manager when a new servlet becomes available.
     * 
     * @param serviceReference the service reference of the servlet;
     * @param servlet the servlet to add.
     */
    public void addServlet(ServiceReference serviceReference, Servlet servlet) {
        m_servletHandlerRegistry.addServletHandler(serviceReference, servlet);
    }

    /**
     * Called by the dependency manager when a servlet goes away.
     * 
     * @param serviceReference the service reference of the servlet;
     * @param servlet the servlet to remove.
     */
    public void removeServlet(ServiceReference serviceReference, Servlet servlet) {
        m_servletHandlerRegistry.removeServletHandler(serviceReference, servlet);
    }

    /**
     * Called by the dependency manager when a new http context becomes available.
     * 
     * @param serviceReference the service reference of the http context;
     * @param httpContext the http context to add.
     */
    public void addHttpContext(ServiceReference serviceReference, HttpContext httpContext) {
        String contextId = getStringProperty(serviceReference, Constants.CONTEXT_ID_KEY);
        if (contextId != null) {
            m_filterHandlerRegistry.addHttpContext(contextId, httpContext);
            m_servletHandlerRegistry.addHttpContext(contextId, httpContext);
        }
    }

    /**
     * Called by the dependency manager when a http context goes away.
     * 
     * @param serviceReference the service reference of the http context;
     * @param httpContext the http context to remove.
     */
    public void removeHttpContext(ServiceReference serviceReference, HttpContext httpContext) {
        String contextId = getStringProperty(serviceReference, Constants.CONTEXT_ID_KEY);
        if (contextId != null) {
            m_filterHandlerRegistry.removeHttpContext(contextId);
            m_servletHandlerRegistry.removeHttpContext(contextId);
        }
    }

    /**
     * Dispatches a request through all current filters, servlets and extender filters.
     * 
     * @param httpServletRequest the servlet request;
     * @param httpServletResponse the servlet response;
     * @param filterChain the filter chain to use.
     * @throws IOException in case of I/O problems;
     * @throws ServletException in case generic servlet problems.
     */
    public void dispatchRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
        FilterChain filterChain) throws IOException, ServletException {

        ExtenderFilterPipeline pipeline =
            new ExtenderFilterPipeline(m_filterHandlerRegistry.getExtenderFilterHandlers(), new CustomFilterPipeline(
                m_filterHandlerRegistry, m_servletHandlerRegistry));
        pipeline.dispatch(httpServletRequest, httpServletResponse, filterChain);
    }

    /**
     * Retrieves a string property from a service reference.
     * 
     * @param ref the service reference to use;
     * @param key the name of the property to retrieve.
     * @return the string-value of the requested key, or <code>null</code> if either the value was not a string,
     *         or not available.
     */
    private String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }

    /*
     * LogService interface
     */

    public void log(int level, String message) {
        m_logService.log(level, message);
    }

    public void log(int level, String message, Throwable exception) {
        this.m_logService.log(level, message, exception);
    }

    public void log(ServiceReference sr, int level, String message) {
        this.m_logService.log(sr, level, message);
    }

    public void log(ServiceReference sr, int level, String message, Throwable exception) {
        this.m_logService.log(sr, level, message, exception);
    }
}
