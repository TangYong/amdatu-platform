/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.osgi.service.http.HttpContext;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
final class ServletHandlerRequest extends HttpServletRequestWrapper {

    private final String m_alias;
    private String m_contextPath;
    private String m_pathInfo;
    private boolean m_pathInfoCalculated = false;

    public ServletHandlerRequest(HttpServletRequest req, String alias) {
        super(req);
        m_alias = alias;
    }

    @Override
    public String getAuthType() {
        String authType = (String) getAttribute(HttpContext.AUTHENTICATION_TYPE);
        if (authType != null) {
            return authType;
        }
        return super.getAuthType();
    }

    @Override
    public String getContextPath() {
        if (m_contextPath == null) {
            final String context = super.getContextPath();
            final String servlet = super.getServletPath();
            if (context.length() == 0) {
                m_contextPath = servlet;
            }
            else if (servlet.length() == 0) {
                m_contextPath = context;
            }
            else {
                m_contextPath = context + servlet;
            }
        }
        return m_contextPath;
    }

    @Override
    public String getPathInfo() {
        if (!m_pathInfoCalculated) {
            m_pathInfo = calculatePathInfo();
            m_pathInfoCalculated = true;
        }
        return m_pathInfo;
    }

    @Override
    public String getPathTranslated() {
        final String info = getPathInfo();
        return (null == info) ? null : getRealPath(info);
    }

    @Override
    public String getRemoteUser() {
        String remoteUser = (String) getAttribute(HttpContext.REMOTE_USER);
        if (remoteUser != null) {
            return remoteUser;
        }
        return super.getRemoteUser();
    }

    @Override
    public String getServletPath() {
        if ("/".equals(m_alias)) {
            return "";
        }
        return m_alias;
    }

    private String calculatePathInfo() {
        String pathInfo = super.getPathInfo();
        if (pathInfo != null) {
            if (!"/".equals(m_alias)) {
                pathInfo = pathInfo.substring(m_alias.length());
            }
            if (pathInfo.length() == 0) {
                pathInfo = null;
            }
        }
        return pathInfo;
    }
}
