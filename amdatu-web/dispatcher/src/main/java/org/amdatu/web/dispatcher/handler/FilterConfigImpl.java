/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;

public final class FilterConfigImpl implements FilterConfig {

    private final String m_name;
    private final ServletContext m_servletcontext;
    private final Map<String, String> m_initParams;

    public FilterConfigImpl(String name, ServletContext context, Map<String, String> initParams) {
        m_name = name;
        m_servletcontext = context;
        m_initParams = initParams;
    }

    public String getFilterName() {
        return m_name;
    }

    public ServletContext getServletContext() {
        return m_servletcontext;
    }

    public String getInitParameter(String name) {
        return m_initParams.get(name);
    }

    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(m_initParams.keySet());
    }
}
