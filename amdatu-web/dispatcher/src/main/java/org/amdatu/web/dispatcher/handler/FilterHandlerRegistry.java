/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import static org.amdatu.tenant.Constants.PID_KEY;
import static org.osgi.framework.Constants.SERVICE_ID;
import static org.osgi.framework.Constants.SERVICE_RANKING;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.dispatcher.DispatchExtenderFilter;
import org.amdatu.web.dispatcher.context.HandlerServletContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

/**
 * Handler registry that manages <code>FilterHandler</code> instances lifecycle
 * based on <code>HttpContext</code> availability.
 * 
 * TODO revisit threadsafety / spawning threads
 * TODO add eager filter matching and optimize
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class FilterHandlerRegistry extends AbstractHandlerRegistry {
    private static final String MAGIC_NOTENANT_PID = "";
    private final ReentrantReadWriteLock m_extenderFilterHandlersLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock m_filterHandlersLock = new ReentrantReadWriteLock();
    private final Map<ServiceReference, FilterHandler> m_filterHandlers =
        new HashMap<ServiceReference, FilterHandler>();
    private final Map<String, FilterHandler[]> m_tenantFilterHandlerArrays = new HashMap<String, FilterHandler[]>();
    private final Map<ServiceReference, FilterHandler> m_extenderFilterHandlers =
        new HashMap<ServiceReference, FilterHandler>();
    private FilterHandler[] m_extenderFilterHandlerArray = new FilterHandler[] {};

    public void addDispatchExtenderFilter(ServiceReference serviceReference, DispatchExtenderFilter extenderFilter) {

        int serviceId = getIntProperty(serviceReference, SERVICE_ID, 0);
        int serviceRanking = getIntProperty(serviceReference, SERVICE_RANKING, 0);
        String contextId = getStringProperty(serviceReference, Constants.CONTEXT_ID_KEY);
        if (contextId == null) {
            contextId = "";
        }
        String pattern = getStringProperty(serviceReference, Constants.PATTERN_KEY);
        if (pattern == null) {
            return;
        }

        final FilterHandler filterHandler =
            new FilterHandler(serviceId, contextId, extenderFilter, pattern, serviceRanking, null);
        filterHandler.setInitParams(getInitParams(serviceReference));

        m_extenderFilterHandlersLock.writeLock().lock();
        try {
            if (m_extenderFilterHandlers.containsKey(serviceReference)) {
                throw new IllegalStateException("unforseen duplicate extender....");
            }

            HttpContext context = getHttpContext(contextId, serviceReference.getBundle());
            if (context != null) {
                HandlerServletContext servletContextWrapper =
                    new HandlerServletContext(getServletContext(), context);
                filterHandler.setExtServletContext(servletContextWrapper);

                new Thread(new Runnable() {
                    public void run() {
                        try {
                            filterHandler.init();
                        }
                        catch (ServletException e) {
                            filterHandler.destroy();
                        }
                    }
                }).start();
            }

            m_extenderFilterHandlers.put(serviceReference, filterHandler);
            m_extenderFilterHandlerArray =
                m_extenderFilterHandlers.values().toArray(new FilterHandler[m_extenderFilterHandlers.size()]);
            Arrays.sort(m_extenderFilterHandlerArray);
        }
        finally {
            m_extenderFilterHandlersLock.writeLock().unlock();
        }
    }

    public void removeDispatchExtenderFilter(ServiceReference serviceReference, DispatchExtenderFilter extenderFilter) {
        m_extenderFilterHandlersLock.writeLock().lock();
        try {
            if (!m_extenderFilterHandlers.containsKey(serviceReference)) {
                throw new IllegalStateException("unforseen unknown extender....");
            }
            final FilterHandler handler = m_extenderFilterHandlers.remove(serviceReference);
            m_extenderFilterHandlerArray =
                m_extenderFilterHandlers.values().toArray(new FilterHandler[m_extenderFilterHandlers.size()]);
            Arrays.sort(m_extenderFilterHandlerArray);
            if (handler != null) {
                new Thread(new Runnable() {
                    public void run() {
                        handler.destroy();
                    }
                }).start();
            }
        }
        finally {
            m_extenderFilterHandlersLock.writeLock().unlock();
        }
    }

    public FilterHandler[] getExtenderFilterHandlers() {
        return m_extenderFilterHandlerArray;
    }

    public void addFilterHandler(ServiceReference serviceReference, Filter filter) {

        int serviceId = getIntProperty(serviceReference, SERVICE_ID, 0);
        int serviceRanking = getIntProperty(serviceReference, SERVICE_RANKING, 0);
        String contextId = getStringProperty(serviceReference, Constants.CONTEXT_ID_KEY);
        if (contextId == null) {
            contextId = "";
        }
        String pattern = getStringProperty(serviceReference, Constants.PATTERN_KEY);
        if (pattern == null) {
            if (getLogService() != null) {
                getLogService().log(LogService.LOG_WARNING, "Cannot register a filter without pattern");
            }
            return;
        }
        String tenantPID = getStringProperty(serviceReference, PID_KEY);

        final FilterHandler filterHandler =
            new FilterHandler(serviceId, contextId, filter, pattern, serviceRanking, tenantPID);
        filterHandler.setInitParams(getInitParams(serviceReference));

        m_filterHandlersLock.writeLock().lock();
        try {
            if (m_filterHandlers.containsKey(serviceReference)) {
                throw new IllegalStateException("Unexpected.... ");
            }
            HttpContext context = getHttpContext(contextId, serviceReference.getBundle());
            if (context != null) {
                HandlerServletContext servletContextWrapper =
                    new HandlerServletContext(getServletContext(), context);
                filterHandler.setExtServletContext(servletContextWrapper);

                new Thread(new Runnable() {
                    public void run() {
                        try {
                            filterHandler.init();
                        }
                        catch (ServletException e) {
                            filterHandler.destroy();
                        }
                    }
                }).start();
            }

            m_filterHandlers.put(serviceReference, filterHandler);
            m_tenantFilterHandlerArrays.clear();
        }
        finally {
            m_filterHandlersLock.writeLock().unlock();
        }
    }

    public void removeFilterHandler(ServiceReference serviceReference, Filter filter) {
        m_filterHandlersLock.writeLock().lock();
        try {
            if (!m_filterHandlers.containsKey(serviceReference)) {
                throw new IllegalStateException("Unexpected.... ");
            }
            final FilterHandler filterHandler = m_filterHandlers.remove(serviceReference);
            m_tenantFilterHandlerArrays.clear();
            if (filterHandler != null) {
                new Thread(new Runnable() {
                    public void run() {
                        filterHandler.destroy();
                    }
                }).start();
            }
        }
        finally {
            m_filterHandlersLock.writeLock().unlock();
        }
    }

    public FilterHandler[] getFilterHandlers(HttpServletRequest httpServletRequest) {
        String tenantPID = (String) httpServletRequest.getAttribute(Constants.TENANTPID_REQUESTCONTEXT_KEY);
        if (tenantPID == null) {
            tenantPID = MAGIC_NOTENANT_PID;
        }

        m_filterHandlersLock.readLock().lock();
        try {
            FilterHandler[] handlerArray = m_tenantFilterHandlerArrays.get(tenantPID);
            if (handlerArray != null) {
                return handlerArray;
            }
        }
        finally {
            m_filterHandlersLock.readLock().unlock();
        }

        m_filterHandlersLock.writeLock().lock();
        try {
            // retry
            FilterHandler[] handlerArray = m_tenantFilterHandlerArrays.get(tenantPID);
            if (handlerArray != null) {
                return handlerArray;
            }

            // build
            if (tenantPID.equals(MAGIC_NOTENANT_PID)) {
                List<FilterHandler> filterHandlers = new LinkedList<FilterHandler>();
                for (FilterHandler filterHandler : m_filterHandlers.values()) {
                    if (filterHandler.getTenantPID() == null || filterHandler.getTenantPID() == MAGIC_NOTENANT_PID) {
                        filterHandlers.add(filterHandler);
                    }
                }
                handlerArray = filterHandlers.toArray(new FilterHandler[filterHandlers.size()]);
            }
            else {
                List<FilterHandler> filterHandlers = new LinkedList<FilterHandler>();
                for (FilterHandler filterHandler : m_filterHandlers.values()) {
                    if (filterHandler.getTenantPID() == null || filterHandler.getTenantPID() == MAGIC_NOTENANT_PID
                        || filterHandler.getTenantPID().equals(tenantPID)) {
                        filterHandlers.add(filterHandler);
                    }
                }
                handlerArray = filterHandlers.toArray(new FilterHandler[filterHandlers.size()]);
            }

            Arrays.sort(handlerArray);
            m_tenantFilterHandlerArrays.put(tenantPID, handlerArray);
            return handlerArray;
        }
        finally {
            m_filterHandlersLock.writeLock().unlock();
        }
    }

    @Override
    public void httpContextAdded(String contextId, HttpContext context) {

        m_extenderFilterHandlersLock.readLock().lock();
        try {

            for (final FilterHandler handler : m_extenderFilterHandlers.values()) {
                if (!handler.isActive() && handler.getContextId().equals(contextId)) {
                    HandlerServletContext servletContextWrapper =
                        new HandlerServletContext(getServletContext(), context);
                    handler.setExtServletContext(servletContextWrapper);

                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                handler.init();
                            }
                            catch (ServletException e) {
                                handler.destroy();
                            }
                        }
                    }).start();
                }
            }
        }
        finally {
            m_extenderFilterHandlersLock.readLock().unlock();
        }

        m_filterHandlersLock.readLock().lock();
        try {

            for (final FilterHandler handler : m_filterHandlers.values()) {
                if (!handler.isActive() && handler.getContextId().equals(contextId)) {
                    HandlerServletContext servletContextWrapper =
                        new HandlerServletContext(getServletContext(), context);
                    handler.setExtServletContext(servletContextWrapper);
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                handler.init();
                            }
                            catch (ServletException e) {
                                handler.destroy();
                            }
                        }
                    }).start();
                }
            }
        }
        finally {
            m_filterHandlersLock.readLock().unlock();
        }
    }

    @Override
    public void httpContextRemoved(String contextId) {
        m_extenderFilterHandlersLock.readLock().lock();
        try {
            for (final FilterHandler handler : m_extenderFilterHandlers.values()) {
                if (handler.isActive() && handler.getContextId().equals(contextId)) {
                    new Thread(new Runnable() {
                        public void run() {
                            handler.destroy();
                        }
                    }).start();
                }
            }
        }
        finally {
            m_extenderFilterHandlersLock.readLock().unlock();
        }

        m_filterHandlersLock.readLock().lock();
        try {
            for (final FilterHandler handler : m_filterHandlers.values()) {
                if (handler.isActive() && handler.getContextId().equals(contextId)) {
                    new Thread(new Runnable() {
                        public void run() {
                            handler.destroy();
                        }
                    }).start();
                }
            }
        }
        finally {
            m_filterHandlersLock.readLock().unlock();
        }
    }
}
