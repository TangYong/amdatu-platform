/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;

import org.amdatu.web.dispatcher.context.DefaultHttpContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

public abstract class AbstractHandlerRegistry {

    /**
     * Generic service registration property prefix for properties that should
     * be passed through <code>ServletConfig</code> or <code>FilterConfig</code> to
     * the components init method. The prefix will be stripped before from the key
     * and the value must be of type <code>String</code>.
     */
    protected static final String INIT_KEY_PREFIX = "init.";

    private volatile LogService m_logService;
    private ServletContext m_servletContext;

    // handling httpcontext
    private final ConcurrentHashMap<String, HttpContext> m_httpContexts =
        new ConcurrentHashMap<String, HttpContext>();

    public void addHttpContext(String contextId, HttpContext context) {
        HttpContext previousHttpContext = m_httpContexts.putIfAbsent(contextId, context);
        if (previousHttpContext == null) {
            httpContextAdded(contextId, context);
        }
    }

    public abstract void httpContextAdded(String contextId, HttpContext context);

    public void removeHttpContext(String contextId) {
        HttpContext previousHttpContext = m_httpContexts.remove(contextId);
        if (previousHttpContext != null) {
            httpContextRemoved(contextId);
        }
    }

    public abstract void httpContextRemoved(String contextId);

    public HttpContext getHttpContext(String contextId, Bundle bundle) {
        if (contextId == null || contextId.equals("")) {
            return new DefaultHttpContext(bundle);
        }
        HttpContext context = m_httpContexts.get(contextId);
        return context;
    }

    // handling httpcontext

    public AbstractHandlerRegistry() {
    }

    public void setLogService(LogService logService) {
        m_logService = logService;
    }

    public LogService getLogService() {
        return m_logService;
    }

    public void setServletContext(ServletContext servletContext) {
        m_servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return m_servletContext;
    }

    protected String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }

    protected int getIntProperty(ServiceReference ref, String key, int defValue) {
        Object value = ref.getProperty(key);
        if (value == null) {
            return defValue;
        }
        try {
            return Integer.parseInt(value.toString());
        }
        catch (Exception e) {
            return defValue;
        }
    }

    protected Dictionary<String, String> getInitParams(ServiceReference serviceReference) {
        Dictionary<String, String> initParams = new Hashtable<String, String>();
        for (String key : serviceReference.getPropertyKeys()) {
            if (key.startsWith(INIT_KEY_PREFIX)) {
                String paramKey = key.substring(INIT_KEY_PREFIX.length());
                String paramValue = getStringProperty(serviceReference, key);
                if (paramValue != null) {
                    initParams.put(paramKey, paramValue);
                }
            }
        }
        return initParams;
    }

    protected HttpContext getHttpContext(String contextId) {
        return m_httpContexts.get(contextId);
    }
}
