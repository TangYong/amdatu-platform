/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.context;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.http.base.internal.util.MimeTypes;
import org.osgi.service.http.HttpContext;

/**
 * Implementation of <code>ExtServletContext</code> that wraps a parent
 * <code>ServletContext</code> and the <code>HttpContext</code> specific
 * to the component wrapped by the handler it is given to. Security,
 * resource and mimetype related calls are delegated to the
 * <code>HttpContext</code> while generic methods are delegated to the
 * parent <code>ServletContext</code>.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class HandlerServletContext implements ExtServletContext {

    private final ServletContext m_servletContext;
    private final HttpContext m_httpContext;

    public HandlerServletContext(ServletContext servletContext, HttpContext httpContext) {
        m_servletContext = servletContext;
        m_httpContext = httpContext;
    }

    public Object getAttribute(String key) {
        return m_servletContext.getAttribute(key);
    }

    @SuppressWarnings("rawtypes")
    public Enumeration getAttributeNames() {
        return m_servletContext.getAttributeNames();
    }

    public ServletContext getContext(String uri) {
        return m_servletContext.getContext(uri);
    }

    public String getContextPath() {
        return m_servletContext.getContextPath();
    }

    public String getInitParameter(String key) {
        return m_servletContext.getInitParameter(key);
    }

    @SuppressWarnings("rawtypes")
    public Enumeration getInitParameterNames() {
        return m_servletContext.getInitParameterNames();
    }

    public int getMajorVersion() {
        return m_servletContext.getMajorVersion();
    }

    public String getMimeType(String file) {
        String type = m_httpContext.getMimeType(file);
        if (type != null) {
            return type;
        }
        return MimeTypes.get().getByFile(file);
    }

    public int getMinorVersion() {
        return m_servletContext.getMinorVersion();
    }

    public RequestDispatcher getNamedDispatcher(String path) {
        return m_servletContext.getNamedDispatcher(path);
    }

    public String getRealPath(String path) {
        return m_servletContext.getRealPath(path);
    }

    public RequestDispatcher getRequestDispatcher(String uri) {
        return m_servletContext.getRequestDispatcher(uri);
    }

    public URL getResource(String path) {
        return m_httpContext.getResource(path);

    }

    public InputStream getResourceAsStream(String path) {
        URL res = getResource(path);
        if (res != null) {
            try {
                return res.openStream();
            }
            catch (IOException e) {

            }
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public Set getResourcePaths(String path) {
        return m_servletContext.getResourcePaths(path);
    }

    public String getServerInfo() {
        return m_servletContext.getServerInfo();
    }

    /**
     * @deprecated See {@link ServletContext#getServlet(String)}
     */
    @Deprecated
    public Servlet getServlet(String name) throws ServletException {
        return m_servletContext.getServlet(name);
    }

    public String getServletContextName() {
        return m_servletContext.getServletContextName();
    }

    /**
     * @deprecated See {@link ServletContext#getServletNames()}
     */
    @Deprecated
    @SuppressWarnings("rawtypes")
    public Enumeration getServletNames() {
        return m_servletContext.getServletNames();
    }

    /**
     * @deprecated See {@link ServletContext#getServlets()}
     */
    @Deprecated
    @SuppressWarnings("rawtypes")
    public Enumeration getServlets() {
        return m_servletContext.getServlets();
    }

    public void log(String message) {
        m_servletContext.log(message);
    }

    public void log(Exception exception, String message) {
        log(message, exception);
    }

    public void log(String message, Throwable cause) {
        m_servletContext.log(message, cause);
    }

    public void removeAttribute(String key) {
        m_servletContext.removeAttribute(key);
    }

    public void setAttribute(String key, Object value) {
        m_servletContext.setAttribute(key, value);
    }

    public boolean handleSecurity(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        throws IOException {
        return m_httpContext.handleSecurity(httpServletRequest, httpServletResponse);
    }
}
