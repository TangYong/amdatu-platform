/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.osgi;

import javax.servlet.Filter;
import javax.servlet.Servlet;

import org.amdatu.web.dispatcher.DispatchExtenderFilter;
import org.amdatu.web.dispatcher.service.DispatcherServiceImpl;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.http.api.ExtHttpService;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

public final class DispatcherServiceActivator extends DependencyActivatorBase {

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        manager.add(
            createComponent()
                .setImplementation(DispatcherServiceImpl.class)
                .add(createServiceDependency().setService(ExtHttpService.class).setRequired(true))
                .add(createServiceDependency().setService(LogService.class).setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(DispatchExtenderFilter.class)
                        .setCallbacks("addExtenderFilter", "removeExtenderFilter")
                        .setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(Filter.class)
                        .setCallbacks("addFilter", "removeFilter")
                        .setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(Servlet.class)
                        .setCallbacks("addServlet", "removeServlet")
                        .setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(HttpContext.class)
                        .setCallbacks("addHttpContext", "removeHttpContext")
                        .setRequired(false))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
