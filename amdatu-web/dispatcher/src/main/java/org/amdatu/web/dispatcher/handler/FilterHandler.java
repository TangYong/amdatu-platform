/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.handler;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class FilterHandler extends AbstractHandler implements Comparable<FilterHandler> {

    private final Filter m_filter;
    private final String m_pattern;
    private final Pattern m_compiledPattern;

    public FilterHandler(int handlerId, String contextId, Filter filter, String pattern, int ranking,
        String tenantPID) {
        super(handlerId, ranking, contextId, tenantPID);
        m_filter = filter;
        if (pattern == null || pattern.equals("")) {
            m_pattern = ".*";
        }
        else {
            m_pattern = pattern;
        }
        m_compiledPattern = Pattern.compile(m_pattern);
    }

    public Filter getFilter() {
        return m_filter;
    }

    public String getPattern() {
        return m_pattern;
    }

    public void doInit() throws ServletException {
        String name = "filter_" + getId();
        FilterConfig config = new FilterConfigImpl(name, getContext(), getInitParams());
        m_filter.init(config);
    }

    public void doDestroy() {
        m_filter.destroy();
    }

    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
        FilterChain filterChain) throws ServletException, IOException {

        String localRequestUri = httpServletRequest.getRequestURI();
        if (!httpServletRequest.getContextPath().equals("")) {
            localRequestUri = localRequestUri.replaceFirst(httpServletRequest.getContextPath(), "");
        }

        if (isActive() && matches(localRequestUri)) {
            if (!getContext().handleSecurity(httpServletRequest, httpServletResponse)) {
                httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
            else {
                m_filter.doFilter(httpServletRequest, httpServletResponse, filterChain);
            }
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    public boolean matches(String uri) {
        if (uri == null) {
            uri = "/";
        }
        return m_compiledPattern.matcher(uri).matches();
    }

    public int compareTo(FilterHandler other) {
        // service.id rules when service.ranking equals
        if (m_ranking == other.m_ranking) {
            return m_handlerId - other.m_handlerId;
        }
        return other.m_ranking - m_ranking;
    }
}
