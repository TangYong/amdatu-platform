/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.dispatcher.service.DispatcherServiceImpl;
import org.osgi.service.log.LogService;

/**
 * Simple intercept filter that collaborates with <code>DispatcherService</code>
 * to redirect request to a custom (mostly tenant specific) pipeline. When there
 * is no custom pipeline or no target matches the request is continued down the
 * original chain.
 */
public final class DispatchInterceptFilter implements Filter {

    private DispatcherServiceImpl m_dispatcherService;
    private FilterConfig m_filterConfig;

    public void setDispatcherService(DispatcherServiceImpl dispatcherService) {
        m_dispatcherService = dispatcherService;
    }

    public ServletContext getServletContext() {
        return m_filterConfig.getServletContext();
    }

    public void init(FilterConfig filterConfig) throws ServletException {
        m_filterConfig = filterConfig;
    }

    public void destroy() {
        m_filterConfig = null;
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException,
        ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        try {
            m_dispatcherService.dispatchRequest(httpServletRequest, httpServletResponse, filterChain);
        }
        catch (IOException e) {
            m_dispatcherService.log(LogService.LOG_ERROR, "DispatcherService threw an IOException", e);
            httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        catch (ServletException e) {
            m_dispatcherService.log(LogService.LOG_ERROR, "DispatcherService threw a ServletException", e);
            httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
