/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.dispatch;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Simple utility <code>FilterChain</code> baseclass that downcasts the inteface method parameters
 * to the http specific classes.
 */
public abstract class HttpFilterChain implements FilterChain {

    public final void doFilter(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException,
        ServletException {
        doFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse);
    }

    protected abstract void doFilter(HttpServletRequest req, HttpServletResponse res) throws IOException,
        ServletException;
}
