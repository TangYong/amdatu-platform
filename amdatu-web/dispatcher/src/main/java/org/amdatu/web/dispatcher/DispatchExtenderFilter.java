/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher;

import javax.servlet.Filter;

/**
 * Service registration interface for <code>Filter</code> components that
 * are intended to extend the functionality of the <code>DispatcherService
 * </code> handling cross-cutting concerns such as Tenant resolving.
 * <p/>
 * Extender filters are handled just like any <code>Filter</code> within
 * a <code>FilterChain</code> but are not considered to be tenant aware
 * and are by definition invoked in an extender chain before any regular
 * filters and servlets are considered. Inside the extender chain ordering
 * is based on standard service ranking.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface DispatchExtenderFilter extends Filter {
}
