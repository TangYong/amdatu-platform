/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.dispatcher.dispatch;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.web.dispatcher.handler.FilterHandler;
import org.amdatu.web.dispatcher.handler.FilterHandlerRegistry;
import org.amdatu.web.dispatcher.handler.ServletHandler;
import org.amdatu.web.dispatcher.handler.ServletHandlerRegistry;

/**
 * Pipeline implementation responsible for building the application filterchain in collaboration
 * with the specified handler registries.
 */
public final class CustomFilterPipeline {

    private final FilterHandlerRegistry m_filterHandlerRegistry;
    private final ServletHandlerRegistry m_servletHandlerRegistry;

    public CustomFilterPipeline(FilterHandlerRegistry filterHandlerRegistry,
        ServletHandlerRegistry servletHandlerRegistry) {
        m_filterHandlerRegistry = filterHandlerRegistry;
        m_servletHandlerRegistry = servletHandlerRegistry;
    }

    public void dispatch(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
        FilterChain proceedingChain) throws ServletException, IOException {

        ServletHandler servletHandler = m_servletHandlerRegistry.getServletHandler(httpServletRequest);
        if (servletHandler == null) {
            proceedingChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        FilterHandler[] filterHandlers = m_filterHandlerRegistry.getFilterHandlers(httpServletRequest);
        CustomFilterChain chain = new CustomFilterChain(filterHandlers, servletHandler, proceedingChain);
        chain
            .doFilter(new DispatcherRequestWrapper(this, httpServletRequest, servletHandler.getAlias()),
                httpServletResponse);
    }

    ServletHandlerRegistry getServletHandlerRegistry() {
        return m_servletHandlerRegistry;
    }

    interface RegistryAccess {
        ServletHandlerRegistry getServletHandlerRegistry();
    }

    /**
     * Application <code>RequestDispatcher</code> implementation that invokes the specified
     * <code>ServletHandler</code> and uses a <code>DispatcherForwardRequestWrapper</code>
     * to ensure that subsequent calls to <code>getServletPath()</code> and <code>
     * getPathInfo()</code> correspond to the target <code>ServletHandler</code> and
     * the path the dispatcher was requested for.
     */
    static final class DispatcherRequestDispatcher implements RequestDispatcher {

        private final ServletHandler m_servletHandler;
        private final String m_path;

        public DispatcherRequestDispatcher(ServletHandler servletHandler, String path) {
            m_servletHandler = servletHandler;
            m_path = path;
        }

        public void forward(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException,
            IOException {
            m_servletHandler.handle(new DispatcherForwardRequestWrapper((HttpServletRequest) servletRequest,
                m_servletHandler.getAlias(), m_path), (HttpServletResponse) servletResponse);
        }

        public void include(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException,
            IOException {
            m_servletHandler.handle(new DispatcherForwardRequestWrapper((HttpServletRequest) servletRequest,
                m_servletHandler.getAlias(), m_path), (HttpServletResponse) servletResponse);
        }
    }

    /**
     * Application <code>HttpServletRequestWrapper</code> for incomming requests. It wraps the original request
     * so that subsequent calls to <code>getServletPath()</code> and <code>getPathInfo()</code>
     * correspond to the target <code>ServletHandler</code>.
     */
    static final class DispatcherRequestWrapper extends HttpServletRequestWrapper {

        private final CustomFilterPipeline m_customFilterPipeline;
        private final String m_pathInfo;
        private final String m_servletPath;

        public DispatcherRequestWrapper(CustomFilterPipeline customFilterPipeline,
            HttpServletRequest httpServletRequest, String servletAlias) {
            super(httpServletRequest);
            m_customFilterPipeline = customFilterPipeline;
            m_servletPath = servletAlias;
            m_pathInfo = httpServletRequest.getPathInfo().replaceFirst(servletAlias, "");
        }

        @Override
        public String getPathInfo() {
            return m_pathInfo;
        }

        @Override
        public String getServletPath() {
            return m_servletPath;
        }

        @Override
        public RequestDispatcher getRequestDispatcher(String path) {
            ServletHandler servletHandler =
                m_customFilterPipeline.getServletHandlerRegistry().getServletHandler(this, path);
            if (servletHandler != null) {
                return new DispatcherRequestDispatcher(servletHandler, path);
            }
            return null;
        }
    }

    /**
     * Application <code>HttpServletRequestWrapper</code> for forward requests. It wraps the original request
     * so that subsequent calls to <code>getServletPath()</code> and <code>getPathInfo()</code>
     * correspond to the new target <code>ServletHandler</code>.
     */
    static final class DispatcherForwardRequestWrapper extends HttpServletRequestWrapper {

        private final String m_servletPath;
        private final String m_path;

        public DispatcherForwardRequestWrapper(HttpServletRequest httpServletRequest, String servletAlias,
            String path) {
            super(httpServletRequest);
            m_servletPath = servletAlias;
            m_path = path;
        }

        @Override
        public String getPathInfo() {
            return m_path.substring(m_servletPath.length());
        }

        @Override
        public String getServletPath() {
            return m_servletPath;
        }

        @Override
        public String getRequestURI() {
            return getContextPath() + m_path;
        }

        @Override
        public StringBuffer getRequestURL() {
            String url = super.getRequestURL().toString();
            url = url.replace(super.getRequestURI(), getRequestURI());
            return new StringBuffer(url);
        }
    }
}
