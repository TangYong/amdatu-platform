/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.jsp.service;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.jasper.Constants;
import org.apache.jasper.servlet.JspServlet;
import org.ops4j.pax.swissbox.core.ContextClassLoaderUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * This class implements a servlet that facilitates rendering and compiling JSPs for a particular resource provider.
 * It extends the Jasper implementation of the JSPServlet but uses OPS4J Swissbox to fix some classloading issues with
 * this servlet.
 * This class was copied and modified from org.ops4j.pax.web.jsp.JspServletWrapper.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class ResourceProviderJspServlet implements Servlet {

    // Injected by DM
    private volatile LogService m_logService;
    private volatile BundleContext m_bundleContext;

    private JasperClassLoader m_jasperClassLoader;
    private JspServlet m_jasperServlet;

    private final Bundle m_bundle;
    private final long m_providerId;
    private File m_jspTmpDir;

    // Constructor
    public ResourceProviderJspServlet(long providerId, Bundle bundle) {
        m_bundle = bundle;
        m_providerId = providerId;
    }

    /**
     * DependencyManager callback.
     */
    public void dmInit() throws Exception {
        createJspTmpDir();
    }

    /**
     * DependencyManager callback.
     */
    public void dmDestroy() throws Exception {
        deleteJspTmpDir();
    }

    /**
     * Delegates to jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#init(ServletConfig)
     */
    public void init(final ServletConfig config) throws ServletException {
        m_jasperClassLoader = new JasperClassLoader(m_bundle, JasperClassLoader.class.getClassLoader());
        m_jasperClassLoader.addClassPathJars(m_bundleContext.getBundle());
        m_jasperServlet = new JspServlet();

        // tell catalina about our jspTmpDir
        config.getServletContext().setAttribute(Constants.TMP_DIR, m_jspTmpDir);
        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.init(config);
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "Could not initialize Jsp Servlet", e);
            throw new ServletException(e);
        }
    }

    /**
     * Delegates to jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#destroy()
     */
    public void destroy() {
        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.destroy();
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "Could not destroy JspServletServiceImpl", e);
        }
        m_jasperClassLoader = null;
        m_jasperServlet = null;
    }

    /**
     * Delegates to jasper servlet.
     * 
     * @see ResourceProviderJspServlet#getServletConfig()
     */
    public ServletConfig getServletConfig() {
        return m_jasperServlet.getServletConfig();
    }

    /**
     * Delegates to Jasper servlet with a controlled context class loader.
     * 
     * @see ResourceProviderJspServlet#service(ServletRequest, ServletResponse)
     */
    public void service(final ServletRequest req, final ServletResponse res) throws ServletException, IOException {

        final HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        String target = httpServletRequest.getRequestURI();
        if (!httpServletRequest.getContextPath().equals("")) {
            target = target.replaceFirst(httpServletRequest.getContextPath(), "");
        }
        req.setAttribute(Constants.JSP_FILE, target);

        try {
            ContextClassLoaderUtils.doWithClassLoader(
                m_jasperClassLoader,
                new Callable<Void>() {
                    public Void call() throws Exception {
                        m_jasperServlet.service(req, res);
                        return null;
                    }
                }
                );
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_ERROR, "An error occurred while handling JSP request", e);
            throw new ServletException(e);
        }
    }

    /**
     * Delegates to jasper servlet.
     * 
     * @see ResourceProviderJspServlet#getServletInfo()
     */
    public String getServletInfo() {
        return m_jasperServlet.getServletInfo();
    }

    private void createJspTmpDir() throws Exception {
        m_jspTmpDir = new File(m_bundleContext.getDataFile(""), "jspTmpDir" + File.separator + m_providerId);
        m_jspTmpDir.mkdirs();
        if (!m_jspTmpDir.exists()) {
            throw new Exception("Failed to created JSP tmpdir " + m_jspTmpDir);
        }
    }

    private void deleteJspTmpDir() throws IOException {
        try {
            deleteDirectory(m_jspTmpDir);
        }
        catch (IOException e) {
            // be nice.. try again
            deleteDirectory(m_jspTmpDir);
        }
    }

    private void deleteDirectory(File directory) throws IOException {
        if (!directory.exists() || !directory.isDirectory()) {
            return;
        }
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                }
                else {
                    file.delete();
                }
            }
        }
        directory.delete();
    }
}
