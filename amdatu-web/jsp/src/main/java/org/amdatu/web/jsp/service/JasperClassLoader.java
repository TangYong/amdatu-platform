/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.jsp.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.ops4j.pax.swissbox.core.BundleClassLoader;
import org.osgi.framework.Bundle;

/**
 * The Jasper classloader is used for the compiled Java classes generated from the JSPs.
 * The classloader creates a BundleClassLoader for the bundle that holds the JSP and sets the
 * parent classloader to the classloader of the Jasper classes itself (which is the classloader of this bundle).<br />
 * <br />
 * <b>See also:</b> <a href="http://www.ops4j.org/projects/pax/web/pax-web-jsp/apidocs/org/ops4j/pax/web/jsp/internal/JasperClassLoader.html">org.ops4j.pax.web.jsp.internal.JasperClassLoader</a>.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class JasperClassLoader extends URLClassLoader {

    /**
     * Internal bundle class loader.
     */
    private final BundleClassLoader m_bundleClassLoader;

    public JasperClassLoader(final Bundle bundle, final ClassLoader parent) {
        super(getClassPathJars(bundle));
        m_bundleClassLoader = new BundleClassLoader(bundle, parent);
    }

    /**
     * Delegate to bundle class loader.
     * 
     * @see BundleClassLoader#getResource(String)
     */
    @Override
    public URL getResource(String name) {
        return m_bundleClassLoader.getResource(name);
    }

    /**
     * Delegate to bundle class loader.
     * 
     * @see BundleClassLoader#getResources(String)
     */
    @Override
    public Enumeration<URL> getResources(String name)
        throws IOException {
        return m_bundleClassLoader.getResources(name);
    }

    /**
     * Delegate to bundle class loader.
     * 
     * @see BundleClassLoader#loadClass(String)
     */
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Class loadClass(final String name)
        throws ClassNotFoundException {
        return m_bundleClassLoader.loadClass(name);
    }

    @Override
    public String toString() {
        return new StringBuffer()
            .append(this.getClass().getSimpleName())
            .append("{")
            .append("bundleClassLoader=").append(m_bundleClassLoader)
            .append("}")
            .toString();
    }

    /**
     * Returns a list of urls to jars that composes the Bundle-ClassPath.
     * 
     * @param bundle the bundle from which the class path should be taken
     * 
     * @return list or urls to jars that composes the Bundle-ClassPath.
     */
    private static URL[] getClassPathJars(final Bundle bundle) {
        final List<URL> urls = new ArrayList<URL>();
        final String bundleClasspath = (String) bundle.getHeaders().get("Bundle-ClassPath");
        if (bundleClasspath != null) {
            String[] segments = bundleClasspath.split(",");
            for (String segment : segments) {
                final URL url = bundle.getEntry(segment);
                if (url != null) {
                    String extUrl = url.toExternalForm();
                    if (extUrl.endsWith("jar")) {
                        try {
                            URL jarUrl = new URL("jar:" + extUrl + "!/");
                            urls.add(jarUrl);
                        }
                        catch (MalformedURLException ignore) {

                        }
                    }
                }
            }
        }

        return urls.toArray(new URL[urls.size()]);
    }

    /**
     * Adds the jars on classpath of the specified bundle to this classloader. This used to add
     * any TLDs contained by this bundle but not by the bundle that contains the JSP
     * 
     * @param bundle Bundle to add classpath jars for
     */
    public void addClassPathJars(Bundle bundle) {
        URL[] urls = getClassPathJars(bundle);
        for (URL url : urls) {
            addURL(url);
        }
    }
}
