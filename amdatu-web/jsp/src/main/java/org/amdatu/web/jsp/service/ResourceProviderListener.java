/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.jsp.service;

import static org.amdatu.tenant.Constants.PID_KEY;
import static org.amdatu.web.dispatcher.Constants.ALIAS_KEY;
import static org.amdatu.web.dispatcher.Constants.CONTEXT_ID_KEY;
import static org.amdatu.web.jsp.Constants.JSP_ALIAS_KEY;
import static org.osgi.framework.Constants.SERVICE_ID;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Servlet;

import org.amdatu.web.httpcontext.ResourceProvider;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * This class is responsible for registration of JSP Servlets for each ResourceProvider that
 * comes available.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ResourceProviderListener {

    private final ConcurrentHashMap<ServiceReference, Component> m_components =
        new ConcurrentHashMap<ServiceReference, Component>();

    private volatile DependencyManager m_dependencyManager;
    private volatile LogService m_logService;

    public void stop() {
        for (Component component : m_components.values()) {
            m_dependencyManager.remove(component);
        }
        m_components.clear();
    }

    public void resourceProviderAdded(ServiceReference serviceReference) {

        final long serviceId = getLongProperty(serviceReference, SERVICE_ID);

        final String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId == null || "".equals(contextId)) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") does not specify a contextId. Ignoring..");
            return;
        }

        final String jspAlias = getStringProperty(serviceReference, JSP_ALIAS_KEY);
        if (jspAlias == null || "".equals(jspAlias)) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") does not specify a jspAlias. Ignoring..");
            return;
        }

        final Bundle jspBundle = serviceReference.getBundle();

        if (jspAlias == null || "".equals(jspAlias)) {
            m_logService.log(LogService.LOG_DEBUG, "ResourceProvider(" + serviceId
                + ") does not specify a jspalias. Ignoring..");
            return;
        }

        final Component jspServletComponent = createJspServletComponent(serviceReference, jspBundle);
        if (m_components.putIfAbsent(serviceReference, jspServletComponent) != null) {
            m_logService.log(LogService.LOG_WARNING, "ResourceProvider(" + serviceId
                + ") specifies already registered jspalias. Ignoring..");
            return;
        }
        m_dependencyManager.add(jspServletComponent);
        m_logService.log(LogService.LOG_DEBUG, "Registered JSP servlet at " + jspAlias + " for ResourceProvider("
            + serviceId + ").");
    }

    public void resourceProviderRemoved(ServiceReference serviceReference, ResourceProvider provider) {

        final long serviceId = getLongProperty(serviceReference, SERVICE_ID);
        final String jspAlias = getStringProperty(serviceReference, JSP_ALIAS_KEY);

        final Component jspServletComponent = m_components.remove(serviceReference);
        if (jspServletComponent == null) {
            m_logService.log(LogService.LOG_DEBUG, "No JSP servlet registered for ResourceProvider(" + serviceId
                + "). Ignoring..");
            return;
        }
        m_dependencyManager.remove(jspServletComponent);
        m_logService.log(LogService.LOG_DEBUG, "Removed JSP servlet at " + jspAlias + " for ResourceProvider("
            + serviceId + ").");
    }

    private Component createJspServletComponent(final ServiceReference serviceReference, final Bundle jspBundle) {

        final Long serviceID = (Long) serviceReference.getProperty(Constants.SERVICE_ID);

        Dictionary<String, Object> properties = new Hashtable<String, Object>();

        final String jspAlias = getStringProperty(serviceReference, JSP_ALIAS_KEY);
        properties.put(ALIAS_KEY, jspAlias);

        final String contextId = getStringProperty(serviceReference, CONTEXT_ID_KEY);
        if (contextId != null && !"".equals(contextId)) {
            properties.put(CONTEXT_ID_KEY, contextId);
        }

        // AMDATU-479 - this is tricky
        final String tenantPID = getStringProperty(serviceReference, PID_KEY);
        if (tenantPID != null) {
            properties.put(PID_KEY, tenantPID);
        }

        ResourceProviderJspServlet jspServlet = new ResourceProviderJspServlet(serviceID, jspBundle);
        Component component = m_dependencyManager.createComponent()
            .setInterface(Servlet.class.getName(), properties)
            .setImplementation(jspServlet)
            .add(m_dependencyManager.createServiceDependency()
                .setService(LogService.class)
                .setRequired(false))
            .setCallbacks("dmInit", "dmStart", "dmStop", "dmDestroy");
        return component;
    }

    private Long getLongProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof Long) ? (Long) value : -1;
    }

    private String getStringProperty(ServiceReference ref, String key) {
        Object value = ref.getProperty(key);
        return (value instanceof String) ? (String) value : null;
    }
}
