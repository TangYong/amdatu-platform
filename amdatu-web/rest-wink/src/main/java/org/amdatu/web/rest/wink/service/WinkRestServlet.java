/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.rest.wink.service;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.wink.server.internal.RequestProcessor;
import org.apache.wink.server.internal.servlet.RestServlet;
import org.apache.wink.server.utils.RegistrationUtils;

/**
 * Extends the default Apache Wink <code>RestServlet</code> to handle multiple
 * processors and wrap incomming requests to match resources.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class WinkRestServlet extends RestServlet {

    private static final long serialVersionUID = -4472025056797521781L;

    private final String m_resourceName;

    // Indicates if this REST servlet has been initialized
    boolean m_initialized = false;

    // The servlet URL
    private String m_requestProcessorId;
    private Object m_service;

    public WinkRestServlet(String resourcePath, Object service, String tenantPID) {
        super();
        m_resourceName = resourcePath;
        m_requestProcessorId = resourcePath + "_" + tenantPID;
        m_service = service;
    }

    @Override
    protected void service(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        throws ServletException, IOException {
        // The next line seems to do nothing, but it does. Without requesting parameter names here, using both
        // @FormParam JAX-RS annotations in a REST resource as well as getParameterNames() in the same @PUT
        // or @POST annotated method will not work (getParameterNames() will return an empty map without the
        // posted parameters). This is because the InputStream in that case is read by the FormParam handler
        // first, which does not write the read from parameters to the parameter map. The other way around
        // works fine; when the input stream has already been read and written to the parameter map the
        // FormParam handler binds the values from this map instead of reading them from the input stream.
        httpServletRequest.getParameterNames();
        super.service(new WinkResourceRequestWrapper(httpServletRequest, m_resourceName), httpServletResponse);
    }

    @Override
    protected RequestProcessor getRequestProcessor() {
        if (!m_initialized) {
            // Override this method to enforce creation of a new requestprocessor whenever a servlet is created
            m_initialized = true;
            return null;
        }
        else {
            return RequestProcessor.getRequestProcessor(getServletContext(), m_requestProcessorId);
        }
    }

    @Override
    protected void storeRequestProcessorOnServletContext(RequestProcessor requestProcessor) {
        requestProcessor.storeRequestProcessorOnServletContext(getServletContext(), m_requestProcessorId);
    }

    @Override
    public void init() throws ServletException {
        // Perform the init with the bundle classloader set as context classloader on
        // the current thread, as the ClassUtils.loadClass method of Wink will try
        // to load classes from this classloader.
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
            super.init();
        }
        finally {
            Thread.currentThread().setContextClassLoader(cl);
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        RegistrationUtils.registerInstances(getServletContext(), m_requestProcessorId, m_service);
    }

    private static final class WinkResourceRequestWrapper extends HttpServletRequestWrapper {

        private final String m_resourcePath;

        public WinkResourceRequestWrapper(HttpServletRequest req, String resourcePath) {
            super(req);
            if (!resourcePath.startsWith("/")) {
                resourcePath = "/" + resourcePath;
            }
            m_resourcePath = resourcePath;
        }

        @Override
        public String getPathInfo() {
            return m_resourcePath + super.getPathInfo();
        }

        @Override
        public String getRequestURI() {
            return getContextPath() + getServletPath() + getPathInfo();
        }
    }
}
