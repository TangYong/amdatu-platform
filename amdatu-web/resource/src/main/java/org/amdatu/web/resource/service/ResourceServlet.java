/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.resource.service;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class ResourceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final int INTERNAL_BUFFER_SIZE = 4096;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String path = getResourcePath(request);
        URL url = getServletContext().getResource(path);
        if (url == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        else {
            String contentType = getServletContext().getMimeType(path);
            if (contentType != null) {
                response.setContentType(contentType);
            }
            URLConnection conn = null;
            OutputStream os = null;
            InputStream is = null;
            try {
                conn = url.openConnection();

                // convert millisecond precision of getLastModified() to second precision used
                // in HTTP headers before comparing
                long lastModified = (conn.getLastModified() / 1000) * 1000;
                if (lastModified != 0) {
                    response.setDateHeader("Last-Modified", lastModified);
                }

                // send the whole response, unless there was an "if modified since" header and the
                // resource is indeed not modified since that date
                long ifModifiedSince = request.getDateHeader("If-Modified-Since");
                if ((lastModified == 0 || ifModifiedSince == -1 || lastModified > ifModifiedSince)) {
                    try {
                        is = conn.getInputStream();
                        os = response.getOutputStream();
                        copy(is, os);
                    }
                    finally {
                        safeClose(is);
                        safeClose(os);
                    }
                }
                else {
                    response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                }
            }
            finally {
                if (conn != null && is == null) {
                    // AMDATU-432 if the connection is open (conn != null), but nothing
                    // has been read (is == null), and the URL points to a local file,
                    // it stays open, so explicitly close it.
                    safeClose(conn.getInputStream());
                }
            }
        }
    }

    /**
     * Returns the path to the resource based on the request. This is the
     * path part of the request URI minus the servlet context path.
     */
    private String getResourcePath(HttpServletRequest request) {
        String path = request.getRequestURI();
        String contextPath = request.getContextPath();
        if (path == null) {
            path = "";
        }
        if (!path.startsWith("/")) {
            path += "/" + path;
        }
        int contextPathLength = contextPath.length();
        if (contextPathLength > 0) {
            path = path.substring(contextPathLength);
        }
        return path;
    }

    /**
     * Closes a given {@link Closeable} implementation in "safe" manner, meaning that it can
     * handle null-values and ignores any I/O exceptions that occur during the close.
     * 
     * @param closeable the {@link Closeable} implementation to close, can be <code>null</code>
     *        in which case this method does nothing.
     */
    private static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            }
            catch (IOException ignore) {
                // Not relevant; we're trashing the implementation anyway...
            }
        }
    }

    /**
     * Copy data from InputStream to OutputStream.
     */
    private static void copy(InputStream is, OutputStream os) throws IOException {
        byte[] buf = new byte[INTERNAL_BUFFER_SIZE];
        int n;

        while ((n = is.read(buf, 0, buf.length)) >= 0) {
            os.write(buf, 0, n);
        }
    }
}
