/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.tenantresolver.parameter.service;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.amdatu.tenant.Tenant;
import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.dispatcher.DispatchExtenderFilter;
import org.osgi.service.log.LogService;

/**
 * Amdatu Web {@link DispatchExtenderFilter} that resolves the {@link Tenant} by
 * comparing the request parameter {@link #TENANT_REQUEST_PARAMETER} against the
 * id property of available tenants.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public final class ParameterTenantResolverExtenderFilter implements DispatchExtenderFilter {

    public static final String TENANT_REQUEST_PARAMETER = "tenant";

    private final TenantRegistry m_registry;

    private volatile LogService m_logService;

    public ParameterTenantResolverExtenderFilter(TenantRegistry registry) {
        m_registry = registry;
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String tenantPID = httpServletRequest.getParameter(TENANT_REQUEST_PARAMETER);

        if (tenantPID != null) {
            Tenant tenant = m_registry.getTenant(tenantPID);
            if (tenant != null) {
                servletRequest.setAttribute(Constants.TENANT_REQUESTCONTEXT_KEY, tenant);
                servletRequest.setAttribute(Constants.TENANTPID_REQUESTCONTEXT_KEY, tenant.getPID());
            }
            else {
                // No INF/WARN/ERR cause there may be multiple resolvers in play
                m_logService.log(LogService.LOG_DEBUG,
                    "Failed to resolved request to available tenant for request value: " + tenantPID);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
