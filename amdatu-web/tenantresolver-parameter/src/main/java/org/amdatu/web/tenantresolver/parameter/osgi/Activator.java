/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.tenantresolver.parameter.osgi;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.tenant.Tenant;
import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.dispatcher.DispatchExtenderFilter;
import org.amdatu.web.tenantresolver.parameter.service.ParameterTenantResolverExtenderFilter;
import org.amdatu.web.tenantresolver.parameter.service.TenantRegistry;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

/**
 * BundleActivator for the {@link ParameterTenantResolverExtenderFilter} component.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
public final class Activator extends DependencyActivatorBase {

    private final TenantRegistry m_registry = new TenantRegistry();
    private final ParameterTenantResolverExtenderFilter m_filter =
        new ParameterTenantResolverExtenderFilter(m_registry);

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(org.osgi.framework.Constants.SERVICE_RANKING, 0);
        properties.put(Constants.PATTERN_KEY, ".*");

        manager.add(
            createComponent()
                .setInterface(DispatchExtenderFilter.class.getName(), properties)
                .setImplementation(m_filter)
                .setComposition(this, "getComposition")
                .add(createServiceDependency()
                    .setService(Tenant.class)
                    .setRequired(false)
                    .setCallbacks("addTenant", "removeTenant"))
                .add(createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

    public Object[] getComposition() {
        return new Object[] {
            m_filter, m_registry
        };
    }
}
