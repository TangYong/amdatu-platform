/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext.osgi;

import org.amdatu.web.httpcontext.MimeTypeResolver;
import org.amdatu.web.httpcontext.ResourceProvider;
import org.amdatu.web.httpcontext.SecurityHandler;
import org.amdatu.web.httpcontext.service.HttpContextManagerServiceImpl;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * This is the OSGi activator for this http context bundle.
 */
public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {

        manager.add(
            createComponent()
                .setImplementation(HttpContextManagerServiceImpl.class)
                .add(
                    createServiceDependency()
                        .setService(ResourceProvider.class)
                        .setCallbacks("addResourceProvider", "removeResourceProvider")
                        .setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(SecurityHandler.class)
                        .setCallbacks("addSecurityHandler", "removeSecurityHandler")
                        .setRequired(false))
                .add(
                    createServiceDependency()
                        .setService(MimeTypeResolver.class)
                        .setCallbacks("addMimeTypeResolver", "removeMimeTypeResolver")
                        .setRequired(false))
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
