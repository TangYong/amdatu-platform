/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.httpcontext;

/**
 * Service interface for a delegate to a named <code>DelegatingHttpContext</code>.
 * The service must be registered with the relevant contextId property.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface MimeTypeResolver {

    /**
     * Maps a name to a MIME type.
     * 
     * @param name
     *        the MIME type for this name.
     * @return MIME type (e.g. text/html) of the name or null to indicate that
     *         the caller should determine the MIME type itself.
     */
    String getMimetype(String name);
}
