------------------------
SVN:IGNORE readme
------------------------

The .svnignore file in this directory a template for settings the svn:ignore property on directories in the Amdatu
svn repository. More info: http://svnbook.red-bean.com/en/1.1/ch07s02.html


usage: svn propset svn:ignore -F etc\svnignore\.svnignore <directory>