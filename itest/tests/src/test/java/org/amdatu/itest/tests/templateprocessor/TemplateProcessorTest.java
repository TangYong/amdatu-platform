/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.templateprocessor;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;

import javax.inject.Inject;

import junit.framework.Assert;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.itest.tests.mock.VelocityBean;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateProcessor;
import org.amdatu.template.processor.velocity.VelocityTemplateEngine;
import org.amdatu.tenant.Constants;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class TemplateProcessorTest {

    private static final String[] EXPECTED_OUTPUT =
        new String[] { "Bla bla lba beanValue", "", "${", "}", "${}", "A", "A",
            "${a.b}Bla bla", "blaa${a.b.c}Blabla", "${a.b.c}Ab ", "${c} ", " {a}", "zb", "velocity", "z" };;

    private static final String LINE_DELIMITER = System.getProperty("line.separator");

    private static String INPUT_FILE_NAME;
    private static File INPUT_FILE;
    private static URL INPUT_URL;
    private static Reader INPUT_READER;
    private static String INPUT_STRING;

    @Inject
    private BundleContext m_bundleContext;
    private TestContext m_testContext;

    //

    @Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            CoreBundles.provisionAll(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.shell").version("1.4.2"),
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.shell.tui").version("1.4.1"),
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.log").version("1.0.1"),
            
            mavenBundle().groupId("org.amdatu.template").artifactId("org.amdatu.template.processor")
                .versionAsInProject());
    }

    @BeforeClass
    public static void setUpOnce() throws Exception {
        INPUT_FILE_NAME = "src/test/resources/template.txt";
        INPUT_FILE = new File(INPUT_FILE_NAME);
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        CoreConfigs.provisionAll(m_testContext);

        INPUT_READER = new FileReader(INPUT_FILE);
        INPUT_URL = INPUT_FILE.toURI().toURL();
        INPUT_STRING = readInputFile();
    }

    @After
    public void tearDown() throws Exception {
        INPUT_READER.close();

        m_testContext.tearDown();
    }

    /**
     * Test the Velocity implementation
     */
    @Test
    public void testVelocity() throws Exception {
        LogService logService = m_testContext.getService(LogService.class);
        assertThat("Expected a LogService to be available", logService, is(notNullValue()));

        TemplateEngine eng = getEngine();
        Assert.assertNotNull(eng);
    }

    @Test
    public void testGenerateString() throws Exception {
        TemplateEngine eng = getEngine();
        TemplateContext context = fillContext(eng.createContext());
        TemplateProcessor proc;
        String output;

        proc = eng.createProcessor(INPUT_FILE);
        output = proc.generateString(context);
        validate(output);

        proc = eng.createProcessor(INPUT_STRING);
        output = proc.generateString(context);
        validate(output);

        proc = eng.createProcessor(INPUT_READER);
        output = proc.generateString(context);
        validate(output);

        proc = eng.createProcessor(INPUT_URL);
        output = proc.generateString(context);
        validate(output);
    }

    @Test
    public void testGenerateFile() throws Exception {
        TemplateEngine eng = getEngine();
        TemplateContext context = fillContext(eng.createContext());
        TemplateProcessor proc;
        File outputFile = File.createTempFile("tet", ".txt");

        proc = eng.createProcessor(INPUT_FILE);
        proc.generateFile(context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_STRING);
        proc.generateFile(context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_READER);
        proc.generateFile(context, outputFile);
        validate(outputFile);

        proc = eng.createProcessor(INPUT_URL);
        proc.generateFile(context, outputFile);
        validate(outputFile);
    }

    @Test
    public void testGenerateURL() throws Exception {
        TemplateEngine eng = getEngine();
        TemplateContext context = fillContext(eng.createContext());
        TemplateProcessor proc;
        URL outputURL;

        proc = eng.createProcessor(INPUT_FILE);
        outputURL = proc.generateURL(context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_STRING);
        outputURL = proc.generateURL(context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_READER);
        outputURL = proc.generateURL(context);
        validate(outputURL);

        proc = eng.createProcessor(INPUT_URL);
        outputURL = proc.generateURL(context);
        validate(outputURL);
    }

    @Test
    public void testGenerateStream() throws Exception {
        TemplateEngine eng = getEngine();
        TemplateContext context = fillContext(eng.createContext());
        TemplateProcessor proc;
        File outputFile = File.createTempFile("tet", ".txt");
        Writer writer;

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_FILE);
        proc.generateStream(context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_STRING);
        proc.generateStream(context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_READER);
        proc.generateStream(context, writer);
        validate(outputFile);

        writer = new FileWriter(outputFile);
        proc = eng.createProcessor(INPUT_URL);
        proc.generateStream(context, writer);
        validate(outputFile);
    }

    //

    private TemplateEngine getEngine() throws Exception {
        return m_testContext.getService(TemplateEngine.class, "(" + TemplateEngine.KEY_TEMPLATE_ENGINE_TYPE + "="
            + VelocityTemplateEngine.ENGINE_TYPE + ")");
    }

    private void validate(String output) {
        String[] outputArray = output.split(LINE_DELIMITER);
        assertArrayEquals(EXPECTED_OUTPUT, outputArray);
    }

    private void validate(File outputFile) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(outputFile));
        validate(read(br));
    }

    private void validate(URL outputURL) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader((InputStream) outputURL.getContent()));
        validate(read(br));
    }

    private String readInputFile() throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
        return read(br);
    }

    private String read(BufferedReader br) throws Exception {
        String line = br.readLine();
        String result = line;
        while ((line = br.readLine()) != null) {
            result += LINE_DELIMITER;
            result += line;
        }
        br.close();
        return result;
    }

    private TemplateContext fillContext(TemplateContext context) {
        context.put("a", "A");
        context.put("a.b", "AB");
        context.put("a.b.c", "ABC");
        context.put("newline", "\n");
        context.put("bean", new VelocityBean());
        return context;
    }

}
