/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.useradmin;

import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import javax.inject.Inject;

import junit.framework.Assert;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.tenant.Constants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.useradmin.Role;
import org.osgi.service.useradmin.UserAdmin;

/**
 * Benchmark for useradmin. Tests whether it is possible to create a "large" number of roles.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class UseradminBenchmarkTest {

    private static final int USER_COUNT = 100;
    private static final int GROUP_COUNT = 100;

    @Inject
    private BundleContext m_bundleContext;
    private TestContext m_testContext;

    @org.ops4j.pax.exam.junit.Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAll());
    }

    /**
     * Sets up an individual test case.
     * 
     * @throws Exception not part of this set up.
     */
    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");

        CoreConfigs.provisionAll(m_testContext);
        m_testContext.waitForSystemToSettle();
    }

    /**
     * Tears down the test case.
     */
    @After
    public void tearDown() {
        m_testContext.tearDown();
    }

    /**
     * Very basic create & drop test
     * 
     * @throws Exception not part of this test case.
     */
    @Test
    public void testBenchmark() throws Exception {

        UserAdmin useradmin = m_testContext.getService(UserAdmin.class);

        for (int i = 0; i < USER_COUNT; i++) {
            useradmin.createRole("user-" + i, Role.USER);
        }

        for (int i = 0; i < GROUP_COUNT; i++) {
            useradmin.createRole("group-" + i, Role.GROUP);
        }
        Role[] roles = useradmin.getRoles(null);
        Assert.assertEquals(USER_COUNT + GROUP_COUNT, roles.length);

        for (Role role : roles) {
            useradmin.removeRole(role.getName());
        }

        Assert.assertEquals(null, useradmin.getRoles(null));
    }
}
