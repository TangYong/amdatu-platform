/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.*;

import java.util.Properties;

import javax.inject.Inject;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.Fixture;
import org.amdatu.itest.base.TestContext;
import org.amdatu.itest.tests.mock.TestService;
import org.amdatu.itest.tests.mock.TestServiceImpl;
import org.amdatu.tenant.Constants;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.ServiceDependency;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;
import org.osgi.service.useradmin.UserAdmin;

/**
 * Integration test covering basic availability of core services in a default
 * configuration provided by {@link Fixture}.
 * <p/>
 * Note: This class is also extended to run tests against different
 * configurations.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class DefaultCoreServicesTest {

    @Inject
    private BundleContext m_bundleContext;
    private TestContext m_testContext;

    @Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAll());
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        CoreConfigs.provisionAll(m_testContext);
    }

    @After
    public void tearDown() throws Exception {
        m_testContext.tearDown();
    }

    /**
     * Test availability of core non tenant aware services that should always be available.
     * 
     * @param bundleContext the injected context
     * @throws Exception when things go bad
     */
    @Test
    public void testCoreServicesAvailable() throws Exception {
        LogService logService = m_testContext.getService(LogService.class);
        assertThat("Expected a LogService to be available", logService, is(notNullValue()));
        ConfigurationAdmin configurationAdmin = m_testContext.getService(ConfigurationAdmin.class);
        assertThat("Expected a ConfigurationAdmin to be available", configurationAdmin, is(notNullValue()));
        EventAdmin eventAdmin = m_testContext.getService(EventAdmin.class);
        assertThat("Expected a EventAdmin to be available", eventAdmin, is(notNullValue()));
    }

    /**
     * Test DependencyManager usages. Shows how to use Components. Is work in progress.
     * 
     * @param bundleContext
     * @throws Exception
     */
    @Test
    public void testWithServiceDependencies() throws Exception {

        Component component = m_testContext.getDependencyManager().createComponent();
        component.setInterface(TestService.class.getName(), null);
        component.setImplementation(TestServiceImpl.class);

        ServiceDependency serviceDependency = m_testContext.getDependencyManager().createServiceDependency();
        serviceDependency.setService(LogService.class);
        serviceDependency.setRequired(true);
        component.add(serviceDependency);
        m_testContext.getDependencyManager().add(component);

        TestService testInterface = m_testContext.getService(TestService.class);
        assertThat(testInterface, is(notNullValue()));
    }

    /**
     * Testing tenant aware behavior for UserAdmin. Shows how a test can update configuration.
     * 
     * @param bundleContext injected bundleContext
     * @throws Exception when things go bad
     */
    @Test
    public void testTenantAwareUserAdminAvailable() throws Exception {

//        Properties properties = new Properties();
//        properties.put(Constants.PID_KEY, "tenant@localhost");
//        properties.put(Constants.NAME_KEY, "Tenant localhost");
//
//        m_testContext.updateFactoryConfig(CoreConfigs.TENANT.getPid(), properties);
//
//        properties.put(Constants.PID_KEY, "tenant@127.0.0.1");
//        properties.put(Constants.NAME_KEY, "Tenant 127.0.0.1");
//        m_testContext.updateFactoryConfig(CoreConfigs.TENANT.getPid(), properties);
        
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", "tenant@127.0.0.1", "tenant@localhost");


        UserAdmin userAdmin1 =
            m_testContext.getService(UserAdmin.class,
                String.format("(%1$s=%2$s)", Constants.PID_KEY, "tenant@localhost"));

        assertThat("Expected a UserAdmin for tenant@localhost to be available", userAdmin1, is(notNullValue()));

        UserAdmin userAdmin2 =
            m_testContext.getService(UserAdmin.class,
                String.format("(%1$s=%2$s)", Constants.PID_KEY, "tenant@127.0.0.1"));
        assertThat("Expected a UserAdmin for tenant@127.0.0.1 to be available", userAdmin2, is(notNullValue()));

        UserAdmin userAdmin3 = m_testContext.getService(UserAdmin.class,
            String.format("(%1$s=%2$s)", Constants.PID_KEY, "tenant@overtherainbow"), 100);
        assertThat("Expected no UserAdmin for tenant@overtherainbow to not be available", userAdmin3, is(nullValue()));
    }
}
