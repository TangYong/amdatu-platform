/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.dp;

import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.net.URL;

import javax.inject.Inject;

import org.amdatu.itest.base.ManagementAgent;
import org.apache.ace.builder.DeploymentPackageBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class MultiTenantTest {
    private static final int TIMEOUT = 3000;
    private static final int SLEEPTIME = 100;
    @Inject
    private BundleContext m_bundleContext;
    
    @Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            wrappedBundle(mavenBundle().groupId("org.apache.ace").artifactId("org.apache.ace.builder").versionAsInProject()),
            ManagementAgent.managementAgentBundle()
        );
    }

    @Test
    public void testDeploymentPackageWithConfiguration() throws Exception {
        String target = "target-MT";
        ManagementAgent ma = new ManagementAgent(m_bundleContext, target);
        ma.start();
        
        try {
            // Create a deployment package containing our multi tenancy bundles, a resource processor and two tenant configurations
            ma.createDeploymentPackage(DeploymentPackageBuilder.createDeploymentPackage(target, "1.0.0")
                .addBundle(new URL("mvn:org.amdatu.tenant/org.amdatu.tenant.factory/0.4.0-SNAPSHOT"))
                .addBundle(new URL("mvn:org.amdatu.tenant/org.amdatu.tenant.adapter/0.4.0-SNAPSHOT"))
                .addResourceProcessor(new URL("mvn:org.amdatu.tenant/org.amdatu.tenant.conf.rp/0.4.0-SNAPSHOT"))
                .addArtifact(getClass().getResource("/tenant-A.tenant"), "org.amdatu.tenant.conf.rp")
                .addArtifact(getClass().getResource("/tenant-B.tenant"), "org.amdatu.tenant.conf.rp")
            );
            ServiceTracker tracker = new ServiceTracker(m_bundleContext, m_bundleContext.createFilter("(&(" + Constants.OBJECTCLASS + "=org.amdatu.tenant.Tenant)(org.amdatu.tenant.pid=*))"), null);
            tracker.open();
            // validate that we end up with two tenant services
            int countdown = TIMEOUT / SLEEPTIME;
            ServiceReference[] refs = tracker.getServiceReferences();
            while (countdown-- > 0 && (refs == null || refs.length != 2)) {
                Thread.sleep(SLEEPTIME);
                refs = tracker.getServiceReferences();
            }
            Assert.assertTrue(countdown > 0);
        }
        finally {
            ma.stop();
        }
    }
}
