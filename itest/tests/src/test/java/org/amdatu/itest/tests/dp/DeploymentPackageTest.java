/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.dp;

import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.net.URL;

import javax.inject.Inject;

import org.amdatu.itest.base.ManagementAgent;
import org.apache.ace.builder.DeploymentPackageBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.http.HttpService;
import org.osgi.service.log.LogService;
import org.osgi.util.tracker.ServiceTracker;

@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class DeploymentPackageTest {
    private static final int TIMEOUT = 3000;
    private static final int SLEEPTIME = 100;
    @Inject
    private BundleContext m_bundleContext;
    
    @Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            wrappedBundle(mavenBundle().groupId("org.apache.ace").artifactId("org.apache.ace.builder").versionAsInProject()),
            ManagementAgent.managementAgentBundle()
        );
    }

    @Test
    public void testDeploymentPackageWithLogService() throws Exception {
        String target = "target-A";
        ManagementAgent ma = new ManagementAgent(m_bundleContext, target);
        ma.start();
        
        try {
            // Step 1: Install a deployment package with log service 1.0.0 and make sure we see the log service.
            ma.createDeploymentPackage(DeploymentPackageBuilder.createDeploymentPackage(target, "1.0.0")
                .addBundle(new URL("mvn:org.apache.felix/org.apache.felix.log/1.0.0"))
            );
            ServiceTracker tracker = new ServiceTracker(m_bundleContext, LogService.class.getName(), null);
            tracker.open();
            LogService log = (LogService) tracker.waitForService(TIMEOUT);
            Assert.assertNotNull(log);
            
            // Step 2: Install an updated deployment package and make sure we see a different log service.
            ma.createDeploymentPackage(DeploymentPackageBuilder.createDeploymentPackage(target, "2.0.0")
                .addBundle(new URL("mvn:org.apache.felix/org.apache.felix.log/1.0.1"))
            );
            int countdown = TIMEOUT / SLEEPTIME;
            while (countdown-- > 0 && log.equals(tracker.getService())) {
                Thread.sleep(SLEEPTIME);
            }
            Assert.assertTrue(countdown > 0);
            
            // Step 3: Install an updated and empty deployment package and make sure the log service goes away.
            ma.createDeploymentPackage(DeploymentPackageBuilder.createDeploymentPackage(target, "3.0.0")
            );
            countdown = TIMEOUT / SLEEPTIME;
            while (countdown-- > 0 && tracker.getService() != null) {
                Thread.sleep(SLEEPTIME);
            }
            Assert.assertTrue(countdown > 0);
        }
        finally {
            ma.stop();
        }
    }
    
    @Test
    public void testDeploymentPackageWithConfiguration() throws Exception {
        String target = "target-B";
        ManagementAgent ma = new ManagementAgent(m_bundleContext, target);
        ma.start();
        
        try {
            // Create a deployment package containing the HTTP service, configuration admin, a resource processor and a configuration.
            ma.createDeploymentPackage(DeploymentPackageBuilder.createDeploymentPackage(target, "1.0.0")
                .addBundle(new URL("mvn:org.apache.felix/org.apache.felix.http.jetty/2.2.0"))
                .addBundle(new URL("mvn:org.apache.felix/org.apache.felix.configadmin/1.2.8"))
                .addResourceProcessor(new URL("mvn:org.amdatu.deployment/org.amdatu.deployment.autoconf/0.4.0-SNAPSHOT"))
                .addArtifact(getClass().getResource("/http-conf.xml"), "org.osgi.deployment.rp.autoconf")
            );
            // Validate that we have a HTTP server at port 8008 (as configured) by creating a tracker that
            // listens for HttpService with a port property of 8008.
            ServiceTracker tracker = new ServiceTracker(m_bundleContext, m_bundleContext.createFilter("(&(" + Constants.OBJECTCLASS + "=" + HttpService.class.getName() + ")(org.osgi.service.http.port=8008))"), null);
            tracker.open();
            HttpService http = (HttpService) tracker.waitForService(TIMEOUT);
            Assert.assertNotNull(http);
        }
        finally {
            ma.stop();
        }
    }
}
