/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tests.tenant;

import static junit.framework.Assert.assertTrue;
import static org.amdatu.itest.tests.tenant.MultiTenantTest.countServices;
import static org.amdatu.tenant.Constants.PID_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.ops4j.pax.exam.CoreOptions.junitBundles;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.provision;
import static org.ops4j.pax.exam.CoreOptions.systemTimeout;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.itest.tenant.MyDependentService;
import org.amdatu.tenant.Constants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;

/**
 * Benchmark test for multi-tenancy. Tests whether it is possible to run a "large" number of tenants in a single container.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class MultiTenantBenchmarkTest {

    private static final int TENANT_COUNT = 100;

    @Inject
    private BundleContext m_bundleContext;
    private TestContext m_testContext;
    private final List<Configuration> m_configurations = new ArrayList<Configuration>();

    @org.ops4j.pax.exam.junit.Configuration
    public Option[] config() {
        return options(
            junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAll(),
            provision(mavenBundle().groupId("org.amdatu.itest").artifactId("org.amdatu.itest.tenant")
                .versionAsInProject()),
            systemTimeout(5000));
    }

    /**
     * Sets up an individual test case.
     *
     * @throws Exception not part of this set up.
     */
    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");

        CoreConfigs.provisionAllExcluding(m_testContext);

        m_testContext.waitForSystemToSettle();
    }

    /**
     * Tears down the test case.
     */
    @After
    public void tearDown() {
        for (Configuration config : m_configurations) {
            try {
                config.delete();
            }
            catch (Exception exception) {
                // Ignore...
            }
        }
        m_configurations.clear();
        m_testContext.tearDown();
    }

    /**
     * Tests that for a single OSGi-container many tenant services can be materialized.
     *
     * @throws Exception not part of this test case.
     */
    @Test
    public void testBenchmark() throws Exception {
        List<String> createdTenantPIDs = new ArrayList<String>(TENANT_COUNT);

        // Ramp up: create up to TENANT_COUNT tenants (start at one as we get a _PLATFORM tenant for free)...
        String[] tenants = new String[TENANT_COUNT];
        for (int i = 0; i < TENANT_COUNT; i++) {
            tenants[i] = "tenant-" + i;
        }
        
        m_testContext.configureTenants(tenants);

        long timeout = TENANT_COUNT * TENANT_COUNT;

        // Wait until the system has been settled and all tenants are activated...
        waitUntilServicesAreRegistered(timeout, TENANT_COUNT, MyDependentService.class);

        assertEquals(TENANT_COUNT, countMyDependentServices());

        List<Configuration> tmpConfigs = new ArrayList<Configuration>(m_configurations);

        // Make a call to each and every tenant...
        List<String> seenList = new ArrayList<String>(createdTenantPIDs);
        
        for (int i = 0; i < TENANT_COUNT; i++) {
            String tenantPID = tenants[i];
            MyDependentService service = getServiceInstance(tenantPID);
            assertNotNull(service.sayIt());

            // Mark this tenant as seen...
            seenList.remove(tenantPID);
        }

        assertEquals("Not all tenants were seen/called?!", 0, seenList.size());

        // We should have seen *no* new tenants coming in, or old tenants leaving us...
        assertEquals(TENANT_COUNT, countMyDependentServices());

        // Ramp down: remove each and every tenant instance...
        m_testContext.configureTenants(Constants.PID_VALUE_PLATFORM);

        // Wait until the system has been settled and all tenants are activated...
        waitUntilServicesAreRegistered(timeout, 1, MyDependentService.class);

        // We should have only the platform tenant left by now...
        assertEquals(1, countMyDependentServices());
    }

    /**
     * Waits until either a given number of services is seen, or until a certain timeout occurred.
     *
     * @param timeout the time out, in milliseconds;
     * @param serviceCount the number of services to expect;
     * @param countedServiceClass the service to count.
     * @throws InterruptedException in case we're being interrupted while waiting for the services to come up.
     */
    private void waitUntilServicesAreRegistered(long timeout, int serviceCount, Class<?> countedServiceClass)
        throws InterruptedException {
        long end = System.currentTimeMillis() + timeout;

        int sc;
        do {
            TimeUnit.MILLISECONDS.sleep(10);
            sc = countServices(m_bundleContext, countedServiceClass.getName());
        }
        while ((sc != serviceCount) && (System.currentTimeMillis() < end));
    }

    /**
     * @param tenantPID
     * @return
     */
    private MyDependentService getServiceInstance(String tenantPID) throws Exception {
        return m_testContext.getService(MyDependentService.class, String.format("(%1$s=%2$s)", PID_KEY, tenantPID));
    }

    /**
     * @return the number of {@link MyDependentService} instances registered with OSGi.
     */
    private int countMyDependentServices() {
        return countServices(m_bundleContext, MyDependentService.class.getName());
    }
}
