/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tenant.impl;

import org.amdatu.itest.tenant.MyGlobalService;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MyGlobalServiceImpl implements MyGlobalService {
    private volatile LogService m_logService;
    private volatile ServiceRegistration m_serviceRegistration;

    /**
     * @see org.amdatu.itest.tenant.MyGlobalService#getServiceRegistration()
     */
    public ServiceRegistration getServiceRegistration() {
        m_logService.log(LogService.LOG_WARNING, "MyGlobalService#getServiceRegistration() called!");
        return m_serviceRegistration;
    }
}
