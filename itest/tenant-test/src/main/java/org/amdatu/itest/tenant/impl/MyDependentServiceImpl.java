/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.tenant.impl;

import org.amdatu.itest.tenant.MyDependencyService;
import org.amdatu.itest.tenant.MyDependentService;
import org.amdatu.tenant.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

/**
 * Test implementation of {@link MyDependentService}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MyDependentServiceImpl implements MyDependentService, LogListener {
    private volatile ServiceRegistration m_serviceReg;
    private volatile MyDependencyService m_dependency;

    /**
     * @see org.amdatu.itest.tenant.MyDependentService#sayIt()
     */
    public String sayIt() {
        return String.format("[%s] %s",
            m_serviceReg.getReference().getProperty(Constants.PID_KEY),
            m_dependency.saySomething());
    }

    /**
     * @see org.osgi.service.log.LogListener#logged(org.osgi.service.log.LogEntry)
     */
    public void logged(LogEntry entry) {
        if (entry.getLevel() <= LogService.LOG_WARNING) {
            System.out.println("[MyDependentService] " + entry.getMessage());
        }
        if (entry.getException() != null) {
            entry.getException().printStackTrace();
        }
    }

    /**
     * @param logReaderService the added log reader service.
     */
    final void addLogReaderService(LogReaderService logReaderService) {
        logReaderService.addLogListener(this);
    }

    /**
     * @param logReaderService the removed log reader service.
     */
    final void removeLogReaderService(LogReaderService logReaderService) {
        logReaderService.removeLogListener(this);
    }
}
