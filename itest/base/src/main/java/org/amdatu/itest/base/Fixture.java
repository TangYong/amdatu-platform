/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.ProvisionOption;

/**
 * Amdatu Core integration test fixture providing provisioning and configuration
 * for tests.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @deprecated
 */
public final class Fixture {

    /**
     * Utility class, not meant to instantiate.
     */
    private Fixture() {
        // NO-op
    }

    /**
     * Returns the classifier to add to maven artifact options that have coverage.
     * 
     * @return the classifier
     */
    public static String getCoverageClassifier() {
        return (System.getProperty("amdatu.build.itest.classifier") == null ? "" : System
            .getProperty("amdatu.build.itest.classifier"));
    }

    /**
     * Returns a Pax Exam composite provisioning option for all requested bundles.
     * 
     * @param includes the included bundles, can be none if no bundles are to be provisioned.
     * @return the provisioning option, never <code>null</code>.
     */
    public static Option provision(ProvisionedBundle... includes) {
        if (includes == null || includes.length == 0) {
            throw new IllegalArgumentException(
                "Fixture#provision() should have at least one bundle to provision. Incorrect API-call?");
        }

        List<ProvisionOption<?>> provisionOptions = new ArrayList<ProvisionOption<?>>();
        for (ProvisionedBundle b : includes) {
            provisionOptions.add(b.getProvisionOption());
        }

        return org.ops4j.pax.exam.CoreOptions.provision(provisionOptions
            .toArray(new ProvisionOption<?>[provisionOptions.size()]));
    }

    /**
     * Configures a <code>TestContext</code> with the default configuration for configurations that
     * are not listed as an exclude.
     * 
     * @param testContext the testContext to configure;
     * @param includes the included configurations, can be none if no configurations are to be provisioned.
     * @throws Exception if (partial) configuration fails
     */
    public static void configure(TestContext testContext, ProvisionedConfig... includes) throws IOException {
        if (includes == null || includes.length == 0) {
            throw new IllegalArgumentException(
                "Fixture#configure() should have at least one configuration to provision. Incorrect API-call?");
        }
        try {
            for (ProvisionedConfig c : includes) {
                testContext.updateConfig(c.getPid(), c.getProperties(), c.isFactory());
            }
        }
        finally {
            // In any case, wait a little while, even if the configuration was rejected...
            testContext.waitForSystemToSettle();
        }
    }
}
