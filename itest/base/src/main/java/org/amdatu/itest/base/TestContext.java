/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.base;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;

import org.amdatu.tenant.Tenant;
import org.amdatu.tenant.TenantFactoryConfiguration;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Amdatu Core integration test utility class for tests providing utilities
 * to easily and safely write tests.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @deprecated
 */
public class TestContext {

    /** The number of milliseconds to wait until a service lookup should fail. */
    private static final int SERVICE_LOOKUP_TIMEOUT = 10000;

    private final List<ServiceTracker> m_serviceTrackers = new LinkedList<ServiceTracker>();

    private BundleContext m_bundleContext;
    private DependencyManager m_dependencyManager;

    /**
     * Creates a new {@link TestContext} instance, ensuring a new dependency manager is created.
     * 
     * @param bundleContext the bundle context of this test context, cannot be <code>null</code>.
     */
    public TestContext(BundleContext bundleContext) {
        m_bundleContext = bundleContext;
        m_dependencyManager = new DependencyManager(m_bundleContext);
    }
    
    public void configureTenants(String... pids) {
        Map<String, Map<String, Object>> tenants = new HashMap<String, Map<String, Object>>();
        for (String pid : pids) {
            String name = "Tentant " + pid;
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(org.amdatu.tenant.Constants.PID_KEY, pid);
            props.put(org.amdatu.tenant.Constants.NAME_KEY, name);
            tenants.put(pid, props);
        }
        TenantFactoryConfiguration tfc = getService(TenantFactoryConfiguration.class);
        if (tfc != null) {
            tfc.update(tenants);
        }
        else {
            Assert.fail("No tenant factory configuration service found.");
        }
    }
    
    public void configureTenants(TenantWithProperties... tenants) {
        Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
        for (TenantWithProperties tenant : tenants) {
            result.put(tenant.getPid(), tenant.getProperties());
        }
        TenantFactoryConfiguration tfc = getService(TenantFactoryConfiguration.class);
        if (tfc != null) {
            tfc.update(result);
        }
        else {
            Assert.fail("No tenant factory configuration service found.");
        }
    }
    
    public static class TenantWithProperties {
        private final String m_pid;
        private final Map<String, Object> m_properties;

        public TenantWithProperties(String pid, Object... properties) {
            m_pid = pid;
            m_properties = new HashMap<String, Object>();
            for (int i = 0; i < properties.length; i += 2) {
                m_properties.put((String) properties[i], properties[i + 1]);
            }
        }
        
        public String getPid() {
            return m_pid;
        }
        
        public Map<String, Object> getProperties() {
            return m_properties;
        }
    }
    
    /**
     * Returns the current test's bundle context.
     * 
     * @return this test context's bundle context, never <code>null</code>.
     */
    public BundleContext getBundleContext() {
        return m_bundleContext;
    }

    /**
     * Convenience method to return the {@link ConfigurationAdmin} service.
     * 
     * @return the {@link ConfigurationAdmin} service, can be <code>null</code> if it is not available.
     */
    public ConfigurationAdmin getConfigurationAdmin() {
        return getService(ConfigurationAdmin.class);
    }

    /**
     * Returns the Felix dependency manager.
     * 
     * @return a dependency manager instance, never <code>null</code>.
     */
    public DependencyManager getDependencyManager() {
        return m_dependencyManager;
    }

    /**
     * Tries to acquire a service with the default timeout ({@value #SERVICE_LOOKUP_TIMEOUT} ms).
     * <p>NOTE: this method blocks until the requested service becomes available, or until the timeout has expired!</p>
     * 
     * @param serviceClass the service class to obtain a service for, cannot be <code>null</code>.
     * @return the service instance, can be <code>null</code> if no matching service is found.
     */
    public <T> T getService(Class<T> serviceClass) {
        return getService(serviceClass, SERVICE_LOOKUP_TIMEOUT);
    }

    /**
     * Tries to acquire a service with a given timeout.
     * <p>NOTE: this method blocks until the requested service becomes available, or until the given timeout has expired!</p>
     * 
     * @param serviceClass the service class to obtain a service for, cannot be <code>null</code>;
     * @param timeout the timeout (in milliseconds) to wait for the service to become available, >= 0.
     * @return the service instance, can be <code>null</code> if no matching service is found.
     */
    public <T> T getService(Class<T> serviceClass, long timeout) {
        try {
            return getService(serviceClass, null, timeout);
        }
        catch (InvalidSyntaxException exception) {
            throw new RuntimeException("null-filter invalid?!");
        }
    }

    /**
     * Tries to acquire a service additionally specified by the given filter expression.
     * <p>NOTE: this method blocks until the requested service becomes available, or until the timeout has expired!</p>
     * 
     * @param serviceClass the service class to obtain a service for, cannot be <code>null</code>;
     * @param extraFilterExpression the additional filter clause to narrow down the requested service.
     * @return the service instance, can be <code>null</code> if no matching service is found.
     * @throws InvalidSyntaxException in case the given filter expression was invalid.
     */
    public <T> T getService(Class<T> serviceClass, String extraFilterExpression)
        throws InvalidSyntaxException {
        return getService(serviceClass, extraFilterExpression, SERVICE_LOOKUP_TIMEOUT);
    }

    /**
     * Tries to acquire a service additionally specified by the given filter expression.
     * <p>NOTE: this method blocks until the requested service becomes available, or until the given timeout has expired!</p>
     * 
     * @param serviceClass the service class to obtain a service for, cannot be <code>null</code>;
     * @param extraFilterExpression the additional filter clause to narrow down the requested service;
     * @param timeout the timeout (in milliseconds) to wait for the service to become available, >= 0.
     * @return the service instance, can be <code>null</code> if no matching service is found.
     * @throws InvalidSyntaxException in case the given filter expression was invalid.
     */
    public <T> T getService(Class<T> serviceClass, String extraFilterExpression, long timeout)
        throws InvalidSyntaxException {
        String filterExpression = String.format("(%s=%s)", Constants.OBJECTCLASS, serviceClass.getName());
        if (extraFilterExpression != null) {
            filterExpression = String.format("(&%s%s)", filterExpression, extraFilterExpression);
        }

        ServiceTracker serviceTracker =
            new ServiceTracker(m_bundleContext, m_bundleContext.createFilter(filterExpression), null);
        serviceTracker.open();

        m_serviceTrackers.add(serviceTracker);

        try {
            return (T) serviceTracker.waitForService(timeout);
        }
        catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while waiting for service!");
        }
    }

    /**
     * Should be called before test to ensure all bundles are started.
     */
    public void setUp() throws Exception {
        for (Bundle b : m_bundleContext.getBundles()) {
            if (b.getState() != Bundle.ACTIVE) {
                b.start();
            }
        }
    }

    /**
     * Should be called after test to ensure all created service trackers are closed.
     */
    public void tearDown() {
        for (ServiceTracker serviceTracker : m_serviceTrackers) {
            serviceTracker.close();
        }
    }

    /**
     * Updates the configuration for the {@link ManagedService} identified by the given PID to the given properties.
     * 
     * @param pid the PID of the {@link ManagedService} to update, cannot be <code>null</code>;
     * @param properties the new service properties, can be <code>null</code> to delete the existing service properties (if any).
     * @return the updated configuration information, never <code>null</code>.
     * @throws IOException if the configuration was rejected or otherwise caused an exception.
     */
    public Configuration updateConfig(String pid, Properties properties) throws IOException {
        return updateConfig(pid, properties, false /* factory */);
    }

    /**
     * Updates the configuration for the {@link ManagedServiceFactory} identified by the given PID to the given properties.
     * 
     * @param pid the PID of the {@link ManagedServiceFactory} to update, cannot be <code>null</code>;
     * @param properties the new service properties, can be <code>null</code> to delete the existing service properties (if any).
     * @return the updated configuration information, never <code>null</code>.
     * @throws IOException if the configuration was rejected or otherwise caused an exception.
     */
    public Configuration updateFactoryConfig(String pid, Properties properties) throws IOException {
        return updateConfig(pid, properties, true /* factory */);
    }

    /**
     * Allows the system to settle after a configuration has been supplied to the {@link ConfigurationAdmin} service.
     */
    public void waitForSystemToSettle() {
        try {
            Thread.sleep(250);
        }
        catch (InterruptedException exception) {
            fail("Interrupted while waiting for configuration!");
        }
    }

    /**
     * Updates the configuration for the {@link ManagedService} identified by the given PID to the given properties.
     * 
     * @param pid the PID of the {@link ManagedService} to update, cannot be <code>null</code>;
     * @param properties the new service properties, can be <code>null</code> to delete the existing service properties (if any);
     * @param factory if <code>true</code> assume the given PID is a {@link ManagedServiceFactory} that creates the configuration for us, <code>false</code> if the given PID is a "normal" {@link ManagedService}.
     * @return the updated configuration information, never <code>null</code>.
     * @throws IOException if the configuration was rejected or otherwise caused an exception.
     */
    final Configuration updateConfig(String pid, Properties properties, boolean factory) throws IOException {
        ServiceReference sr = getConfigAdminServiceRef();
        try {
            ConfigurationAdmin configurationAdmin = getConfigAdminService(sr);

            Configuration configuration;
            if (!factory) {
                configuration = configurationAdmin.getConfiguration(pid, null /* location */);
            }
            else {
                configuration = configurationAdmin.createFactoryConfiguration(pid, null /* location */);
            }
            configuration.update(properties);

            return configuration;
        }
        finally {
            m_bundleContext.ungetService(sr);
        }
    }

    /**
     * Obtains the {@link ConfigurationAdmin} service through the given service reference.
     * 
     * @param sr the service reference of the {@link ConfigurationAdmin} service, cannot be <code>null</code>.
     * @return the {@link ConfigurationAdmin} service, never <code>null</code>.
     * @throws AssertionFailedError in case the requested service was not available.
     */
    private ConfigurationAdmin getConfigAdminService(ServiceReference sr) {
        ConfigurationAdmin configurationAdmin = (ConfigurationAdmin) m_bundleContext.getService(sr);
        assertThat("ConfigurationAdmin service unavailable", configurationAdmin, is(notNullValue()));
        return configurationAdmin;
    }

    /**
     * Obtains the {@link ConfigurationAdmin} service reference.
     * 
     * @return the {@link ConfigurationAdmin} service reference, never <code>null</code>.
     * @throws AssertionFailedError in case the requested service reference was not available.
     */
    private ServiceReference getConfigAdminServiceRef() {
        ServiceReference[] refs = null;
        try {
            refs = m_bundleContext.getServiceReferences(ConfigurationAdmin.class.getName(), "(" + org.amdatu.tenant.Constants.PID_KEY + "=" + org.amdatu.tenant.Constants.PID_VALUE_PLATFORM + ")");
        }
        catch (InvalidSyntaxException e) {
            throw new RuntimeException(e);
        }
        assertThat("ConfigurationAdmin serviceReference unavailable", refs, is(notNullValue()));
        return refs[0];
    }
}
