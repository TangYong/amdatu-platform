/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.base;

import static org.ops4j.pax.exam.CoreOptions.mavenBundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.ProvisionOption;
import org.ops4j.pax.exam.options.UrlProvisionOption;

/**
 * Provides a mean to include/exclude platform bundles from an integration test.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @deprecated
 */
public enum CoreBundles implements ProvisionedBundle {
    // build properties used
    MANAGEMENTAGENT("org.apache.ace", "org.apache.ace.managementagent", "${org.apache.ace.version}", false),
        DEPENDENCYMANAGER("org.apache.felix", "org.apache.felix.dependencymanager", null, false),
        LOG("org.apache.felix", "org.apache.felix.log", null, false),
        METATYPE("org.apache.felix", "org.apache.felix.metatype", null, false),
        FILEINSTALL("org.apache.felix", "org.apache.felix.fileinstall", null, false),
        CONFIGADMIN("org.amdatu.configadmin", "org.amdatu.multitenant.org.apache.felix.configadmin", null, true),
        EVENTADMIN("org.amdatu.eventadmin", "org.amdatu.multitenant.org.apache.felix.eventadmin", null, true),
        PREFS("org.amdatu.preferences", "org.amdatu.multitenant.org.apache.felix.prefs", null, true),
        USERADMIN("org.amdatu.useradmin", "org.amdatu.multitenant.org.ops4j.pax.useradmin.pax-useradmin-service", null,
            true),
        USERADMIN_FSSTORAGE("org.amdatu.useradmin", "org.amdatu.multitenant.org.amdatu.useradmin.pax.fsstorage", null,
            true),
        ITEST_BASE("org.amdatu.itest", "org.amdatu.itest.base", null, true),
        DEPLOYMENT_AUTOCONF("org.amdatu.deployment", "org.amdatu.deployment.autoconf", null, true),
        FILEINSTALL_AUTOCONF("org.amdatu.fileinstall", "org.amdatu.fileinstall.autoconf", null, true),
        TENANT("org.amdatu.tenant", "org.amdatu.tenant.api", null, true),
        TENANT_ADAPTER("org.amdatu.tenant", "org.amdatu.tenant.adapter", null, true),
        TENANT_FACTORY("org.amdatu.tenant", "org.amdatu.tenant.factory", null, true),
        TENANT_CONF_RP("org.amdatu.tenant", "org.amdatu.tenant.conf.rp", null, true);
//        TENANT_CONF_CONFIGADMIN("org.amdatu.tenant", "org.amdatu.tenant.conf.configadmin", null, true);

    private final String m_groupId;
    private final String m_artifactId;
    private final String m_version;
    private final boolean m_addCoverageClassifier;

    /**
     * Creates a new {@link CoreBundles} instance with a given group and artifact-ID.
     * 
     * @param groupId the Maven group ID of the bundle;
     * @param artifactId the Maven artifact ID of the bundle.
     */
    private CoreBundles(String groupId, String artifactId, String version, boolean addCoverageClassifier) {
        m_groupId = groupId;
        m_artifactId = artifactId;
        m_version = version;
        m_addCoverageClassifier = addCoverageClassifier;
    }

    /**
     * Convenience method to provision all of the core bundles.
     * 
     * @return a PAX-exam provisioning option with all core bundles.
     */
    public static Option provisionAll() {
        return provisionAllExcluding();
    }

    /**
     * Convenience method to provision a subset of the core bundles.
     * 
     * @param excludes the optional core bundles to exclude from provisioning.
     * @return a PAX-exam provisioning option with the requested subset of core bundles.
     */
    public static Option provisionAllExcluding(CoreBundles... excludes) {
        List<CoreBundles> values = new ArrayList<CoreBundles>(Arrays.asList(values()));
        values.removeAll(Arrays.asList(excludes));
        return Fixture.provision(values.toArray(new CoreBundles[values.size()]));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.amdatu.itest.base.ProvisionedBundle#getProvisionOption()
     */
    public ProvisionOption<?> getProvisionOption() {
        ProvisionOption<?> option = null;
        if (m_version != null) {
            option =
                mavenBundle().groupId(m_groupId).artifactId(m_artifactId).version(m_version).start(false);
        }
        else {
            option =
                mavenBundle().groupId(m_groupId).artifactId(m_artifactId).versionAsInProject().start(false);
        }
        if (m_addCoverageClassifier) {
            option = new UrlProvisionOption(option.getURL() + "//" + Fixture.getCoverageClassifier());
        }
        return option;
    }
}