/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.base;

import static org.ops4j.pax.exam.CoreOptions.mavenBundle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.ace.builder.DeploymentPackageBuilder;
import org.ops4j.pax.exam.options.CompositeOption;
import org.ops4j.pax.exam.options.DefaultCompositeOption;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

/**
 * Abstraction for working with a management agent in integration tests. Can be used together with
 * the DeploymentPackageBuilder to create deployment packages and install them in the running framework.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ManagementAgent {
    private static final String MANAGEMENTAGENT_GROUPID = "org.apache.ace";
    private static final String MANAGEMENTAGENT_SYMBOLICNAME = "org.apache.ace.managementagent";
    private static final String MANAGEMENTAGENT_VERSION = "${org.apache.ace.version}"; // build property
    private final String m_target;
    private final Bundle m_bundle;
    private File m_tempDir;
    private File m_targetDir;

    /**
     * Creates a new management agent.
     * 
     * @param bundleContext the context to use when looking for the agent bundle
     * @param target the name of the target to use for this agent
     */
    public ManagementAgent(BundleContext bundleContext, String target) {
        m_target = target;
        Bundle[] bundles = bundleContext.getBundles();
        for (Bundle b : bundles) {
            if (MANAGEMENTAGENT_SYMBOLICNAME.equals(b.getSymbolicName())) {
                m_bundle = b;
                return;
            }
        }
        throw new IllegalStateException("Could not find management agent.");
    }

    /**
     * Returns the bundle for the management agent, to be used with the Pax Exam options.
     */
    public static CompositeOption managementAgentBundle() {
        return new DefaultCompositeOption(
            mavenBundle(MANAGEMENTAGENT_GROUPID, MANAGEMENTAGENT_SYMBOLICNAME, MANAGEMENTAGENT_VERSION).noStart(),
            mavenBundle("org.apache.felix", "org.apache.felix.dependencymanager").versionAsInProject(),
            mavenBundle("org.amdatu.tenant", "org.amdatu.tenant.api").versionAsInProject(),
            mavenBundle("org.amdatu.itest", "org.amdatu.itest.base").versionAsInProject());
    }

    /**
     * Starts the management agent, using a temporary directory for discovery.
     */
    public void start() throws Exception {
        m_tempDir = File.createTempFile("deploy-", "-dir");
        if (!m_tempDir.delete()) {
            throw new IllegalStateException("Failed to delete temporary file: " + m_tempDir.getAbsolutePath());
        }
        if (!m_tempDir.mkdir()) {
            throw new IllegalStateException("Failed to create directory: " + m_tempDir.getAbsolutePath());
        }
        String discovery = m_tempDir.getAbsoluteFile().toURI().toURL().toExternalForm();
        System.setProperty("discovery", discovery);
        System.setProperty("identification", m_target);

        m_targetDir = new File(m_tempDir, "deployment/" + m_target + "/versions");
        if (!m_targetDir.mkdirs()) {
            throw new IOException("Could not make dirs: " + m_targetDir.getAbsolutePath());
        }

        m_bundle.start();
    }

    /**
     * Stops the management agent and cleans up any temporary files.
     */
    public void stop() throws Exception {
        m_bundle.stop();

        boolean success = true;
        File targetNameFolder = m_targetDir.getParentFile();
        File deploymentFolder = targetNameFolder.getParentFile();
        for (File f : m_targetDir.listFiles()) {
            success &= f.delete();
        }
        success &= m_targetDir.delete();
        success &= targetNameFolder.delete();
        success &= deploymentFolder.delete();
        success &= m_tempDir.delete();
        if (!success) {
            throw new IOException("Could not delete all files under " + m_tempDir.getAbsolutePath());
        }
    }

    /**
     * Returns the directory where versioned deployment packages should be installed.
     */
    public File getTargetDir() {
        return m_targetDir;
    }

    /**
     * Creates a new deployment packages using a builder that has already been configured to include
     * all artifacts, bundles and resource processors. The package will be created in the correct folder
     * for installing it, using the version as its file name.
     * 
     * @param builder a builder to which all artifacts have already been added
     */
    public void createDeploymentPackage(DeploymentPackageBuilder builder) throws Exception {
        File dp = new File(m_targetDir, builder.getVersion());
        File tempFile = File.createTempFile("dp-", ".jar");
        FileOutputStream fos = new FileOutputStream(tempFile);
        builder.generate(fos);
        fos.close();
        tempFile.renameTo(dp);
    }
}
