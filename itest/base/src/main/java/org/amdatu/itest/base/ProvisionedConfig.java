/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.itest.base;

import java.util.Properties;

import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Denotes a provisioned configuration.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * @deprecated
 */
public interface ProvisionedConfig {
    /**
     * Returns the PID of the {@link ManagedService} or {@link ManagedServiceFactory}.
     * 
     * @return a PID, never <code>null</code>.
     */
    String getPid();

    /**
     * Returns the configuration properties.
     * 
     * @return a configuration properties, can be <code>null</code> if the configuration is to be deleted.
     */
    Properties getProperties();

    /**
     * Returns whether or not the PID denotes a {@link ManagedServiceFactory}.
     * 
     * @return <code>true</code> if this configuration is for a {@link ManagedServiceFactory}, <code>false</code> if it is for a {@link ManagedService}.
     */
    boolean isFactory();
}
