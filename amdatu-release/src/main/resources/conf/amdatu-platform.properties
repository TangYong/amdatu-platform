#
# Copyright (c) 2010-2012 The Amdatu Foundation
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#
# Amdatu Platform core configuration properties for the Apache Felix 
# OSGi container.
#
# In general there should be as little as possible generic configuration 
# in this file. Avoid any project specific settings as they will may
# conflict with other modules and may even fail in a managed cloud 
# deployment. As a rule off thumb:
#
# 1) Configuration must be done through ConfigurationAdmin in the
#    correct (tenant) configuration space.
# 2) Specific classloading settings, both bootdelegation and framework
#    packages, should be avoided at all cost.
#
# In short.. do not touch this file unless you know what you are doing.
# 

# The following property makes specified packages from the class path
# available to all bundles. You should avoid using this property.
#org.osgi.framework.bootdelegation=sun.*,com.sun.*

# Felix tries to guess when to implicitly boot delegate in certain
# situations to ease integration without outside code. This feature
# is enabled by default, uncomment the following line to disable it.
#felix.bootdelegation.implicit=false

# To override the packages the framework exports by default from the
# class path, set this variable.
#org.osgi.framework.system.packages=

# To append packages to the default set of exported system packages,
# set this value.
org.osgi.framework.system.packages.extra= \
    sun.misc, \
    com.sun.management, \
    org.w3c.dom.traversal

# The following property explicitly specifies the location of the bundle
# cache, which defaults to "felix-cache" in the current working directory.
# If this value is not absolute, then the felix.cache.rootdir controls
# how the absolute location is calculated. (See next property)
#org.osgi.framework.storage=felix-cache

# The following property is used to convert a relative bundle cache
# location into an absolute one by specifying the root to prepend to
# the relative cache path. The default for this property is the
# current working directory.
felix.cache.rootdir=work

# The following property is a space-delimited list of bundle URLs
# to install and start when the framework starts. The ending numerical
# component is the target start level. Any number of these properties
# may be specified for different start levels.
felix.auto.start.1= \
    reference:file:lib/org.osgi.compendium-4.2.0.jar \
    reference:file:lib/org.apache.felix.fileinstall-${org.apache.felix.file.install.version}.jar

# Configuration for file install in a local development scenario. It 
# should not be deployed and enabled in a real world deployment. 
felix.fileinstall.poll=3000
felix.fileinstall.dir=deploy
felix.fileinstall.debug=1
felix.fileinstall.bundles.new.start=true
felix.fileinstall.filter=.*
felix.fileinstall.disableConfigSave=true