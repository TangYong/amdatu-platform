/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.base;

import static org.amdatu.tenant.Constants.NAME_KEY;
import static org.amdatu.tenant.Constants.PID_KEY;
import static org.amdatu.web.tenantresolver.hostname.Constants.HOSTNAMES_KEY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.amdatu.itest.base.Fixture;
import org.amdatu.itest.base.ProvisionedConfig;
import org.amdatu.itest.base.TestContext;

/**
 * Provides a mean to provision web-configurations.
 */
public enum WebConfigs implements ProvisionedConfig {
    HTTPSERVICE("org.apache.felix.http", httpCnf(), false),
        TENANT("org.amdatu.tenant.factory", tenantCnf(), true);

    private final String m_pid;
    private final Properties m_properties;
    private final boolean m_factory;

    private WebConfigs(String pid, Properties properties, boolean factory) {
        m_pid = pid;
        m_properties = properties;
        m_factory = factory;
    }

    /**
     * Convenience method to provision a subset of the core configurations.
     * 
     * @param testContext the {@link TestContext} to use for provisioning the configurations;
     * @param excludes the optional core configurations to exclude from provisioning.
     * @return the given test context, never <code>null</code>.
     * @throws IOException in case on of the core configurations failed.
     */
    public static TestContext provisionAllExcluding(TestContext testContext, WebConfigs... excludes)
        throws IOException {
        List<WebConfigs> values = new ArrayList<WebConfigs>(Arrays.asList(values()));
        values.removeAll(Arrays.asList(excludes));

        Fixture.configure(testContext, values.toArray(new WebConfigs[values.size()]));
        return testContext;
    }

    /**
     * Convenience method to provision all of the core configurations.
     * 
     * @param testContext the {@link TestContext} to use for provisioning the configurations.
     * @return the given test context, never <code>null</code>.
     * @throws IOException in case on of the core configurations failed.
     */
    public static TestContext provisionAll(TestContext testContext) throws IOException {
        return provisionAllExcluding(testContext);
    }

    /*
     * (non-Javadoc)
     * @see org.amdatu.itest.base.ProvisionedConfig#getPid()
     */
    public String getPid() {
        return m_pid;
    }

    /*
     * (non-Javadoc)
     * @see org.amdatu.itest.base.ProvisionedConfig#getProperties()
     */
    public Properties getProperties() {
        return m_properties;
    }

    /*
     * (non-Javadoc)
     * @see org.amdatu.itest.base.ProvisionedConfig#isFactory()
     */
    public boolean isFactory() {
        return m_factory;
    }

    /**
     * @return The properties, port=8080, debug=true, storeDebug=false
     */
    private static Properties httpCnf() {
        Properties properties = new Properties();
        properties.put("org.osgi.service.http.port", "8080");
        properties.put("org.apache.felix.http.debug", "true");
        properties.put("org.apache.felix.log.storeDebug", "false");
        return properties;
    }

    private static Properties tenantCnf() {
        Properties properties = new Properties();
        properties.put(PID_KEY, "Default_Web");
        properties.put(NAME_KEY, "Default Tenant");
        properties.put(HOSTNAMES_KEY, new String[] {"localhost"});
        return properties;
    }
}