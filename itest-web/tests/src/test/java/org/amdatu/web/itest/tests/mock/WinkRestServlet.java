/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests.mock;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Path("")
public class WinkRestServlet 
{
    
    @GET
    @Produces({MediaType.TEXT_PLAIN})
    public String getTenantMessage1() {
        return "SUCCESS";
    }

    @GET
    @Path("foo")
    @Produces({MediaType.TEXT_PLAIN})
    public String getTenantMessage2() {
        return "FAIL @ /foo";
    }

    @GET
    @Path("foo/bar")
    @Produces({MediaType.TEXT_PLAIN})
    public String getTenantMessage3() {
        return "FAIL @ /foo/bar";
    }
    
}
