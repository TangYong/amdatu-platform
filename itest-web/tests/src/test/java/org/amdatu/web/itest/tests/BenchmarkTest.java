/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.Servlet;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.itest.base.WebBundles;
import org.amdatu.web.itest.base.WebConfigs;
import org.amdatu.web.itest.tests.mock.BenchMarkServlet;
import org.amdatu.web.itest.tests.mock.HttpRequestRunner;
import org.amdatu.web.itest.tests.util.WebUtils;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.CoreOptions;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;

/**
 * Simple benchmark used to compare basic HTTP service performance changes.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class BenchmarkTest {

    private final static int NR_SAMPLES = 40000;
    private final static int NR_THREADS = 40;
    private final static int TIMEOUT = 20; /* seconds */

    private final static String RES_CONTENT = "Hello World";
    private final static String SERV_PATH = "/benchmark";

    @Inject
    private static BundleContext m_bundleContext;
    private TestContext m_testContext;
    private BenchMarkServlet m_benchmarkServlet;
    private Component m_benchmarkComponent;
    private String m_port;

    @Configuration
    public Option[] config() {
        return options(
            CoreOptions.junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.shell").version("1.4.2"),
            mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.shell.tui").version("1.4.1"),
            CoreBundles.provisionAll(),
            WebBundles.provisionAllExcluding(WebBundles.TENANTRESOLVER_PARAMETER));
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default");

        CoreConfigs.provisionAllExcluding(m_testContext);
        WebConfigs.provisionAllExcluding(m_testContext, WebConfigs.HTTPSERVICE);

        // set this test on custom port
        m_port = "18880";
        Properties httpProps = new Properties();
        httpProps.put("org.osgi.service.http.port", m_port);
        m_testContext.updateConfig("org.apache.felix.http", httpProps);

        DependencyManager dm = m_testContext.getDependencyManager();
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(Constants.ALIAS_KEY, SERV_PATH);
        m_benchmarkServlet = new BenchMarkServlet(RES_CONTENT);
        m_benchmarkComponent = dm.createComponent()
            .setInterface(Servlet.class.getName(), properties)
            .setImplementation(m_benchmarkServlet);
        dm.add(m_benchmarkComponent);

        WebUtils.waitForURL("http://localhost:" + m_port + SERV_PATH + "?wait");
    }

    @After
    public void tearDown() throws Exception {
        DependencyManager dm = m_testContext.getDependencyManager();
        dm.remove(m_benchmarkComponent);
        m_testContext.tearDown();
    }

    @Test
    public void testBenchmark() throws Exception {
        final URL url = new URL("http://localhost:" + m_port + SERV_PATH);

        final ExecutorService exe = Executors.newFixedThreadPool(NR_THREADS);
        final CountDownLatch lat = new CountDownLatch(NR_SAMPLES);

        for (int i = 0; i < NR_SAMPLES; i++) {
            exe.submit(new HttpRequestRunner(url, lat));
        }

        // Wait for only a specific amount of time...
        assertTrue(lat.await(TIMEOUT, TimeUnit.SECONDS));
        // Don't shut down gracefully, but stop immediately...
        exe.shutdownNow();

        assertEquals(NR_SAMPLES, m_benchmarkServlet.getDoGetCount());
    }
}
