/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests;

import static junit.framework.Assert.assertTrue;
import static org.amdatu.tenant.Constants.NAME_KEY;
import static org.amdatu.tenant.Constants.PID_KEY;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.Servlet;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.tenant.conf.rp.Processor;
import org.amdatu.tenant.factory.TenantServiceFactory;
import org.amdatu.web.dispatcher.Constants;
import org.amdatu.web.itest.base.WebBundles;
import org.amdatu.web.itest.tests.mock.HttpRequestRunner;
import org.amdatu.web.itest.tests.mock.TenantResolverServlet;
import org.amdatu.web.itest.tests.util.WebUtils;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.CoreOptions;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class ParameterTenantResolverTest {

    private final static String SERV_HOST = "localhost";
    private final static String SERV_PATH = "/test";
    private final static int TIMEOUT = 2; /* seconds */

    @Inject
    private static BundleContext m_bundleContext;
    private TestContext m_testContext;
    private TenantResolverServlet m_tenantResolverServlet;
    private Component m_tenantResolverComponent;
    private String m_port;
    private ExecutorService m_executor;

    @Configuration
    public Option[] config() {
        return options(
            CoreOptions.junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAllExcluding(),
            WebBundles.provisionAllExcluding(WebBundles.TENANTRESOLVER_HOSTNAME));
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default");

        m_executor = Executors.newSingleThreadExecutor();

        CoreConfigs.provisionAllExcluding(m_testContext);

        // set this test on custom port
        m_port = "18882";
        Properties httpProps = new Properties();
        httpProps.put("org.osgi.service.http.port", m_port);
        m_testContext.updateConfig("org.apache.felix.http", httpProps);

        // register a servlet
        DependencyManager dm = m_testContext.getDependencyManager();
        Dictionary<String, Object> properties = new Hashtable<String, Object>();
        properties.put(Constants.ALIAS_KEY, SERV_PATH);
        m_tenantResolverServlet = new TenantResolverServlet();
        m_tenantResolverComponent = dm.createComponent()
            .setInterface(Servlet.class.getName(), properties)
            .setImplementation(m_tenantResolverServlet);
        dm.add(m_tenantResolverComponent);

        WebUtils.waitForURL("http://" + SERV_HOST + ":" + m_port + SERV_PATH);
    }

    @After
    public void tearDown() throws Exception {
        DependencyManager dm = m_testContext.getDependencyManager();
        dm.remove(m_tenantResolverComponent);
        m_testContext.tearDown();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void testRegisteredTenantResolves() throws Exception {

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 not to resolve", m_tenantResolverServlet.getLastTenant() == null);

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 not to resolve", m_tenantResolverServlet.getLastTenant() == null);

        // configure tenant1
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default", "tenant1");
        m_testContext.waitForSystemToSettle();

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant1"));

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 not to resolve", m_tenantResolverServlet.getLastTenant() == null);

        // configure tenant2
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default", "tenant1", "tenant2");
        m_testContext.waitForSystemToSettle();

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant1"));

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant2"));

        // update tenant1
        m_testContext.configureTenants(
            new TestContext.TenantWithProperties(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM),
            new TestContext.TenantWithProperties("Default"),
            new TestContext.TenantWithProperties("tenant1", org.amdatu.tenant.Constants.NAME_KEY, "Some new name"),
            new TestContext.TenantWithProperties("tenant2")
        );
        m_testContext.waitForSystemToSettle();

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant1"));

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant2"));

        // remove tenant2
        m_testContext.configureTenants(
            new TestContext.TenantWithProperties(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM),
            new TestContext.TenantWithProperties("Default"),
            new TestContext.TenantWithProperties("tenant1", org.amdatu.tenant.Constants.NAME_KEY, "Some new name")
        );
        m_testContext.waitForSystemToSettle();

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 to resolve", m_tenantResolverServlet.getLastTenant() != null
            && m_tenantResolverServlet.getLastTenant().getPID().equals("tenant1"));

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 not to resolved", m_tenantResolverServlet.getLastTenant() == null);

        // remove tenant1
        m_testContext.configureTenants(
            new TestContext.TenantWithProperties(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM),
            new TestContext.TenantWithProperties("Default")
        );
        m_testContext.waitForSystemToSettle();

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant1");
        Assert.assertTrue("Expected tenant1 not to resolved", m_tenantResolverServlet.getLastTenant() == null);

        executeRequest("http://" + SERV_HOST + ":" + m_port + SERV_PATH + "?tenant=" + "tenant2");
        Assert.assertTrue("Expected tenant2 not to resolved", m_tenantResolverServlet.getLastTenant() == null);
    }

    private void executeRequest(final String getUrl) throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        m_executor.submit(new HttpRequestRunner(getUrl, latch));

        // Wait for only a specific amount of time...
        assertTrue(latch.await(TIMEOUT, TimeUnit.SECONDS));
    }
}
