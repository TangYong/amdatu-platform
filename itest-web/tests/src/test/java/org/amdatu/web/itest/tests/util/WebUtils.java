/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


/**
 * Simple utility class used to wait for the availability of an URL.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class WebUtils
{
    private final static int HTTP_TIMEOUT = 10000;
    private final static int HTTP_STATUS_OK = HttpURLConnection.HTTP_OK;

    private WebUtils() {
    }

    public static void waitForURL(String url) throws MalformedURLException {
        waitForURL(new URL(url));
    }

    public static void waitForURL(URL url) {
        waitForURL(url, HTTP_STATUS_OK);
    }

    public static void waitForURL(String url, int status) throws MalformedURLException {
        waitForURL(new URL(url), status);
    }

    public static void waitForURL(URL url, int status) {
        if (!waitForURL(url, status, HTTP_TIMEOUT)) {
            throw new IllegalStateException("Timeout occurred waiting for '" + url + "' to come available!");
        }
    }

    /**
     * Wait until the service at the specified URL returns the specified response code with a timeout as specified.
     * 
     * @param URL the URL to wait for;
     * @param responseCode the expected response code;
     * @param timeout the timeout in milliseconds to wait for the URL.
     */
    public static boolean waitForURL(URL url, int responseCode, int timeout) {
        final long deadline = System.currentTimeMillis() + timeout;

        boolean found = false;
        while (!found && (System.currentTimeMillis() < deadline)) {
            try {
                int rc = checkURL(url);
                
                if (responseCode == rc) {
                    found = true;
                }
            }
            catch (IOException e) {
                // Ignore...
            }

            try {
                TimeUnit.MILLISECONDS.sleep(50);
            }
            catch (InterruptedException ie) {
                // Make sure the thread's administration remains correct!
                Thread.currentThread().interrupt();
            }
        }
        
        return found;
    }

    /**
     * Invokes a URL and returns the response code.
     * 
     * @throws IOException
     */
    public static int checkURL(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        int responseCode;
        try {
            connection.connect();
            responseCode = connection.getResponseCode();
        }
        finally {
            connection.disconnect();
        }
        return responseCode;
    }
}
