/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests.mock;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class HttpRequestRunner implements Callable<String> {
    private final URL m_url;
    private final CountDownLatch m_latch;

    /**
     * @param url
     * @param lat
     * @throws MalformedURLException
     */
    public HttpRequestRunner(String url, CountDownLatch lat) throws MalformedURLException {
        this(new URL(url), lat);
    }

    /**
     * @param url
     * @param lat
     */
    public HttpRequestRunner(URL url, CountDownLatch lat) {
        m_url = url;
        m_latch = lat;
    }

    /**
     * @see java.util.concurrent.Callable#call()
     */
    public String call() throws Exception {
        HttpURLConnection connection = null;
        InputStream is = null;

        try {
            connection = (HttpURLConnection) m_url.openConnection();
            connection.setDoInput(true);

            is = connection.getInputStream();

            StringBuilder builder = new StringBuilder();
            
            int i;
            while ((i = is.read()) != -1) {
                builder.append((char) i);
            }

            return builder.toString();
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                }
                catch (IOException exception) {
                    // Ignored...
                    exception.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                }
                catch (Exception exception) {
                    // Ignored...
                    exception.printStackTrace();
                }
            }
            
            // Mark this request as "done"...
            m_latch.countDown();
        }
    }
}