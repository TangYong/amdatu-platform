/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests;

import static org.amdatu.web.dispatcher.Constants.CONTEXT_ID_KEY;
import static org.amdatu.web.jsp.Constants.JSP_ALIAS_KEY;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import javax.inject.Inject;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.web.httpcontext.ResourceProvider;
import org.amdatu.web.itest.base.WebBundles;
import org.amdatu.web.itest.base.WebConfigs;
import org.amdatu.web.itest.tests.util.WebUtils;
import org.apache.felix.dm.DependencyManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.CoreOptions;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 * 
 */
@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class JspResourceProviderTest {

    // This is a basic test for JSP support including loading additional
    // code from the JSP provider's bundle.
    //
    // Note that the test.jsp resource contains a scriptlet using packages
    // from th GSon library. For this to work the JSP support library must
    // consult the JSP provider's bundle. As in this case this is our test
    // probe we make sure we can (dynamically) import those packages and
    // all should work fine.

    @Inject
    private static BundleContext m_bundleContext;

    private TestContext m_testContext;
    private String m_url_base = "http://localhost:";
    private String m_port;

    @Configuration
    public Option[] config() {
        return options(
            CoreOptions.junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.google.code.gson").artifactId("gson").versionAsInProject()),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAll(),
            WebBundles.provisionAll());
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default");

        CoreConfigs.provisionAll(m_testContext);
        WebConfigs.provisionAllExcluding(m_testContext, WebConfigs.HTTPSERVICE);

        // Setup a custom HttpService configuration with a port not used by
        // other tests.
        m_port = "18890";
        Properties httpProps = new Properties();
        httpProps.put("org.osgi.service.http.port", m_port);
        m_testContext.updateConfig("org.apache.felix.http", httpProps);

        // Register a ResourceProvider to deliver the JSP resource to the
        // JSP support component.
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(CONTEXT_ID_KEY, "" + JspResourceProviderTest.class.getSimpleName());
        props.put(JSP_ALIAS_KEY, "/" + JspResourceProviderTest.class.getSimpleName());

        DependencyManager dm = m_testContext.getDependencyManager();
        dm.add(dm.createComponent()
            .setInterface(ResourceProvider.class.getName(), props)
            .setImplementation(new ResourceProvider() {
                public URL getResource(String name) {
                    if (name.startsWith("/WEB-INF/"))
                        return null;
                    return m_bundleContext.getBundle().getResource("/jsp/JspResourceProviderTest.jsp");
                }
            }));
    }

    /**
     * If the JSP compiles and executes without errors we should simply expect a 200
     * response within a reasonable timeframe.
     */
    @Test
    public void testJspServlet() throws Exception {
        try {
            WebUtils.waitForURL(m_url_base + m_port + "/" + JspResourceProviderTest.class.getSimpleName());
        }
        catch (Exception e) {}
    }
}
