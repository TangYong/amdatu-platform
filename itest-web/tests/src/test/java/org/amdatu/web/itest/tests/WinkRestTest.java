/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.web.itest.tests;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.CoreOptions.wrappedBundle;

import java.net.URL;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.amdatu.itest.base.CoreBundles;
import org.amdatu.itest.base.CoreConfigs;
import org.amdatu.itest.base.TestContext;
import org.amdatu.web.itest.base.WebBundles;
import org.amdatu.web.itest.base.WebConfigs;
import org.amdatu.web.itest.tests.mock.HttpRequestRunner;
import org.amdatu.web.itest.tests.mock.WinkRestServlet;
import org.amdatu.web.itest.tests.util.WebUtils;
import org.apache.felix.dm.DependencyManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.CoreOptions;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.Configuration;
import org.ops4j.pax.exam.junit.ExamReactorStrategy;
import org.ops4j.pax.exam.junit.JUnit4TestRunner;
import org.ops4j.pax.exam.spi.reactors.EagerSingleStagedReactorFactory;
import org.osgi.framework.BundleContext;

@RunWith(JUnit4TestRunner.class)
@ExamReactorStrategy(EagerSingleStagedReactorFactory.class)
public class WinkRestTest
{

    public static final String PATH_KEY = "wink.rest.path";
    public static final String PID_VALUE = "org.amdatu.web.rest.wink";

    @Inject
    private static BundleContext m_bundleContext;

    private TestContext m_testContext;
    private WinkRestServlet m_winkRestServlet;
    private String m_url_base = "http://localhost:";
    private String m_port;

    @Configuration
    public Option[] config() {
        return options(
            CoreOptions.junitBundles(),
            wrappedBundle(mavenBundle().groupId("com.cenqua.clover").artifactId("clover").versionAsInProject()),
            CoreBundles.provisionAll(),
            WebBundles.provisionAll());
    }

    @Before
    public void setUp() throws Exception {
        m_testContext = new TestContext(m_bundleContext);
        m_testContext.setUp();
        m_testContext.configureTenants(org.amdatu.tenant.Constants.PID_VALUE_PLATFORM, "Default");

        CoreConfigs.provisionAll(m_testContext);
        WebConfigs.provisionAllExcluding(m_testContext, WebConfigs.HTTPSERVICE);

        // set this test on custom port
        m_port = "18993";
        Properties httpProps = new Properties();
        httpProps.put("org.osgi.service.http.port", m_port);
        m_testContext.updateConfig("org.apache.felix.http", httpProps);

        // register testservlet
        DependencyManager dm = m_testContext.getDependencyManager();
        m_winkRestServlet = new WinkRestServlet();
        dm.add(dm.createComponent()
            .setInterface(WinkRestServlet.class.getName(), null)
            .setImplementation(m_winkRestServlet));
    }

    @Test
    public void test() throws Exception {
        URL url;

        url = updatePath("/foo/bar");
        executeRequest(url);

        url = updatePath("/foo");
        executeRequest(url);

        url = updatePath("");
        executeRequest(url);
    }

    private URL updatePath(String newPath) throws Exception {
        Properties properties = new Properties();
        properties.put(PATH_KEY, newPath);
        m_testContext.updateConfig(PID_VALUE, properties);

        URL url = new URL(m_url_base + m_port + newPath + "/");
        WebUtils.waitForURL(url);

        return url;
    }

    private void executeRequest(URL url) throws Exception {
        CountDownLatch latch = new CountDownLatch(1);

        String result = new HttpRequestRunner(url, latch).call();
        assertTrue(latch.await(2, TimeUnit.SECONDS));

        assertNotNull(result);
        assertTrue(result.length() > 0);
    }
}
